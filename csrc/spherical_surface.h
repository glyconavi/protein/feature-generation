#ifndef SPHERICAL_SURFACE_H_
#define SPHERICAL_SURFACE_H_
#include "figure3d.h"
#include <stddef.h>

double calc_surface_area_of_spheres(
        Sphere* spheres, int num_spheres, double margin);

#endif /* SPHERICAL_SURFACE_H_ */
