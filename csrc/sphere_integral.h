#ifndef SPHERE_INTEGRAL_H_
#define SPHERE_INTEGRAL_H_
#include "spheres_collision.h"
#include "figure3d.h"

double spherical_surfaces_integral(
        Sphere* spheres, size_t num_spheres, SpheresCollision* col,
        double margin, size_t num_slice);

double spherical_surface_integral(
        Sphere* sphere, SpheresCollision* col,
        double margin, size_t num_slice);

void spherical_surfaces_integral_multi_margin(
        double* out_areas,
        Sphere* spheres, size_t num_spheres, SpheresCollision* col,
        double* margins, size_t num_margin, size_t num_slice);

void spherical_surface_integral_multi_margin(
        double* out_areas,
        Sphere* sphere, SpheresCollision* col,
        double* margins, size_t num_margin,
        size_t num_slice);

#endif /* SPHERE_INTEGRAL_H_ */

