#include "astar_search.h"
#include <stdbool.h>
#include <stdlib.h>
#include <float.h>

typedef struct {
    const void* node;
    bool opened;
    double score;
    double hscore;
    const void* prev;
} ValueBuf;

/**
 * A* search による最短経路探索を全ノード数に比例したメモリを使用して行う.
 * @param [in] value_array 入力ノード集合
 * @param [in] num_value ノード総数
 * @param [in] size_value 1ノードのバイト数
 * @param [in] start_idx 開始地点のvalue_array上のインデックス
 * @param [in] end_idx 終了地点のvalue_array上のインデックス
 * @param [in] heuristic_func ヒューリスティック関数 距離(ノード,ノード)
 * @param [in] neighbor_func 隣接ノード取得関数
 *                           隣接ノード数(隣接ノードデータバッファ,
 *                                        バッファサイズ,
 *                                        基準ノード,
 *                                        context)
 * @param [in] context neighbor_funcに渡されるデータ
 * @return 最短経路の総距離, 経路が見つからなかった場合は -1.0
 */
double aster_search_from_short_array(
        const void* value_array, size_t num_value, size_t size_value,
        size_t start_idx, size_t end_idx,
        double (*heuristic_func)(const void*, const void*),
        ASterNeighbor* (*neighbor_func)(size_t*, const void*, void*),
        void* context) {
    const char* values = (const char*)value_array;
    const char* end_node = values + end_idx * size_value;
    ValueBuf* searched = malloc(num_value * sizeof(ValueBuf));
    for (size_t i = 0; i < num_value; ++i) {
        searched[i].opened = false;
        searched[i].hscore = DBL_MAX;
        searched[i].prev = NULL;
    }
    size_t* queue_buf = malloc(num_value * sizeof(size_t));
    queue_buf[0] = start_idx;
    searched[queue_buf[0]].opened = true;
    searched[queue_buf[0]].score = 0.0;
    searched[queue_buf[0]].hscore = heuristic_func(
            values + start_idx * size_value, end_node);
    size_t num_queue = 1;

    double result_score;
    while (1) {
        size_t min_queue_idx = 0;
        double min_hscore = searched[queue_buf[0]].hscore;
        for (size_t i = 1; i < num_queue; ++i) {
            if (searched[queue_buf[i]].hscore < min_hscore) {
                min_queue_idx = i;
                min_hscore = searched[queue_buf[i]].hscore;
            }
        }
        const double min_score = searched[queue_buf[min_queue_idx]].score;
        const size_t min_idx = queue_buf[min_queue_idx];
        if (min_idx == end_idx) {
            result_score = min_score;
            break;
        }
        size_t num_neg;
        ASterNeighbor* neg_buf = neighbor_func(
                &num_neg, values + min_idx * size_value, context);
        searched[min_idx].opened = false;
        if (num_queue > 1) {
            queue_buf[min_queue_idx] = queue_buf[num_queue - 1];
        }
        --num_queue;
        for (size_t i = 0; i < num_neg; ++i) {
            size_t* w_idx = queue_buf + num_queue;
            *w_idx = (((const char*)neg_buf[i].node) - values) / size_value;
            ValueBuf* w = searched + *w_idx;
            double new_score = min_score + neg_buf[i].d;
            double new_hscore = new_score
                                + heuristic_func(neg_buf[i].node, end_node);
            if (new_hscore < w->hscore) {
                w->score = new_score;
                w->hscore = new_hscore;
                if (!w->opened) {
                    ++num_queue;
                }
                w->opened = true;
                w->prev = values + min_idx * size_value;
            }
        }
        free(neg_buf);
        if (num_queue == 0) {
            result_score = -1.0;
            break;
        }
    }
    free(queue_buf);
    free(searched);
    return result_score;
}

typedef struct CalcBuf_ {
    const void* node;
    double score;
    double hscore;
    struct CalcBuf_* prev;
    struct CalcBuf_* list_next;
} CalcBuf;

static void remove_first_node_from_list(CalcBuf** list_prev, const void* node) {
    CalcBuf* p = *list_prev;
    CalcBuf* c = p->list_next;
    while (c != NULL) {
        if (c->node == node) {
            p->list_next = c->list_next;
            free(c);
            break;
        }
        p = c;
        c = c->list_next;
    }
}

/**
 * @param opened
 * @param score
 * @param hscore
 * @param node
 * @param prev_buf
 */
static void insert_buf_list(
        CalcBuf** opened, double score, double hscore, const void* node,
        CalcBuf* prev_buf) {
    CalcBuf* c = *opened;
    CalcBuf* p = NULL;
    while (1) {
        if ((c != NULL) && (c->node == node)) {
            if (hscore < c->hscore) {
                c->score = score;
                c->hscore = hscore;
                c->prev = prev_buf;
            }
            break;
        }
        if ((c == NULL) || (hscore <= c->hscore)) {
            if (p == NULL) {
                *opened = malloc(sizeof(CalcBuf));
                p = *opened;
            } else {
                p->list_next = malloc(sizeof(CalcBuf));
                p = p->list_next;
            }
            p->node = node;
            p->score = score;
            p->hscore = hscore;
            p->prev = prev_buf;
            p->list_next = c;
            remove_first_node_from_list(&p, node);
            break;
        }
        p = c;
        c = c->list_next;
    }
}

/**
 * closedリストにnodeが含まれていて、該当nodeのhscoreが入力hscoreより大きい場合
 * はclosedリストから該当ノードを削除する.
 * @param [inout] closed リスト
 * @param [in] hscore
 * @param [in] node
 * @return closedリストにnodeが含まれていない時は true
 *         closedリストにnodeが含まれている時は,
 *         hscoreがclosedリストの同一ノードより小さい場合はtrueそれ以外はfalse
 */
static bool check_close_and_pop(
        CalcBuf** closed, double hscore, const void* node) {
    CalcBuf* c = *closed;
    CalcBuf* p = NULL;
    while (c != NULL) {
        if (c->node == node) {
            if (hscore < c->hscore) {
                if (p != NULL) {
                    p->list_next = c->list_next;
                } else {
                    *closed = c->list_next;
                }
                free(c);
                return true;
            } else {
                return false;
            }
        }
        p = c;
        c = c->list_next;
    }
    return true;
}

/**
 * A* search による最短経路探索を行う.
 * @param [in] value_array 入力ノード集合
 * @param [in] num_value ノード総数
 * @param [in] size_value 1ノードのバイト数
 * @param [in] start_idx 開始地点のvalue_array上のインデックス
 * @param [in] end_idx 終了地点のvalue_array上のインデックス
 * @param [in] heuristic_func ヒューリスティック関数 距離(ノード,ノード)
 * @param [in] neighbor_func 隣接ノード取得関数
 *                           隣接ノード数(隣接ノードデータバッファ,
 *                                        バッファサイズ,
 *                                        基準ノード,
 *                                        context)
 * @param [in] context neighbor_funcに渡されるデータ
 * @return 最短経路の総距離, 経路が見つからなかった場合は -1.0
 */
double aster_search(
        const void* value_array, size_t num_value, size_t size_value,
        size_t start_idx, size_t end_idx,
        double (*heuristic_func)(const void*, const void*),
        ASterNeighbor* (*neighbor_func)(size_t*, const void*, void*),
        void* neighbor_context) {
    return aster_search_from_iterator(
            value_array + start_idx * size_value,
            value_array + end_idx * size_value,
            heuristic_func, neighbor_func, neighbor_context);
}

/**
 * A* search による最短経路探索を行う.
 * @param [in] start_node 経路の開始ノード
 * @param [in] end_node 経路の終端ノード
 * @param [in] heuristic_func ヒューリスティック関数 距離(ノード,ノード)
 * @param [in] neighbor_func 隣接ノード取得関数
 *                           隣接ノード数(隣接ノードデータバッファ,
 *                                        バッファサイズ,
 *                                        基準ノード,
 *                                        context)
 * @param [in] context neighbor_funcに渡されるデータ
 * @return 最短経路の総距離, 経路が見つからなかった場合は -1.0
 */
double aster_search_from_iterator(
        const void* start_node, const void* end_node,
        double (*heuristic_func)(const void*, const void*),
        ASterNeighbor* (*neighbor_func)(size_t*, const void*, void*),
        void* neighbor_context) {
    if (start_node == end_node) {
        return 0.0;
    }
    CalcBuf* closed = NULL;
    CalcBuf* opened = (CalcBuf*)malloc(sizeof(CalcBuf));
    opened->node = start_node;
    opened->score = 0.0;
    opened->hscore = heuristic_func(start_node, end_node);
    opened->prev = NULL;
    opened->list_next = NULL;

    double result_score;
    while (1) {
        CalcBuf* min_buf = opened;
        if (min_buf == NULL) {
            result_score = -1.0;
            break;
        }
        if (min_buf->node == end_node) {
            result_score = min_buf->score;
            break;
        }
        size_t num_neg;
        ASterNeighbor* neg_buf = neighbor_func(
                &num_neg, min_buf->node, neighbor_context);
        opened = min_buf->list_next;
        CalcBuf* closed_next = closed;
        closed = min_buf;
        min_buf->list_next = closed_next;
        for (size_t i = 0; i < num_neg; ++i) {
            double new_score = min_buf->score + neg_buf[i].d;
            double new_hscore = new_score
                                + heuristic_func(neg_buf[i].node, end_node);
            bool is_insert = check_close_and_pop(
                    &closed, new_hscore, neg_buf[i].node);
            if (is_insert) {
                insert_buf_list(
                        &opened, new_score, new_hscore, neg_buf[i].node,
                        min_buf);
            }
        }
        free(neg_buf);
    }
    CalcBuf* c = opened;
    CalcBuf* n;
    while (c != NULL) {
        n = c->list_next;
        free(c);
        c = n;
    }
    c = closed;
    while (c != NULL) {
        n = c->list_next;
        free(c);
        c = n;
    }
    return result_score;
}
