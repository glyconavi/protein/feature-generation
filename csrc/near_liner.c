#include "near_liner.h"

NearestResult nearest_neighbor_liner(
        double (*distance_func)(const void*, const void*),
        void* data_start, void* data_end, size_t data_size,
        const void* v) {
    NearestResult ret = {
        .d = (*distance_func)(v, data_start),
        .val = data_start,
    };
    for (void* p = data_start + data_size; p < data_end; p += data_size) {
        double d = (*distance_func)(v, p);
        if (d < ret.d) {
            ret.d = d;
            ret.val = p;
        }
    }
    return ret;
}
