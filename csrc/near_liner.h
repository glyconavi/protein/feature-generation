#ifndef NEAR_LINER_H_
#define NEAR_LINER_H_
#include "neighbor_def.h"
#include <stddef.h>

NearestResult nearest_neighbor_liner(
        double (*distance_func)(const void*, const void*),
        void* data_start, void* data_end, size_t data_size,
        const void* v);

#endif /* NEAR_LINER_H_ */
