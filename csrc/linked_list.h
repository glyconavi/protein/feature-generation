#ifndef LINKED_LIST_H_
#define LINKED_LIST_H_
#include <stddef.h>
#include <stdbool.h>

typedef struct _ConstLinkedListNode ConstLinkedListNode;

typedef struct _ListIterator {
    ConstLinkedListNode* node;
} ListIterator;

typedef struct _ConstLinkedList {
    size_t num;
    ConstLinkedListNode* head;
} ConstLinkedList;

ConstLinkedList* create_list();

void free_list(ConstLinkedList* list);

void push_to_list(ConstLinkedList* list, const void* val);

void remove_if_list(ConstLinkedList* list,
                    bool (*cond)(const void*, void*), void* context);

bool insert_prev_list(ConstLinkedList* list,
                      const void* new_val,
                      bool (*cond)(const void*, void*),
                      bool (*stop)(const void*, void*),
                      void* context);

void move_front_2list(ConstLinkedList* dst, ConstLinkedList* src);

ListIterator get_list_iterator(ConstLinkedList* list);

const void* list_next(void* itr);

void* list_to_array(ConstLinkedList* list, size_t size);

#endif /* LINKED_LIST_H_ */
