#ifndef SORT_H_
#define SORT_H_
#include <stddef.h>

void merge_sort_work(void * restrict start, size_t num, size_t size,
                     int (*compare)(const void*, const void*),
                     void * restrict work);

void merge_sort(void* start, size_t num, size_t size,
                int (*compare)(const void*, const void*));

#endif /* SORT_H_ */
