#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include "sphere_integral.h"
//#include "cone_hide.h"
#include <stdio.h>

#undef PI
#define PI 3.141592653589793238462643

typedef struct {
    double start;
    double end;
} Range;

typedef struct {
    double val;
    bool open;
} RangeFlag;

/**
 * [-all_len, 2 * all_len]の範囲に収まる数値を
 * [0, all_len]の周期の範囲に収まるように変更する.
 * @param val [-all_len, 2 * all_len] の範囲に収まる数値
 * @param all_len 周期の幅
 * @return [0, all_len]の周期の範囲の値
 */
static inline double to_loop_val(double val, const double all_len) {
    if (val < 0) {
        val += all_len;
    } else if (val > all_len) {
        val -= all_len;
    }
    return val;
}

/**
 * RangeFlag型の比較関数
 * RangeFlag.valの大小比較を行う.
 */
static int compare_range_flag(const void* lhs, const void* rhs) {
    if (((RangeFlag*)lhs)->val < ((RangeFlag*)rhs)->val) {
        return -1;
    } else if (((RangeFlag*)lhs)->val > ((RangeFlag*)rhs)->val) {
        return 1;
    } else {
        return 0;
    }
}

/**
 * rangesで指定した領域を除外した[0, all_len]領域の長さを計算する.
 * @param all_len 領域の幅
 * @param ranges 除外領域の配列
 * @param num_range rangesの配列長
 */
static double calc_loop_length(double all_len, Range* ranges, size_t num_range)
{
    RangeFlag* flags = malloc(num_range * 2 * sizeof(RangeFlag));
    unsigned int num_init_open = 0;
    for (size_t i = 0; i < num_range; ++i) {
        flags[i * 2] = (RangeFlag) {
            .val = ranges[i].start,
            .open = true,
        };
        flags[i * 2 + 1] = (RangeFlag) {
            .val = ranges[i].end, all_len,
            .open = false,
        };
        if (flags[i * 2].val > flags[i * 2 + 1].val) {
            ++num_init_open;
        }
    }
    qsort(flags, num_range * 2, sizeof(RangeFlag), compare_range_flag);
    double loop_len = 0.0;
    unsigned int cur_opened = num_init_open;
    double prev_val = 0.0;
    for (RangeFlag* flag = flags; flag < flags + num_range * 2; ++flag) {
        if (flag->open) {
            if (cur_opened == 0) {
                loop_len += flag->val - prev_val;
            }
            ++cur_opened;
        } else {
            --cur_opened;
            if (cur_opened == 0) {
                prev_val = flag->val;
            }
        }
    }
    free(flags);
    if (cur_opened == 0) {
        loop_len += all_len - prev_val;
    }
    return loop_len;
}

typedef struct {
    double r2;
    double cz;
    double c_norm;
    double c2_inv;
    double c_theta;
} OtherCircleBuf;

/**
 * 指定された球に衝突している球集合を取得し、積分計算に使用するデータを生成する.
 * @param [out] num 衝突している球の数
 * @param [in] sphere 1つの球
 * @param [in] col 衝突判定オブジェクト
 * @param [in] margin
 * @return 指定された球に衝突している球集合に対応する
 *         積分計算に使用するデータ配列
 */
static OtherCircleBuf* create_circle_buf(
        size_t* num, Sphere* sphere, SpheresCollision* col,
        double margin) {
    ConstLinkedList* list = get_collided_spheres(col, sphere, margin);
    //clean_spheres_from_linked_list(list, sphere, margin);
    *num = list->num;
    OtherCircleBuf* array = malloc((*num) * sizeof(OtherCircleBuf));
    ListIterator itr = get_list_iterator(list);
    const Sphere* s = list_next(&itr);
    for (int i = 0; i < list->num; ++i) {
        Vec3d sub = vec3d_sub(&s->center, &sphere->center);
        const double c2 = sub.x * sub.x + sub.y * sub.y;
        array[i] = (OtherCircleBuf) {
            .r2 = pow(s->radius + margin, 2),
            .cz = sub.z,
            .c_norm = sqrt(c2),
            .c2_inv = 1.0 / c2,
            .c_theta = atan2(sub.y, sub.x),
        };
        s = list_next(&itr);
    }
    free_list(list);
    return array;
}


/**
 * 円1の中心からみた2つの円の交点の角度の1/2
 * @param [in] r1 円1の半径
 * @param [in] r1_2 円1の半径の2乗
 * @param [in] r2_2 円2の半径の2乗
 * @param [in] c_norm 円1の中心から円2の中心までの距離
 * @param [in] c2_inv 1.0 / (c_norm*c_norm)
 * @return 円1の中心からみた2つの円の交点の角度の1/2
 */
static inline double circle_theta(double r1, double r1_2, double r2_2,
                                  double c_norm, double c2_inv) {
    const double d1 = 0.5 * (1.0 + (r1_2 - r2_2) * c2_inv) * c_norm;
    return acos(d1 / r1);
}

/**
 * 複数の球からなる立体の表面積を計算する.
 * 球同士の衝突判定に入力球を登録済みの衝突判定オブジェクトを使用する.
 * 計算は球面を指定数で分割して積分を行う.
 * @param spheres 球の配列
 * @param num_spheres 入力球の数
 * @param col 入力球が登録された衝突判定オブジェクト
 * @param margin 衝突判定対象の2つの球の両方の半径を指定値分加算する.
 * @param num_slice 1つの球面を指定数で分割して積分を行う
 * @return 入力球集合の重複分を省いた合計表面積
 */
double spherical_surfaces_integral(
        Sphere* spheres, size_t num_spheres, SpheresCollision* col,
        double margin, size_t num_slice) {
    double area = 0.0;
    for (size_t i = 0; i < num_spheres; ++i) {
        area += spherical_surface_integral(
                &spheres[i], col, margin, num_slice);
    }
    /*
    int count1_4 = 0;
    int count2_8 = 0;
    int count7_0 = 0;
    for (size_t i = 0; i < num_spheres; ++i) {
        if (spherical_surface_integral(
                    &spheres[i], col, 1.4, 16) > 0) {
            ++count1_4;
            if (spherical_surface_integral(
                        &spheres[i], col, 2.8, 16) > 0) {
                ++count2_8;
                if (spherical_surface_integral(
                            &spheres[i], col, 7.0, 16) > 0) {
                    ++count7_0;
                    area += spherical_surface_integral(
                            &spheres[i], col, margin, num_slice);
                }
            }
        }
    }
    printf("1.4_per = %f\n", ((double)count1_4) / num_spheres);
    printf("2.8_per = %f\n", ((double)count2_8) / num_spheres);
    printf("7.0_per = %f\n", ((double)count7_0) / num_spheres);
    */
    return area;
}

/**
 * 複数の球からなる立体に含まれる指定された1つの球の占める表面積を計算する.
 * @param sphere 計算対象の球
 * @param col 立体を構成する球が登録された衝突判定オブジェクト
 * @param margin 衝突判定対象の2つの球の両方の半径を指定値分加算する.
 * @param num_slice 球を指定数分分割して積分を行う
 * @return 複数の球からなる立体に含まれる指定された1つの球の占める表面積
 */
double spherical_surface_integral(
        Sphere* sphere, SpheresCollision* col,
        double margin, size_t num_slice) {
    size_t num_others;
    OtherCircleBuf* others =
        create_circle_buf(&num_others, sphere, col, margin);
    Range* range_buf = malloc(num_others * sizeof(Range));
    const double sr = sphere->radius + margin;
    const double sr2 = sr * sr;
    const double inv_num_slice = 1.0 / num_slice;
    const double one_slice_area_div_2pi = (2 * sr2) * inv_num_slice;
    double area = 0.0;
    for (size_t i = 0; i < num_slice; ++i) {
        const double dz = sr * ((2.0 * (i + 0.5) * inv_num_slice) - 1.0);
        const double r2 = sr2 - dz*dz;
        const double r = sqrt(r2);
        size_t num_circle = 0;
        for (OtherCircleBuf* o = others; o < others + num_others; ++o) {
            const double r_o2 = o->r2 - pow(o->cz - dz, 2);
            if (r_o2 <= 0.0) {
                continue;
            }
            const double r_o = sqrt(r_o2);
            if ((o->c_norm >= r + r_o) || (o->c_norm <= r - r_o)) {
                continue;
            }
            if (o->c_norm <= r_o - r) {
                range_buf[0].start = 0.0;
                range_buf[0].end = 2 * PI;
                num_circle = 1;
                break;
            }
            double width_theta = circle_theta(
                r, r2, r_o2, o->c_norm, o->c2_inv);
            range_buf[num_circle].start =
                to_loop_val(o->c_theta - width_theta, 2 * PI);
            range_buf[num_circle].end =
                to_loop_val(o->c_theta + width_theta, 2 * PI);
            ++num_circle;
        }
        const double circum = calc_loop_length(PI * 2, range_buf, num_circle);
        area += circum * one_slice_area_div_2pi;
    }
    free(range_buf);
    free(others);
    return area;
}

void spherical_surfaces_integral_multi_margin(
        double* out_areas,
        Sphere* spheres, size_t num_spheres, SpheresCollision* col,
        double* margins, size_t num_margin, size_t num_slice) {
    double area = 0.0;
    for (size_t m = 0; m < num_margin; ++m) {
        out_areas[m] = 0.0;
    }
    for (size_t i = 0; i < num_spheres; ++i) {
        spherical_surface_integral_multi_margin(
                out_areas, &spheres[i], col, margins, num_margin, num_slice);
    }
}

void spherical_surface_integral_multi_margin(
        double* out_areas,
        Sphere* sphere, SpheresCollision* col,
        double* margins, size_t num_margin,
        size_t num_slice) {
    OtherCircleBuf** others = malloc(num_margin * sizeof(OtherCircleBuf*));
    size_t* num_others = malloc(num_margin * sizeof(size_t));
    for (size_t m = 0; m < num_margin; ++m) {
        others[m] = create_circle_buf(
                &num_others[m], sphere, col, margins[m]);
    }
    Range* range_buf = malloc(num_others[num_margin - 1] * sizeof(Range));
    const double inv_num_slice = 1.0 / num_slice;
    double area = 0.0;
    for (size_t i = 0; i < num_slice; ++i) {
        for (size_t m = 0; m < num_margin; ++m) {
            const double sr = sphere->radius + margins[m];
            const double sr2 = sr * sr;
            const double dz = sr * ((2.0 * (i + 0.5) * inv_num_slice) - 1.0);
            const double r2 = sr2 - dz*dz;
            const double r = sqrt(r2);
            size_t num_circle = 0;
            for (OtherCircleBuf* o = others[m]; o < others[m] + num_others[m];
                    ++o) {
                const double r_o2 = o->r2 - pow(o->cz - dz, 2);
                if (r_o2 <= 0.0) {
                    continue;
                }
                const double r_o = sqrt(r_o2);
                if ((o->c_norm >= r + r_o) || (o->c_norm <= r - r_o)) {
                    continue;
                }
                if (o->c_norm <= r_o - r) {
                    range_buf[0].start = 0.0;
                    range_buf[0].end = 2 * PI;
                    num_circle = 1;
                    break;
                }
                double width_theta = circle_theta(
                    r, r2, r_o2, o->c_norm, o->c2_inv);
                range_buf[num_circle].start =
                    to_loop_val(o->c_theta - width_theta, 2 * PI);
                range_buf[num_circle].end =
                    to_loop_val(o->c_theta + width_theta, 2 * PI);
                ++num_circle;
            }
            const double circum =
                calc_loop_length(PI * 2, range_buf, num_circle);
            if (circum == 0.0) {
                break;
            }
            out_areas[m] += circum * 2 * sr2 * inv_num_slice;
        }
    }
    free(range_buf);
    free(num_others);
    free(others);
}
