/**
 * 球面をほぼ均等に覆う点を使用した計算
 */
#include "spherical_surface_plot.h"
#include "figure3d.h"
#include <math.h>
#include <stddef.h>
#include <stdlib.h>

#undef PI
#define PI 3.141592653589793238462643

#define LEN_COL_BUF 128

/**
 * 複数の球からなる立体に含まれる指定された1つの球の占める表面積を計算する.
 * @param sphere 計算対象の球
 * @param col 立体を構成する球が登録された衝突判定オブジェクト
 * @param margin 衝突判定対象の2つの球の両方の半径を指定値分加算する.
 * @param unit_points 中心原点,半径1の球面をほぼ均等に覆う点配列
 * @param num_points unit_pointsが保持する点の数
 * @return 複数の球からなる立体に含まれる指定された1つの球の占める表面積
 */
static double calc_single_sphere_area(
        Sphere* sphere, SpheresCollision* col,
        double margin, const Vec3d* unit_points, size_t num_points) {
    const double r = sphere->radius + margin;
    ConstLinkedList* list = get_collided_spheres(col, sphere, margin);
    Sphere* array = list_to_array(list, sizeof(Sphere));
    const size_t array_num = list->num;
    free_list(list);
    int count = num_points;
    for (size_t i = 0; i < num_points; ++i) {
        const Vec3d ori_p = vec3d_mul(unit_points + i, r);
        const Vec3d p = vec3d_add(&sphere->center, &ori_p);
        for (Sphere* s = array; s < array + array_num; ++s) {
            const Vec3d d = vec3d_sub(&s->center, &p);
            if ((d.x * d.x + d.y * d.y + d.z * d.z)
                    < pow(s->radius + margin, 2)) {
                --count;
                break;
            }
        }
    }
    free(array);
    return pow(r, 2) * PI * 4.0 * count / num_points;
}

/**
 * 複数の球からなる立体に含まれる指定された1つの球の占める表面積を
 * 複数のmarginを指定して計算する.
 * @param [out] out_areas 長さmarginsの配列
 *                        入力marginの順に対応する表面積を出力する.
 * @param sphere 計算対象の球
 * @param col 立体を構成する球が登録された衝突判定オブジェクト
 * @param margins 衝突判定対象の2つの球の両方の半径を指定値分加算する.
 *                複数の値を昇順で与える.
 * @param num_margin marginsの配列長さ
 * @param unit_points 中心原点,半径1の球面をほぼ均等に覆う点配列
 * @param num_points unit_pointsが保持する点の数
 * @param work_buf 計算バッファ サイズは
 *                 num_margin * (sizeof(int) + sizeof(size_t)
 *                               + sizeof(Sphere**)
 *                               + LEN_COL_BUF * sizeof(Sphere*))
 * @return 複数の球からなる立体に含まれる指定された1つの球の占める表面積
 */
static void calc_single_sphere_area_multi_margin(
        double* out_areas,
        const Sphere* sphere, SpheresCollision* col,
        const double* margins, size_t num_margin,
        const Vec3d* unit_points, size_t num_points,
        char* work_buf) {
    size_t* num_cols = (size_t*)work_buf;
    work_buf += num_margin * sizeof(size_t);
    Sphere*** cols = (Sphere***)work_buf;
    work_buf += num_margin * sizeof(Sphere**);
    for (size_t i = 0; i < num_margin; ++i) {
        cols[i] = (Sphere**)work_buf;
        work_buf += LEN_COL_BUF * sizeof(Sphere*);
        num_cols[i] = get_collided_spheres_to_array(
            col, (const Sphere**)cols[i], LEN_COL_BUF, sphere, margins[i]);
    }
    int* counts = (int*)work_buf;
    for (size_t m = 0; m < num_margin; ++m) {
        counts[m] = 0;
    }
    for (size_t i = 0; i < num_points; ++i) {
        for (size_t m = 0; m < num_margin; ++m) {
            const Vec3d ori_p = vec3d_mul(
                    unit_points + i, sphere->radius + margins[m]);
            const Vec3d p = vec3d_add(&sphere->center, &ori_p);
            if (num_cols[m] <= LEN_COL_BUF) {
                for (Sphere** s = cols[m]; s < cols[m] + num_cols[m]; ++s) {
                    const Vec3d d = vec3d_sub(&(*s)->center, &p);
                    if ((d.x * d.x + d.y * d.y + d.z * d.z)
                            < pow((*s)->radius + margins[m], 2)) {
                        goto margin_break;
                    }
                }
            } else {
                if (exists_collided_sphere_with_point(
                            col, &p, margins[m], sphere)) {
                    break;
                }
            }
            ++(counts[m]);
        }
margin_break: {}
    }
    for (size_t i = 0; i < num_margin; ++i) {
        out_areas[i] = pow((sphere->radius + margins[i]), 2)
            * PI * 4.0 * counts[i] / num_points;
    }
}

/**
 * 複数の球からなる立体に含まれる指定された1つの球のが表面に露出している場合は
 * trueを返す.
 * @param sphere 計算対象の球
 * @param col 立体を構成する球が登録された衝突判定オブジェクト
 * @param margin 衝突判定対象の2つの球の両方の半径を指定値分加算する.
 * @param unit_points 中心原点,半径1の球面をほぼ均等に覆う点配列
 * @param num_points unit_pointsが保持する点の数
 * @return 指定された球が表面に露出している場合はtrue, それ以外はfalse
 */
static bool is_surface_single_sphere(
        Sphere* sphere, SpheresCollision* col,
        double margin, const Vec3d* unit_points, size_t num_points) {
    bool result = false;
    const double r = sphere->radius + margin;
    ConstLinkedList* list = get_collided_spheres(col, sphere, margin);
    Sphere* array = list_to_array(list, sizeof(Sphere));
    const size_t array_num = list->num;
    free_list(list);
    for (size_t i = 0; i < num_points; ++i) {
        const Vec3d ori_p = vec3d_mul(unit_points + i, r);
        const Vec3d p = vec3d_add(&sphere->center, &ori_p);
        for (Sphere* s = array; s < array + array_num; ++s) {
            const Vec3d d = vec3d_sub(&s->center, &p);
            if ((d.x * d.x + d.y * d.y + d.z * d.z)
                    < pow(s->radius + margin, 2)) {
                goto loop_continue;
            }
        }
        result = true;
        break;
loop_continue:
        continue;
    }
    free(array);
    return result;
}

static double calc_single_sphere_area_tree(
        Sphere* sphere, SpheresCollision* col,
        double margin, const Vec3d* unit_points, size_t num_points) {
    const double r = sphere->radius + margin;
    int count = num_points;
    for (size_t i = 0; i < num_points; ++i) {
        const Vec3d ori_p = vec3d_mul(unit_points + i, r);
        const Vec3d p = vec3d_add(&sphere->center, &ori_p);
        if (exists_collided_sphere_with_point(col, &p, margin, sphere)) {
            --count;
        }
    }
    return pow(r, 2) * PI * 4.0 * count / num_points;
}

/**
 * 複数の球からなる立体の表面積を計算する.
 * 球同士の衝突判定に入力球を登録済みの衝突判定オブジェクトを使用する.
 * 計算は球面を指定数で分割した近似値で行う.
 * @param spheres 球の配列
 * @param n_spheres 入力球の数
 * @param col 入力球が登録された衝突判定オブジェクト
 * @param margin 衝突判定対象の2つの球の両方の半径を指定値分加算する.
 * @param plot_num 1つの球面を指定数で分割して近似する
 * @return 入力球集合の重複分を省いた合計表面積
 */
double calc_spheres_area_from_collision(
        Sphere* spheres, size_t n_spheres, SpheresCollision* col,
        double margin, size_t num_plot) {
    Vec3d* unit_points = create_sphere_surface_points(num_plot);
    double area = 0.0;
    for (Sphere* s = spheres; s < spheres + n_spheres; ++s) {
        //area += calc_single_sphere_area(s, col, margin, unit_points, num_plot);
        area += calc_single_sphere_area_tree(s, col, margin, unit_points, num_plot);
    }
    free(unit_points);
    return area;
}

void calc_spheres_area_multi_margin(
        double* out_areas,
        void* sphere_iter, const void* (iter_next)(void*),
        SpheresCollision* col, const double* margins,
        size_t num_margin, size_t num_plot) {
    Vec3d* unit_points = create_sphere_surface_points(num_plot);
    double* buf_areas = malloc(num_margin * sizeof(double));
    for (size_t i = 0; i < num_margin; ++i) {
        out_areas[i] = 0.0;
    }
    char* work_buf = malloc(num_margin
            * (sizeof(int) + sizeof(size_t)
               + sizeof(Sphere**) + LEN_COL_BUF * sizeof(Sphere*)));
    for (const Sphere* s = (const Sphere*)iter_next(sphere_iter); s != NULL;
            s = (const Sphere*)iter_next(sphere_iter)) {
        calc_single_sphere_area_multi_margin(
                buf_areas, s, col, margins, num_margin,
                unit_points, num_plot, work_buf);
        for (size_t i = 0; i < num_margin; ++i) {
            out_areas[i] += buf_areas[i];
        }
    }
    free(work_buf);
    free(buf_areas);
    free(unit_points);
}

/**
 * @param [out] surface_flag 長さn_spheresの配列,
 *                           露出している球はtrueを出力,それ以外にfalseを出力.
 */
void search_surface_spheres_from_collision(
        bool* surface_flag, Sphere* spheres, size_t n_spheres,
        SpheresCollision* col, double margin, size_t num_plot) {
    Vec3d* unit_points = create_sphere_surface_points(num_plot);
    for (size_t i = 0; i < n_spheres; ++i) {
        surface_flag[i] = is_surface_single_sphere(
                &spheres[i], col, margin, unit_points, num_plot);
    }
    free(unit_points);
}

/**
 * 複数の球からなる立体の表面積を計算する.
 * 計算は球面を指定数で分割した近似値で行う.
 * @param spheres 球の配列
 * @param n_spheres 入力球の数
 * @param margin 衝突判定対象の2つの球の両方の半径を指定値分加算する.
 * @param plot_num 1つの球面を指定数で分割して近似する
 * @return 入力された複数の球からなる立体の表面積
 */
double calc_spheres_area(Sphere* spheres, size_t n_spheres,
                         double margin, size_t num_plot) {
    SpheresCollision* col = create_spheres_collision(spheres, n_spheres);
    double area = calc_spheres_area_from_collision(
            spheres, n_spheres, col, margin, num_plot);
    free_spheres_collision(col);
    return area;
}


/**
 * 中心原点,半径1の球面をほぼ均等に覆う点配列を作成する.
 * @param n 生成する点数
 * @return 中心原点,半径1の球面をほぼ均等に覆う点配列
 */
Vec3d* create_sphere_surface_points(size_t n) {
    Vec3d* points = malloc(n * sizeof(Vec3d));
    const double c = 3.6 / sqrt(n);
    const double cos_theta_inc = 2.0 / (n - 1);
    double cos_theta = -1.0 + cos_theta_inc;
    double sin_theta = sqrt(1 - cos_theta * cos_theta);
    double phi = c / sin_theta;
    for (int i = 2; i < n - 2; ++i) {
        cos_theta += cos_theta_inc;
        double sin_theta = sqrt(1 - cos_theta * cos_theta);
        phi += c / sin_theta;
        points[i].x = sin_theta * cos(phi);
        points[i].y = sin_theta * sin(phi);
        points[i].z = cos_theta;
    }
    points[0].x = (points[2].x + points[4].x + points[6].x) / 3;
    points[0].y = (points[2].y + points[4].y + points[6].y) / 3;
    points[0].z = (points[2].z + points[4].z + points[6].z) / 3;
    points[1].x = (points[2].x + points[6].x + points[8].x) / 3;
    points[1].y = (points[2].y + points[6].y + points[8].y) / 3;
    points[1].z = (points[2].z + points[6].z + points[8].z) / 3;
    points[n-1].x = (points[n-3].x + points[n-5].x + points[n-7].x) / 3;
    points[n-1].y = (points[n-3].y + points[n-5].y + points[n-7].y) / 3;
    points[n-1].z = (points[n-3].z + points[n-5].z + points[n-7].z) / 3;
    points[n-2].x = (points[n-3].x + points[n-7].x + points[n-9].x) / 3;
    points[n-2].y = (points[n-3].y + points[n-7].y + points[n-9].y) / 3;
    points[n-2].z = (points[n-3].z + points[n-7].z + points[n-9].z) / 3;
    const size_t v[4] = {0, 1, n-1, n-2};
    for (size_t i = 0; i < 4; ++i) {
        size_t j = v[i];
        double d = 1.0 / sqrt(points[j].x * points[j].x
                              + points[j].y * points[j].y
                              + points[j].z * points[j].z);
        points[j].x *= d;
        points[j].y *= d;
        points[j].z *= d;
    }
    return points;
}
