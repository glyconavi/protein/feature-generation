#ifndef NEIGHBOR_DIF_H_
#define NEIGHBOR_DIF_H_

typedef struct {
    double d;
    const void* val;
} NearestResult;

#endif /* NEIGHBOR_DIF_H_ */
