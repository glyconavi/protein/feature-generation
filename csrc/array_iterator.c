#include "array_iterator.h"

const void* array_iterator_next(void* iter) {
    ArrayIterator* it = (ArrayIterator*)iter;
    const void* next = it->p;
    if (next != it->end) {
        it->p += it->size;
        return next;
    } else {
        return NULL;
    }
}

ArrayIterator create_array_iterator(
        const void* array,
        size_t num,
        size_t size) {
    const char* p = (const char*)array;
    return (ArrayIterator) {
        .p = p,
        .end = p + num * size,
        .size = size,
    };
}
