#include "vptree.h"
#include "sort.h"
#include "array_iterator.h"
#include <stddef.h>
#include <stdlib.h>

#define VP_LEAF_SIZE 32

typedef double (*DistanceFunc)(const void*, const void*);

struct _OtherNode;
struct _MemGetter;
struct _BranchNodeData;
struct _LeafNodeData;

typedef struct _VpTree {
    struct _OtherNode* root;
    double (*distance_func)(const void*, const void*);
    struct _MemGetter* mem;
} VpTree;


/**
 * 球領域を表すノード
 */
typedef struct _SphereNode {
    struct _BranchNodeData *branch;
    struct _LeafNodeData *leaf;
} SphereNode;

/**
 * 球外領域を表すノード
 */
typedef struct _OtherNode {
    struct _BranchNodeData *branch;
    struct _LeafNodeData *leaf;
    const void* value;
} OtherNode;

/**
 * 枝ノードが保持するデータ
 */
typedef struct _BranchNodeData {
    double radius;
    SphereNode* child_sphere;
    OtherNode* child_other;
} BranchNodeData;

typedef struct _LeafNodeData {
    const void** values;
    size_t num_values;
} LeafNodeData;

/**
 * VP木制作時の一時データ型
 */
typedef struct {
    double d;
    const void* val;
} DistVal;

static int compare_dist_val(const void *a, const void *b) {
    double diff = ((DistVal*)a)->d - ((DistVal*)b)->d;
    if (diff < 0) {
        return -1;
    } else if (diff > 0) {
        return 1;
    } else {
        return 0;
    }
}

/**
 * メモリアロケーター
 * 未実装
 */
typedef struct _MemGetter {
} MemGetter;

static SphereNode* malloc_sphere_node(
        MemGetter* mem, BranchNodeData* branch, LeafNodeData* leaf) {
    SphereNode* p = (SphereNode*)malloc(sizeof(SphereNode));
    p->branch = branch;
    p->leaf = leaf;
    return p;
}

static OtherNode* malloc_other_node(
        MemGetter* mem, const void* value,
        BranchNodeData* branch, LeafNodeData* leaf) {
    OtherNode* p = (OtherNode*)malloc(sizeof(OtherNode));
    p->branch = branch;
    p->leaf = leaf;
    p->value = value;
    return p;
}

static BranchNodeData* malloc_branch_node_data(
        MemGetter* mem, double radius, SphereNode* child_sphere, OtherNode* child_other) {
    BranchNodeData* p = (BranchNodeData*)malloc(sizeof(BranchNodeData));
    p->radius = radius;
    p->child_other = child_other;
    p->child_sphere = child_sphere;
    return p;
}

static LeafNodeData* malloc_leaf_node_data(
        MemGetter* mem, DistVal* work_start, DistVal* work_end) {
    LeafNodeData* p = (LeafNodeData*)malloc(sizeof(BranchNodeData));
    p->num_values = work_end - work_start;
    p->values = (const void**)malloc(p->num_values * sizeof(void*));
    for (size_t i = 0; i < p->num_values; ++i) {
        p->values[i] = work_start[i].val;
    }
    return p;
}

static void free_branch_node_data(MemGetter* mem, BranchNodeData* node);

static void free_leaf_node_data(MemGetter* mem, LeafNodeData* node) {
    if (node != NULL) {
        free(node->values);
        free(node);
    }
}

static void free_sphere_node(MemGetter* mem, SphereNode* node) {
    if (node != NULL) {
        free_leaf_node_data(mem, node->leaf);
        free_branch_node_data(mem, node->branch);
        free(node);
    }
}

static void free_other_node(MemGetter* mem, OtherNode* node) {
    if (node != NULL) {
        free_leaf_node_data(mem, node->leaf);
        free_branch_node_data(mem, node->branch);
        free(node);
    }
}

static void free_branch_node_data(MemGetter* mem, BranchNodeData* node) {
    if (node != NULL) {
        free_sphere_node(mem, node->child_sphere);
        free_other_node(mem, node->child_other);
        free(node);
    }
}

static LeafNodeData* create_leaf_node_data(
        MemGetter* mem, DistVal* work_start, DistVal* work_end) {
    if (work_start != work_end) {
        return malloc_leaf_node_data(mem, work_start, work_end);
    } else {
        return NULL;
    }
}

static OtherNode* createOtherNode(
        MemGetter* mem, DistanceFunc distance_func,
        const void* standard, DistVal* work_start, DistVal* work_end,
        DistVal* work_sort);

static SphereNode* createSphereNode(
        MemGetter* mem, DistanceFunc distance_func,
        DistVal* work_start, DistVal* work_end,
        DistVal* work_sort) {
    if (work_start + VP_LEAF_SIZE >= work_end) {
        return malloc_sphere_node(
                mem, NULL, create_leaf_node_data(mem, work_start, work_end));
    }
    DistVal* mid = work_start
        + ((work_end - work_start + 1) / 2 - 1);
    return malloc_sphere_node(mem,
            malloc_branch_node_data(
                mem, mid->d,
                createSphereNode(mem, distance_func, work_start, mid,
                                 work_sort),
                createOtherNode(mem, distance_func, mid->val, mid + 1,
                                work_end, work_sort + (mid - work_start))
            ),
            NULL
    );
}

static OtherNode* createOtherNode(
        MemGetter* mem, DistanceFunc distance_func,
        const void* standard, DistVal* work_start, DistVal* work_end,
        DistVal* work_sort) {
    if (work_start + VP_LEAF_SIZE >= work_end) {
        return malloc_other_node(
                mem, standard, NULL,
                create_leaf_node_data(mem, work_start, work_end));
    }
    for (DistVal* p = work_start; p < work_end; ++p) {
        p->d = (*distance_func)(standard, p->val);
    }
    const size_t work_size = work_end - work_start;
    //qsort(work_start, work_size, sizeof(DistVal), compare_dist_val);
    merge_sort_work(work_start, work_size, sizeof(DistVal), compare_dist_val,
                    work_sort);
    DistVal* mid = work_start + ((work_size + 1) / 2 - 1);
    return malloc_other_node(mem,
            standard,
            malloc_branch_node_data(
                mem, mid->d,
                createSphereNode(mem, distance_func, work_start, mid,
                                 work_sort),
                createOtherNode(mem, distance_func, mid->val, mid + 1,
                                work_end, work_sort + (mid - work_start))
            ),
            NULL
    );
}

/**
 * @fn
 * 近傍探索用のVP木を作成する.
 * @param [in] distance_func 要素間の距離関数
 * @param [in] value_start 入力要素の配列の先頭
 * @param [in] num_value 入力要素の要素数
 * @param [in] value_size 入力要素1つのメモリサイズ
 * @return VP木, 入力要素数が0のときはNULL
 */
VpTree* create_vptree(
        DistanceFunc distance_func,
        const void* value_start, size_t num_value, size_t value_size) {
    ArrayIterator iter = create_array_iterator(
            value_start, num_value, value_size);
    return create_vptree_from_iterator(
            distance_func, &iter, array_iterator_next,
            num_value, value_size);
}


/**
 * @fn
 * 近傍探索用のVP木を作成する.
 * @param [in] distance_func 要素間の距離関数
 * @param [in] iter 入力要素のイテレータデータ
 * @param [in] iter_next 入力要素のイテレータnext関数
 * @param [in] num_value 入力要素数
 * @param [in] value_size 入力要素1つのメモリサイズ
 * @return VP木, 入力要素数が0のときはNULL
 */
VpTree* create_vptree_from_iterator(
        DistanceFunc distance_func,
        void* iter, const void* (iter_next)(void*),
        size_t num_value, size_t value_size) {
    if (num_value == 0) {
        return NULL;
    }
    MemGetter* mem = (MemGetter*)malloc(sizeof(MemGetter));
    size_t work_size = num_value - 1;
    DistVal* work = (DistVal*)malloc(work_size * sizeof(DistVal));
    DistVal* work_sort = (DistVal*)malloc(work_size * sizeof(DistVal));
    DistVal* wp = work;
    const void* standard = iter_next(iter);
    for (const void* v = iter_next(iter); v != NULL; v = iter_next(iter)) {
        wp->val = v;
        ++wp;
    }
    OtherNode* root = createOtherNode(
                mem, distance_func, standard, work, work + work_size,
                work_sort);
    free(work_sort);
    free(work);
    VpTree* tree = malloc(sizeof(VpTree));
    tree->root = root;
    tree->distance_func = distance_func;
    tree->mem = mem;
    return tree;
}

/**
 * @fn
 * VP木と保持するリソースを解放する.
 * @param [in,out] self 解放対象
 */
void free_vptree(VpTree* self) {
    if (self != NULL) {
        free_other_node(self->mem, self->root);
        self->root = NULL;
        free(self->mem);
        self->mem = NULL;
        free(self);
    }
}

/**
 * 近傍探索の結果を更新する関数の型
 * @param [inout] result 結果の出力先
 * @param [in] d 新しい候補要素のクエリとの距離
 * @param [in] v 新しい候補要素
 * @return 更新後の新しい距離のしきい値
 */
typedef double (*Updater)(void* result, double d, const void* v);

static double search_branch_data(
        DistanceFunc distance_func, const void* q,
        BranchNodeData* node,
        double standard_d,
        double min_d,
        void* result,
        Updater updater);

static double search_leaf_node_data(
        DistanceFunc distance_func, const void* q,
        LeafNodeData* leaf,
        double min_d,
        void* result,
        Updater updater) {
    for (size_t i = 0; i < leaf->num_values; ++i) {
        const void* v = leaf->values[i];
        const double d = (*distance_func)(v, q);
        if (d < min_d) {
            min_d = (*updater)(result, d, v);
        }
    }
    return min_d;
}

static double search_node(
        DistanceFunc distance_func, const void* q,
        BranchNodeData* branch,
        LeafNodeData* leaf,
        double standard_d,
        double min_d,
        void* result,
        Updater updater) {
    if (branch != NULL) {
        return search_branch_data(
                distance_func, q, branch, standard_d, min_d, result, updater);
    } else if (leaf != NULL){
        return search_leaf_node_data(
                distance_func, q, leaf, min_d, result, updater);
    } else {
        return min_d;
    }
}

static double search_other_node(
        DistanceFunc distance_func, const void* q,
        OtherNode* node,
        double min_d,
        void* result,
        Updater updater) {
    double standard_d = (*distance_func)(node->value, q);
    if (standard_d < min_d) {
        min_d = (*updater)(result, standard_d, node->value);
    }
    return search_node(
            distance_func, q, node->branch, node->leaf,
            standard_d, min_d, result, updater);
}

static double search_branch_data(
        DistanceFunc distance_func, const void* q,
        BranchNodeData* branch,
        double standard_d,
        double min_d,
        void* result,
        Updater updater) {
    if (standard_d < branch->radius) {
        min_d = search_node(
                distance_func, q,
                branch->child_sphere->branch, branch->child_sphere->leaf,
                standard_d, min_d, result, updater);
        if ((standard_d + min_d) >= branch->radius) {
            min_d = search_other_node(
                    distance_func, q, branch->child_other, min_d, result,
                    updater);
        }
    } else {
        min_d = search_other_node(
                distance_func, q, branch->child_other, min_d, result, updater);
        if (standard_d < branch->radius + min_d) {
            min_d = search_node(
                    distance_func, q,
                    branch->child_sphere->branch, branch->child_sphere->leaf,
                    standard_d, min_d, result, updater);
        }
    }
    return min_d;
}

/**
 * 最近傍探索の結果更新用関数
 */
double nearest_updater(void* result, double d, const void* val) {
    NearestResult* r = (NearestResult*)result;
    r->d = d;
    r->val = val;
    return d;
}

/**
 * @fn
 * 最近傍探索を行う
 * @param [in] self VP木
 * @param [in] q クエリ
 */
NearestResult nearest_neighbor_vp(VpTree* self, const void* q) {
    NearestResult result;
    double min_d = nearest_updater(
            &result, (*self->distance_func)(self->root->value, q),
            self->root->value);
    search_node(
            self->distance_func, q,
            self->root->branch, self->root->leaf,
            min_d, min_d, &result, nearest_updater); 
    return result;
}

typedef struct {
    double threshold;
    ConstLinkedList* result;
} VpTreeNearDistanceResult;


/**
 * 指定距離未満の要素を探索するときの結果更新用関数
 */
double near_distance_updater(void* result, double d, const void* val) {
    VpTreeNearDistanceResult* r = (VpTreeNearDistanceResult*)result;
    push_to_list(r->result, val);
    return r->threshold;
}

/**
 * @fn
 * 近傍探索を行う
 * @param [in] self VP木
 * @param [in] q クエリ
 */
ConstLinkedList* near_distance_vp(VpTree* self, const void* q,
                                  double threshold) {
    VpTreeNearDistanceResult result = (VpTreeNearDistanceResult) {
        .threshold = threshold,
        .result = create_list(),
    };
    search_other_node(
        self->distance_func, q,
        self->root,
        threshold,
        &result,
        near_distance_updater);
    return result.result;
}

typedef struct {
    double threshold;
    const void** out_buf;
    size_t len_buf;
    size_t i;
    bool (*ignore_func)(const void*, void*);
    void* context;
} VpTreeNearDistanceBufResult;

/**
 * 指定距離未満の要素を探索するして結果を配列に保存するときの結果更新用関数
 */
double near_distance_buf_updater(void* result, double d, const void* val) {
    VpTreeNearDistanceBufResult* r = (VpTreeNearDistanceBufResult*)result;
    if (r->i < r->len_buf) {
        if (!r->ignore_func(val, r->context)) {
            *(r->out_buf + r->i) = val;
            ++(r->i);
        }
        return r->threshold;
    } else {
        if (!r->ignore_func(val, r->context)) {
            ++(r->i);
        }
        return 0;
    }
}

/**
 * クエリから指定距離未満の要素を列挙して配列に出力する.
 * @param self Vp木
 * @param out_buf 配列の出力先
 * @param len_buf out_bufの最大容量
 * @param q クエリ
 * @param threshold 近傍探索距離
 * @param ignore_func (要素,context)を引数として無視する要素の場合tureを返す
 * @param context ignore_funcの引数として渡す値
 * @return 見つかった要素数、len_bufより多いときは len_buf + 1
 */
size_t near_distance_vp_to_buffer(
        VpTree* self, const void** out_buf, size_t len_buf,
        const void* q, double threshold,
        bool (*ignore_func)(const void*, void*), void* context) {
    VpTreeNearDistanceBufResult result = (VpTreeNearDistanceBufResult) {
        .threshold = threshold,
        .out_buf = out_buf,
        .len_buf = len_buf,
        .i = 0,
        .ignore_func = ignore_func,
        .context = context,
    };
    search_other_node(
        self->distance_func, q,
        self->root,
        threshold,
        &result,
        near_distance_buf_updater);
    return result.i;
}

/**
 * 独自の更新関数で近傍探索を行う.
 * @param [out] result
 * @param [in] self
 * @param [in] q
 * @param [in] updater 返値 新しいしきい値, 引数(result, 候補の距離, 候補要素)
 * @param [in] inital_threshold クエリからの探索距離の初期値、
 *                               指定しない場合は負の値入力する.
 * @param [in] distance_func 距離関数(ツリーの値, クエリ),
 *                           ツリーのの設定関数を使用する場合はNULL
 */
void custom_near_vp(void* result, VpTree* self, const void* q,
                    double (*updater)(void* result, double d, const void* v),
                    double inital_threathold,
                    double (*distance_func)(const void*, const void*)) {
    if (distance_func == NULL) {
        distance_func = self->distance_func;
    }
    /*
    if (inital_threathold < 0) {
        inital_threathold = updater(
                &result, (*self->distance_func)(self->root->value, q),
                self->root->value);
        search_node(
                self->distance_func, q,
                self->root->branch,
                inital_threathold, inital_threathold, result, updater); 
    } else {
    */
        search_other_node(
            distance_func, q,
            self->root,
            inital_threathold,
            result,
            updater);
    //}
}
