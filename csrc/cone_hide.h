#ifndef CONE_HIDE_H_
#define CONE_HIDE_H_
#include "figure3d.h"
#include "linked_list.h"
#include <stdbool.h>
#include <stddef.h>

void clean_spheres(
        const Sphere* center_sphere,
        void* sphere_iter, const void* (iter_next)(void*),
        size_t num_sphere,
        double margin,
        bool* out_mask);

void clean_spheres_from_linked_list(
        ConstLinkedList* other_spheres,
        const Sphere* center_sphere,
        double margin);

#endif /* CONE_HIDE_H_ */
