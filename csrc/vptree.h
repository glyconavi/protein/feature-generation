/**
 * Vantage-Point木による近傍探索
 */
#ifndef VPTREE_H_
#define VPTREE_H_
#include "neighbor_def.h"
#include "linked_list.h"
#include <stddef.h>

typedef struct _VpTree VpTree;

VpTree* create_vptree(
        double (*distance_func)(const void*, const void*),
        const void* value_start, size_t value_num, size_t value_size);

VpTree* create_vptree_from_iterator(
        double (*distance_func)(const void*, const void*),
        void* iter, const void* (iter_next)(void*),
        size_t num_value, size_t value_size);

void free_vptree(VpTree* self);

NearestResult nearest_neighbor_vp(VpTree* self, const void* v);

ConstLinkedList* near_distance_vp(
        VpTree* self, const void* q, double threshold);

size_t near_distance_vp_to_buffer(
        VpTree* self, const void** out_buf, size_t len_buf,
        const void* q, double threshold,
        bool (*ignore_func)(const void*, void*), void* context);

void custom_near_vp(void* result, VpTree* self, const void* q,
                    double (*updater)(void* result, double d, const void* v),
                    double inital_threathold,
                    double (*distance_func)(const void*, const void*));

#endif /* VPTREE_H_ */
