#include "linked_list.h"
#include <stdlib.h>
#include <string.h>

typedef struct _ConstLinkedListNode {
    const void* val;
    struct _ConstLinkedListNode* next;
} ConstLinkedListNode;

ConstLinkedList* create_list() {
    ConstLinkedList* list = malloc(sizeof(ConstLinkedList));
    list->head = NULL;
    list->num = 0;
    return list;
}

/**
 * リストのメモリリソースを解放する.
 */
void free_list(ConstLinkedList* list) {
    if (list != NULL) {
        ConstLinkedListNode* node = list->head;
        while (node != NULL) {
            ConstLinkedListNode* prev = node;
            node = node->next;
            free(prev);
        }
        free(list);
    }
}

void push_to_list(ConstLinkedList* list, const void* val) {
    ConstLinkedListNode* prev_head = list->head;
    list->head = (ConstLinkedListNode*)malloc(sizeof(ConstLinkedListNode));
    list->head->next = prev_head;
    list->head->val = val;
    ++(list->num);
}

/**
 * 入力関数がtrueを返す要素を削除する.
 */
void remove_if_list(ConstLinkedList* list,
                    bool (*cond)(const void*, void*), void* context) {
    ConstLinkedListNode* node = list->head;
    while ((node != NULL) && cond(node->val, context)) {
        list->head = node->next;
        free(node);
        node = list->head;
        --(list->num);
    }
    if (node != NULL) {
        ConstLinkedListNode* prev = node;
        node = node->next;
        while (node != NULL) {
            if (cond(node->val, context)) {
                prev->next = node->next;
                free(node);
                node = prev->next;
                --(list->num);
            } else {
                prev = node;
                node = node->next;
            }
        }
    }
}

/*
static void insert_after_list(
        ConstLinkedList* list, ConstLinkedListNode* prev, const void* val) {
    ConstLinkedListNode* prev_next = prev->next;
    prev->next = (ConstLinkedListNode*)malloc(sizeof(ConstLinkedListNode));
    prev->next->next = prev_next;
    prev->next->val = val;
    ++(list->num);
}
*/

void move_front_2list(ConstLinkedList* dst, ConstLinkedList* src) {
    ConstLinkedListNode* node = src->head;
    src->head = node->next;
    --(src->num);
    ConstLinkedListNode* dst_node = dst->head;
    dst->head = node;
    node->next = dst_node;
    ++(dst->num);
}

ListIterator get_list_iterator(ConstLinkedList* list) {
    return (ListIterator) {
        .node = list->head,
    };
}

const void* list_next(void* itr) {
    ListIterator* it = (ListIterator*)itr;
    const void* v;
    if (it->node != NULL) {
        v = it->node->val;
        it->node = it->node->next;
    } else {
        v = NULL;
    }
    return v;
}

void* list_to_array(ConstLinkedList* list, size_t size) {
    char* array = malloc(list->num * size);
    ListIterator itr = get_list_iterator(list);
    for (size_t i = 0; i < list->num; ++i) {
        memcpy(array + size * i, list_next(&itr), size);
    }
    return array;
}
