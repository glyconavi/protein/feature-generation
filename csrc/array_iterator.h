#ifndef ARRAY_ITERATOR_H_
#define ARRAY_ITERATOR_H_
#include <stddef.h>

typedef struct {
    const char* p;
    const char* end;
    size_t size;
} ArrayIterator;

const void* array_iterator_next(void* iter);

ArrayIterator create_array_iterator(
        const void* array,
        size_t num,
        size_t size);

#endif /* ARRAY_ITERATOR_H_ */
