#include "vptree.h"
#include "near_liner.h"
#include "figure3d.h"
#include "spheres_collision.h"
#include "spherical_surface_plot.h"
#include "sphere_integral.h"
#include "astar_search.h"
#include "surface_distance.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#define PI 3.141592653589793238462643

double dist3d(const void* a, const void* b) {
    Vec3d* va = (Vec3d*)a;
    Vec3d* vb = (Vec3d*)b;
    return sqrt(pow(va->x - vb->x, 2) + pow(va->y - vb->y, 2) + pow(va->z - vb->z, 2));
}

Vec3d* create_random_vec3d(size_t num) {
    Vec3d* data = (Vec3d*)malloc(num * sizeof(Vec3d));
    for (Vec3d* p = data; p < data + num; ++p) {
        p->x = (double)rand() / RAND_MAX;
        p->y = (double)rand() / RAND_MAX;
        p->z = (double)rand() / RAND_MAX;
    }
    return data;
}

Vec3d rand_unit_spherical_surface() {
    const double cos_theta = -2.0 * ((double)rand() / RAND_MAX) + 1.0;
    const double sin_theta = sqrt(1.0 - cos_theta * cos_theta);
    const double phi = 2.0 * PI * ((double)rand() / RAND_MAX);
    return (Vec3d) {
        .x = sin_theta * cos(phi),
        .y = sin_theta * sin(phi),
        .z = cos_theta,
    };
}

Sphere* random_spheres_snake(size_t num) {
    Sphere* data = (Sphere*)malloc(num * sizeof(Sphere));
    const double br = 0.5;
    const double bd = 0.5;
    data->radius = 1.0;
    data->center.x = 0.0;
    data->center.y = 0.0;
    data->center.z = 0.0;
    for (Sphere* p = data + 1; p < data + num; ++p) {
        p->radius = br + (1.0 - br) * ((double)rand() / RAND_MAX);
        double d = (p->radius + (p - 1)->radius) * 0.9
                   * (bd + (1.0 - bd) * ((double)rand() / RAND_MAX));
        Vec3d move = rand_unit_spherical_surface();
        if (move.z < 0) {
            move.z = -move.z;
        }
        p->center.x = (p - 1)->center.x + move.x * d;
        p->center.y = (p - 1)->center.y + move.y * d;
        p->center.z = (p - 1)->center.z + move.z * d;
    }
    return data;
}

static double distance_sphere_center2(const void* a, const void* b) {
    Vec3d* ca = &((Sphere*)a)->center;
    Vec3d* cb = &((Sphere*)b)->center;
    return pow(ca->x - cb->x, 2)
           + pow(ca->y - cb->y, 2)
           + pow(ca->z - cb->z, 2);
}

static double distance_sphere_center(const void* a, const void* b) {
    return sqrt(distance_sphere_center2(a, b));
}

static bool colled_sphere(const Sphere* lhs, const Sphere* rhs) {
    return (((lhs->radius * lhs->radius)
             + (rhs->radius * rhs->radius)
             + lhs->radius * rhs->radius * 2.0)
            > distance_sphere_center2(lhs, rhs));
}

int double_test() {
    size_t num = 1000;
    double margin = 0.5;
    size_t num_slice = 256;
    size_t num_plot = num_slice*num_slice;
    //Sphere* spheres = malloc(num * sizeof(Sphere));
    //spheres[0] = (Sphere) {
    //    .center = (Vec3d) {.x = 0.0, .y = 0.0, .z = 0.0 },
    //    .radius = 0.5,
    //};
    //spheres[1] = (Sphere) {
    //    .center = (Vec3d) {.x = 0.0, .y = 0.0, .z = 1.0 },
    //    .radius = 0.5,
    //};
    //spheres[1] = (Sphere) {
    //    .center = (Vec3d) {.x = 0.7071, .y = 0.0, .z = 0.7071 },
    //    .radius = 0.3,
    //};
    //spheres[1] = (Sphere) {
    //    .center = (Vec3d) {.x = 1.0, .y = 0.0, .z = 0.0 },
    //    .radius = 0.5,
    //};
    //spheres[1] = (Sphere) {
    //    .center = (Vec3d) {.x = 0.2673, .y = 0.5345, .z = -0.8018 },
    //    .radius = 0.5,
    //};
    Sphere* spheres = random_spheres_snake(num);
    //for (size_t i = 0; i < num; ++i) {
    //    printf("sphere %zu (%f, %f, %f) %f\n",
    //           i,
    //           spheres[i].center.x,
    //           spheres[i].center.y,
    //           spheres[i].center.z,
    //           spheres[i].radius
    //           );
    //}
    SpheresCollision* col = create_spheres_collision(spheres, num);
    double area = spherical_surfaces_integral(
        spheres, num, col, margin, num_slice);
    double area2 = calc_spheres_area_from_collision(
            spheres, num, col, margin, num_plot);
    printf("integral = %f\nplot = %f\nper = %f\n", area, area2, area / area2);
    free_spheres_collision(col);
    free(spheres);
    return 0;
}

int integral_test() {
    size_t num = 3;
    double margin = 14;
    size_t num_slice = 64;
    Sphere* spheres = malloc(num * sizeof(Sphere));
    spheres[0] = (Sphere) {
        .center = (Vec3d) {.x = 0.0, .y = 0.0, .z = 0.0 },
        .radius = 0.5,
    };
    spheres[1] = (Sphere) {
        .center = (Vec3d) {.x = 1.0, .y = 0.0, .z = 0.0 },
        .radius = 0.5,
    };
    spheres[2] = (Sphere) {
        .center = (Vec3d) {.x = 8.0, .y = 1.0, .z = 1.0 },
        .radius = 0.5,
    };
    //spheres[3] = (Sphere) {
    //    .center = (Vec3d) {.x = 3.0, .y = 3.0, .z = 0.0 },
    //    .radius = 0.5,
    //};
    //Sphere* spheres = random_spheres_snake(num);
    SpheresCollision* col = create_spheres_collision(spheres, num);

    double area = spherical_surfaces_integral(
        spheres, num, col, margin, num_slice);
    printf("area = %f\n", area);
    free_spheres_collision(col);
    free(spheres);
    return 0;
}

int surface_test() {
    size_t num = 10000;
    double margin = 14;
    size_t num_plot = 32*32;
    //Sphere* spheres = malloc(num * sizeof(Sphere));
    //spheres[0] = (Sphere) {
    //    .center = (Vec3d) {.x = 0.0, .y = 0.0, .z = 0.0 },
    //    .radius = 0.5,
    //};
    //spheres[1] = (Sphere) {
    //    .center = (Vec3d) {.x = 1.0, .y = 0.0, .z = 0.0 },
    //    .radius = 0.5,
    //};
    Sphere* spheres = random_spheres_snake(num);
    SpheresCollision* col = create_spheres_collision(spheres, num);

    double area = calc_spheres_area_from_collision(
            spheres, num, col, margin, num_plot);
    printf("area = %f\n", area);
    free_spheres_collision(col);
    free(spheres);
    return 0;
}

int col_sphere_test() {
    size_t num = 1000000;
    Sphere* spheres = random_spheres_snake(num);
    SpheresCollision* col = create_spheres_collision(spheres, num);
    for (Sphere* p = spheres; p < spheres + num; ++p) {
        ConstLinkedList* sp = get_collided_spheres(col, p, 0.0);
        ListIterator itr = get_list_iterator(sp);
        int col_count = 0;
        while (1) {
            const Sphere* cp = list_next(&itr);
            if (cp == NULL) {
                break;
            }
            ++col_count;
            if (!colled_sphere(p, cp)) {
                printf("error (sphere %f, (%f, %f, %f))\n"
                       "      (sphere %f, (%f, %f, %f))\n"
                       "      d = %f\n",
                       p->radius, p->center.x, p->center.y, p->center.z,
                       cp->radius, cp->center.x, cp->center.y, cp->center.z,
                       distance_sphere_center(p, cp));
            }
        }
        free_list(sp);
        /*
        int simple_count = 0;
        for (Sphere* p2 = spheres; p2 < spheres + num; ++p2) {
            if (p == p2) {
                continue;
            }
            if (colled_sphere(p, p2)) {
                ++simple_count;
            }
        }
        if (col_count != simple_count) {
            printf("error count %d %d\n", col_count, simple_count);
        }
        */
    }
    free_spheres_collision(col);
    free(spheres);
    return 0;
}

int near_distance_test() {
    size_t n_value = 30000;
    Vec3d* data = create_random_vec3d(n_value);
    int n_query = 1000;
    Vec3d* querys = create_random_vec3d(n_query);
    const double distance = 0.1;

    VpTree* vptree = create_vptree(vec3d_distance_void,
            data, n_value, sizeof(*data));
    for (Vec3d* p = querys; p < querys + n_query; ++p) {
        ConstLinkedList* result = near_distance_vp(vptree, p, distance);
        int t_count = 0;
        ListIterator itr = get_list_iterator(result);
        const Vec3d* v = list_next(&itr);
        while (v != NULL) {
            const double d = dist3d(p, v);
            if (d >= distance) {
                printf("Error %f\n", d);
            }
            ++t_count;
            v = list_next(&itr);
        }
        free_list(result);
        int d_count = 0;
        for (Vec3d* v = data; v < data + n_value; ++v) {
            const double d = dist3d(p, v);
            if (d < distance) {
                ++d_count;
            }
        }
        if (t_count != d_count) {
            printf("Count Error\n");
        }
    }
    free_vptree(vptree);
    free(querys);
    free(data);
    return 0;
}

int nearest_test() {
    size_t n_value = 1000;
    //size_t n_value = 10000;
    Vec3d* data = create_random_vec3d(n_value);
    int n_query = 1000;
    Vec3d* querys = create_random_vec3d(n_query);

    VpTree* vptree = create_vptree(dist3d, data, n_value, sizeof(*data));
    double v = 0.0;
    for (Vec3d* p = querys; p < querys + n_query; ++p) {
        NearestResult ret = nearest_neighbor_vp(vptree, p);
        v += ret.d;
        NearestResult ret_liner = nearest_neighbor_liner(
                dist3d, data, data + n_value, sizeof(*data), p);
        if (dist3d(p, ret.val) != ret.d) {
            printf("fail dist %f %f\n", dist3d(p, ret.val), ret.d);
        }
        if (ret.val != ret_liner.val) {
            printf("chigau %f %f\n", ret.d, ret_liner.d);
        }
    }

    free_vptree(vptree);
    free(querys);
    free(data);
    return 0;
}

static double heuristic_func(const void* lhs, const void* rhs) {
    const Sphere* lsp = (const Sphere*)lhs;
    const Sphere* rsp = (const Sphere*)rhs;
    return vec3d_distance(&lsp->center, &rsp->center);
}

static double sphere_center_distance(const void* lhs, const void* rhs) {
    return vec3d_distance(&((const Sphere*)lhs)->center,
                          &((const Sphere*)rhs)->center);
}

typedef struct {
    SpheresCollision* col;
    double margin;
} NeighborContext;

static ASterNeighbor* neighbor_func(
        size_t* num_neg, const void* node, void* context) {
    const Sphere* sphere = (const Sphere*)node;
    NeighborContext* con = (NeighborContext*)context;
    ConstLinkedList* neg_list =
        get_collided_spheres(con->col, sphere, con->margin);
    *num_neg = neg_list->num;
    ASterNeighbor* buf = malloc((*num_neg) * sizeof(ASterNeighbor));
    ListIterator itr = get_list_iterator(neg_list);
    size_t i = 0;
    for (const Sphere* neg = list_next(&itr); neg != NULL;
            neg = list_next(&itr)) {
        buf[i].node = neg;
        buf[i].d = sphere_center_distance(sphere, neg);
        ++i;
    }
    free_list(neg_list);
    return buf;
}

int test_aster() {
    size_t num = 1000;
    Sphere* spheres = random_spheres_snake(num);
    SpheresCollision* col = create_spheres_collision(spheres, num);
    NeighborContext context = (NeighborContext) {
        .col = col,
        .margin = 0.5,
    };
    double d = aster_search_from_iterator(
        &spheres[0], &spheres[num - 1],
        sphere_center_distance,
        neighbor_func,
        &context);
    double d_short = aster_search_from_short_array(
            spheres, num, sizeof(Sphere),
            0, num - 1,
            heuristic_func, neighbor_func, &context);
    free(spheres);
    printf("d  = %f\n", d);
    printf("ds = %f\n", d_short);
    return 0;
}

int surface_distance_test() {
    size_t num = 5;
    double margin = 0.5;
    size_t num_plot = 64*64;
    Sphere* spheres = malloc(num * sizeof(Sphere));
    spheres[0] = (Sphere) {
        .center = (Vec3d) {.x = 0.0, .y = 0.0, .z = 0.0 },
        .radius = 0.5,
    };
    spheres[1] = (Sphere) {
        .center = (Vec3d) {.x = 0.0, .y = 0.0, .z = 1.0 },
        .radius = 0.5,
    };
    spheres[2] = (Sphere) {
        .center = (Vec3d) {.x = 0.7071, .y = 0.0, .z = 0.7071 },
        .radius = 0.3,
    };
    spheres[3] = (Sphere) {
        .center = (Vec3d) {.x = 1.0, .y = 0.0, .z = 0.0 },
        .radius = 0.5,
    };
    spheres[4] = (Sphere) {
        .center = (Vec3d) {.x = 10.0, .y = 10.0, .z = 10.0 },
        //.center = (Vec3d) {.x = 1.1, .y = 0.0, .z = 0.0 },
        .radius = 0.5,
    };
    //Sphere* spheres = random_spheres_snake(num);
    bool surface_flag[5];
    surface_flag[0] = true;
    surface_flag[1] = true;
    surface_flag[2] = true;
    surface_flag[3] = true;
    surface_flag[4] = true;
    double d = surface_distance(
            spheres, num, surface_flag,
            0, 4, margin);
    printf("distance = %f\n", d);
    free(spheres);
    return 0;
}

int main() {
    //return surface_distance_test();
    return test_aster();
    //return double_test();
    //return integral_test();
    //return surface_test();
    //return col_sphere_test();
    //return near_distance_test();
    //return nearest_test();
}
