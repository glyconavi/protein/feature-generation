#ifndef LIB_INTERFACE_H_
#define LIB_INTERFACE_H_
#include "figure3d.h"

typedef struct {
    double area_1_4;
    double area_7_0;
    double area_14_0;
} SurfaceAreaResult;

typedef struct {
    int start; 
    int end;
} IntIndexPair;

int calc_surface_features(
        SurfaceAreaResult** area_result,
        double** distance_result,
        Sphere* spheres, int num_spheres,
        IntIndexPair* area_ranges, int num_ranges,
        IntIndexPair* distance_idxs, int num_distances);

void free_surface_result(
        SurfaceAreaResult** area_result,
        double** distance_result);

#endif /* LIB_INTERFACE_H_ */
