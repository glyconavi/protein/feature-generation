#ifndef SURFACE_DISTANCE_H_
#define SURFACE_DISTANCE_H_
#include "spheres_collision.h"
#include "figure3d.h"
#include <stdbool.h>

double distance_via_sphere_centers(
        const Sphere* start, const Sphere* end,
        SpheresCollision* col,
        double margin);

SpheresCollision* create_surface_collision(
        const Sphere* spheres, size_t num,
        const bool* surface_flag);

double surface_distance(
        const Sphere* spheres, size_t num,
        const bool* surface_flag,
        size_t start_idx, size_t end_idx,
        double margin);

double calc_surface_distance_from_collision(
        SpheresCollision* surface_col,
        const Sphere* spheres,
        const bool* surface_flag,
        size_t start_idx, size_t end_idx,
        double margin);

#endif /* SURFACE_DISTANCE_H_ */
