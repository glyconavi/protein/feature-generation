#include "mask_iterator.h"

const void* mask_array_iterator_next(void* itr) {
    MaskArrayIterator* m = (MaskArrayIterator*)itr;
    const void* ret = NULL;
    for (; m->p != m->end; m->p += m->size) {
        const bool b = *(m->mask++);
        if (b) {
            ret = m->p;
            m->p += m->size;
            break;
        }
    }
    return ret;
}
