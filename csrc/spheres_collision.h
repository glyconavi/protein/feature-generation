#ifndef SPHERE_COLLISION_H_
#define SPHERE_COLLISION_H_
#include "linked_list.h"
#include "figure3d.h"
#include <stdbool.h>

typedef struct SpheresCollision_ SpheresCollision ;

SpheresCollision* create_spheres_collision(Sphere* spheres, size_t num);
SpheresCollision* create_spheres_collision_from_iterator(
        void* iter, const void* (iter_next)(void*), size_t num);

void free_spheres_collision(SpheresCollision* self);

ConstLinkedList* get_collided_spheres(
        SpheresCollision* self, const Sphere* sphere, double margin);

size_t get_collided_spheres_to_array(
        SpheresCollision* self,
        const Sphere** out_array, size_t len_array,
        const Sphere* sphere, double margin);

const Sphere* get_nearest_center_sphere(
        SpheresCollision* self, const Sphere* sphere);

bool exists_collided_sphere_with_point(
        SpheresCollision* self, const Vec3d* point, double margin,
        const Sphere* ignore);

#endif /* SPHERE_COLLISION_H_ */
