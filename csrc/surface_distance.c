#include "surface_distance.h"
#include "astar_search.h"
#include "mask_iterator.h"
#include <stdlib.h>

static double sphere_center_distance(const void* lhs, const void* rhs) {
    return vec3d_distance(&((const Sphere*)lhs)->center,
                          &((const Sphere*)rhs)->center);
}

typedef struct {
    SpheresCollision* col;
    double margin;
} NeighborContext;

static ASterNeighbor* neighbor_func(
        size_t* num_neg, const void* node, void* context) {
    const Sphere* sphere = (const Sphere*)node;
    NeighborContext* con = (NeighborContext*)context;
    ConstLinkedList* neg_list =
        get_collided_spheres(con->col, sphere, con->margin);
    *num_neg = neg_list->num;
    ASterNeighbor* buf = malloc((*num_neg) * sizeof(ASterNeighbor));
    ListIterator itr = get_list_iterator(neg_list);
    size_t i = 0;
    for (const Sphere* neg = list_next(&itr); neg != NULL;
            neg = list_next(&itr)) {
        buf[i].node = neg;
        buf[i].d = sphere_center_distance(sphere, neg);
        ++i;
    }
    free_list(neg_list);
    return buf;
}

/**
 * 入力球集合の接触している球の中心を辿った場合の2球の最短距離を求める.
 * @param [in] start
 * @param [in] end
 * @param [in] col
 * @param [in] margin
 * @return 入力球集合の接触している球の中心を辿った場合のstart-end球の最短距離
 */
double distance_via_sphere_centers(
        const Sphere* start, const Sphere* end,
        SpheresCollision* col,
        double margin) {
    NeighborContext context = (NeighborContext) {
        .col = col,
        margin = margin,
    };
    return aster_search_from_iterator(
        start, end,
        sphere_center_distance,
        neighbor_func,
        &context);
}

SpheresCollision* create_surface_collision(
        const Sphere* spheres, size_t num,
        const bool* surface_flag) {
    MaskArrayIterator itr = (MaskArrayIterator) {
        .size = sizeof(Sphere),
        .p = (const char*)spheres,
        .end = (const char*)(spheres + num),
        .mask = surface_flag,
    };
    size_t num_surface = 0;
    for (size_t i = 0; i < num; ++i) {
        if (surface_flag[i]) {
            ++num_surface;
        }
    }
    return create_spheres_collision_from_iterator(
            &itr, mask_array_iterator_next, num_surface);
}

double surface_distance(
        const Sphere* spheres, size_t num,
        const bool* surface_flag,
        size_t start_idx, size_t end_idx,
        double margin) {
    SpheresCollision* surface_col = create_surface_collision(
            spheres, num, surface_flag);
    double d = calc_surface_distance_from_collision(
        surface_col, spheres, surface_flag, start_idx, end_idx, margin);
    free_spheres_collision(surface_col);
    return d;
}

double calc_surface_distance_from_collision(
        SpheresCollision* surface_col,
        const Sphere* spheres,
        const bool* surface_flag,
        size_t start_idx, size_t end_idx,
        double margin) {
    const Sphere* start = &spheres[start_idx];
    if (!surface_flag[start_idx]) {
        start = get_nearest_center_sphere(surface_col, start);
    }
    const Sphere* end = &spheres[end_idx];
    if (!surface_flag[end_idx]) {
        end = get_nearest_center_sphere(surface_col, end);
    }
    double d = distance_via_sphere_centers(
        start, end, surface_col, margin);
    return d;
}
