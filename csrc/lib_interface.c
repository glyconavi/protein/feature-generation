#include "lib_interface.h"
#include "spheres_collision.h"
#include "spherical_surface_plot.h"
#include "surface_distance.h"
#include "array_iterator.h"
#include <stdlib.h>

int calc_surface_features(
        SurfaceAreaResult** area_result,
        double** distance_result,
        Sphere* spheres, int num_spheres,
        IntIndexPair* area_ranges, int num_ranges,
        IntIndexPair* distance_idxs, int num_distances) {
    *area_result = malloc(num_ranges * sizeof(SurfaceAreaResult));
    *distance_result = malloc(num_distances * sizeof(double));

    SpheresCollision* col = create_spheres_collision(spheres, num_spheres);
    const size_t num_margin = 3;
    double margins[3] = { 1.4, 7.0, 14.0 };
    double areas[3];
    for (size_t i = 0; i < num_ranges; ++i) {
        ArrayIterator iter = create_array_iterator(
                spheres + area_ranges[i].start,
                area_ranges[i].end - area_ranges[i].start,
                sizeof(Sphere));
        calc_spheres_area_multi_margin(
                areas, &iter, array_iterator_next,
                col, margins, num_margin, 64*64);
        (*area_result)[i].area_1_4 = areas[0];
        (*area_result)[i].area_7_0 = areas[1];
        (*area_result)[i].area_14_0 = areas[2];
    }

    const double surface_margin = 1.4;
    bool* surface_flag = malloc(num_spheres * sizeof(bool));
    search_surface_spheres_from_collision(
            surface_flag, spheres, num_spheres, col, surface_margin, 64*64);
    SpheresCollision* surface_col = create_surface_collision(
            spheres, num_spheres, surface_flag);
    for (size_t i = 0; i < num_distances; ++i) {
        (*distance_result)[i] = calc_surface_distance_from_collision(
                surface_col, spheres, surface_flag,
                distance_idxs[i].start, distance_idxs[i].end, surface_margin);
    }
    free(surface_flag);
    free_spheres_collision(surface_col);
    return 0;
}

void free_surface_result(
        SurfaceAreaResult** area_result,
        double** distance_result) {
    if (*area_result != NULL) {
        free(*area_result);
    }
    if (*distance_result != NULL) {
        free(*distance_result);
    }
}
