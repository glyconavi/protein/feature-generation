/** 球同士の衝突面となる円の影を円錐で定義し重なりを計算する */
#include "cone_hide.h"
#include "figure3d.h"
#include <stdlib.h>

/** 頂点原点 高さ無限の円錐*/
typedef struct {
    /** 中心直線の単位ベクトル */
    Vec3d center;
    /** 頂点から高さ1の円の半径*/
    double radius;
    size_t idx;
} UnitCone;

static inline double calc_sphere_col_cone_radius(
        double r1_2, double r2_2, double c2, double c2_inv) {
    double r2_sub = r1_2 - r2_2;
    return sqrt(r1_2 - 0.25 * (r2_sub*r2_sub * c2_inv + 2.0 * r2_sub + c2));
}

static bool covered_cone(
        const Vec3d* cover_center, double cover_radius,
        const Vec3d* center, double radius) {
    double x = cover_center->x * center->x
        + cover_center->y * center->y
        + cover_center->z * center->z;
    double y = sqrt(1.0 - x * x);
    double edge1_x = x - radius * y;
    double edge1_y = y + radius * x;
    double edge2_x = x + radius * y;
    double edge2_y = y - radius * x;
    return (((cover_radius * edge1_x - edge1_y) >= 0)
            && ((cover_radius * edge2_x - edge2_y) >= 0)
            && (cover_radius * edge1_x + edge1_y) >= 0
            && (cover_radius * edge2_x + edge2_y) >= 0);
}

static int compare_cone(const void* lhs, const void* rhs) {
    const UnitCone* cl = (const UnitCone*)lhs;
    const UnitCone* cr = (const UnitCone*)rhs;
    if (cl->radius > cr->radius) {
        return -1;
    } else if (cl->radius < cr->radius) {
        return 1;
    } else {
        return 0;
    }
}

void clean_spheres(const Sphere* center_sphere,
        void* sphere_iter, const void* (iter_next)(void*),
        size_t num_sphere,
        double margin,
        bool* out_mask) {
    UnitCone* cones = malloc(num_sphere * sizeof(UnitCone));
    const double r = center_sphere->radius + margin;
    const double r2 = r*r;
    const double r_inv = 1.0 / r;
    for (size_t i = 0; i < num_sphere; ++i) {
        const Sphere* sp = iter_next(sphere_iter);
        Vec3d v = vec3d_sub(&sp->center, &center_sphere->center);
        const double c_norm2 = v.x * v.x + v.y * v.y + v.z * v.z;
        const double c_norm = sqrt(c_norm2);
        const double r2_2 = pow(sp->radius + margin, 2);
        cones[i].center = vec3d_mul(&v, 1.0 / c_norm);
        cones[i].radius = calc_sphere_col_cone_radius(
                r2, r2_2, c_norm2, 1.0 / c_norm2);
        cones[i].idx = i;
    }
    qsort(cones, num_sphere, sizeof(UnitCone), compare_cone);
    for (size_t i = 0; i < num_sphere; ++i) {
        out_mask[i] = true;
    }
    for (size_t i = 0; i < num_sphere - 1; ++i) {
        if (!out_mask[cones[i].idx]) {
            continue;
        }
        for (size_t j = i + 1; j < num_sphere; ++j) {
            if (!out_mask[cones[j].idx]) {
                continue;
            }
            if (covered_cone(&(cones[i].center), cones[i].radius,
                             &(cones[j].center), cones[j].radius)) {
                out_mask[cones[j].idx] = false;
            }
        }
    }
    free(cones);
}

typedef struct {
    size_t i;
    const bool* mask;
} ListMaskContext;

bool remove_mask_cond(const void* val, void* context) {
   ListMaskContext* c = (ListMaskContext*)context; 
   return !c->mask[(c->i)++];
}

/**
 * 他の球の影になっている球を除外する.
 */
void clean_spheres_from_linked_list(
        ConstLinkedList* other_spheres,
        const Sphere* center_sphere,
        double margin) {
    bool* list_mask = malloc(other_spheres->num * sizeof(bool));
    ListIterator itr = get_list_iterator(other_spheres);
    clean_spheres(center_sphere, &itr, list_next, other_spheres->num, margin,
                  list_mask);
    ListMaskContext context = (ListMaskContext) {.i = 0, .mask = list_mask };
    remove_if_list(other_spheres, remove_mask_cond, &context);
    free(list_mask);
}
