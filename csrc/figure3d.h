#ifndef FIGURE3D_DEF_H_
#define FIGURE3D_DEF_H_
#include <math.h>

typedef struct Vec3d_ {
    double x;
    double y;
    double z;
} Vec3d;

inline Vec3d vec3d_add(const Vec3d* lhs, const Vec3d* rhs) {
    return (Vec3d) {
        .x = lhs->x + rhs->x,
        .y = lhs->y + rhs->y,
        .z = lhs->z + rhs->z
    };
}

inline Vec3d vec3d_sub(const Vec3d* lhs, const Vec3d* rhs) {
    return (Vec3d) {
        .x = lhs->x - rhs->x,
        .y = lhs->y - rhs->y,
        .z = lhs->z - rhs->z
    };
}

inline Vec3d vec3d_mul(const Vec3d* lhs, double rhs) {
    return (Vec3d) {
        .x = lhs->x * rhs,
        .y = lhs->y * rhs,
        .z = lhs->z * rhs,
    };
}


inline double vec3d_distance(const Vec3d* a, const Vec3d* b) {
    return sqrt(pow(a->x - b->x, 2)
            + pow(a->y - b->y, 2)
            + pow(a->z - b->z, 2));
}

double vec3d_distance_void(const void* a, const void* b);

typedef struct Sphere_ {
    double radius;
    Vec3d center;
} Sphere;

#endif /* FIGURE3D_DEF_H_ */
