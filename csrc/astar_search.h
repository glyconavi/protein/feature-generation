#ifndef ASTER_SEARCH_H_
#define ASTER_SEARCH_H_
#include <stddef.h>

/**
 * 隣接ノードのデータ型
 */
typedef struct {
    /** 隣接ノード */
    const void* node;
    /** 基準ノードとnode間の距離 */
    double d;
} ASterNeighbor;

double aster_search(
        const void* value_array, size_t num_value, size_t size_value,
        size_t start_idx, size_t end_idx,
        double (*heuristic_func)(const void*, const void*),
        ASterNeighbor* (*neighbor_func)(size_t*, const void*, void*),
        void* neighbor_context);

double aster_search_from_iterator(
        const void* start_node, const void* end_node,
        double (*heuristic_func)(const void*, const void*),
        ASterNeighbor* (*neighbor_func)(size_t*, const void*, void*),
        void* neighbor_context);

double aster_search_from_short_array(
        const void* value_array, size_t num_value, size_t size_value,
        size_t start_idx, size_t end_idx,
        double (*heuristic_func)(const void*, const void*),
        ASterNeighbor* (*neighbor_func)(size_t*, const void*, void*),
        void* context);

#endif /* ASTER_SEARCH_H_ */
