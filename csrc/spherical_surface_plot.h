#ifndef SPHERICAL_SURFACE_PLOT_H_
#define SPHERICAL_SURFACE_PLOT_H_
#include "spheres_collision.h"

double calc_spheres_area_from_collision(
        Sphere* spheres, size_t n_spheres, SpheresCollision* col,
        double margin, size_t plot_num);

void search_surface_spheres_from_collision(
        bool* surface_flag, Sphere* spheres, size_t n_spheres,
        SpheresCollision* col, double margin, size_t num_plot);

void calc_spheres_area_multi_margin(
        double* out_areas,
        void* sphere_iter, const void* (iter_next)(void*),
        SpheresCollision* col, const double* margins,
        size_t num_margin, size_t num_plot);

double calc_spheres_area(Sphere* spheres, size_t n_spheres,
                         double margin, size_t plot_num);

Vec3d* create_sphere_surface_points(size_t n);

#endif /* SPHERICAL_SURFACE_PLOT_H_ */

