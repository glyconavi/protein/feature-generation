#include "figure3d.h"

double vec3d_distance_void(const void* a, const void* b) {
    return vec3d_distance((const Vec3d*)a, (const Vec3d*)b);
}
