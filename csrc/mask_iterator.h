#ifndef MASK_ITERATOR_H_
#define MASK_ITERATOR_H_
#include <stdbool.h>
#include <stddef.h>

typedef struct {
    size_t size;
    const char* p;
    const char* end;
    const bool* mask;
} MaskArrayIterator;

const void* mask_array_iterator_next(void* itr);

#endif /* MASK_ITERATOR_H_ */
