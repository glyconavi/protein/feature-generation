#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

static inline void swap(void* p, void* q, size_t size) {
#ifndef __STDC_NO_VLA__
    char buf[size];
#else
    char* buf = malloc(size);
#endif
    memcpy(buf, p, size);
    memcpy(p, q, size);
    memcpy(q, buf, size);
#ifdef __STDC_NO_VLA__
    free(buf);
#endif
}

static void insertion_sort(void* base, size_t num, size_t size,
                           int (*compare)(const void*, const void*)) {
#ifndef __STDC_NO_VLA__
    char buf[size];
#else
    char* buf = malloc(size);
#endif
    const char* end = ((char*)base) + num * size;
    for (char* p = ((char*)base) + size; p < end; p += size) {
        if (compare(p - size, p) > 0) {
            memcpy(buf, p, size);
            char* q = p;
            bool cmp = (compare(base, buf) > 0);
            do {
                memcpy(q, q - size, size);
                q -= size;
            } while((cmp && (q > (char*)base))
                    || (!cmp && (compare(q - size, buf) > 0)));
            memcpy(q, buf, size);
        }
    }
#ifdef __STDC_NO_VLA__
    free(buf);
#endif
}

static void insertion_sort_to_dst(
        void* restrict dst,
        const void* restrict src, size_t num, size_t size,
        int (*compare)(const void*, const void*)) {
    memcpy(dst, src, size);
    const char* end = ((char*)src) + num * size;
    for (const char* p = ((char*)src) + size; p < end; p += size) {
        char* q = ((char*)dst) + (p - ((char*)src));
        const bool cmp = (compare(dst, p) > 0);
        while((cmp && (q > (char*)dst))
              || (!cmp && (compare(q - size, p) > 0))) {
            memcpy(q, q - size, size);
            q -= size;
        }
        memcpy(q, p, size);
    }
}

static void merge_to_work(
        const void* a, size_t num_a,
        const void* b, size_t num_b,
        size_t size, int (*compare)(const void*, const void*),
        void * restrict work) {
#ifndef __STDC_NO_VLA__
    char buf[size];
#else
    char* buf = malloc(size);
#endif
    const char* as = (const char*)a;
    const char* bs = (const char*)b;
    ptrdiff_t num = num_a + num_b;
    char * restrict work_end = ((char*)work) + num * size;
    const char* a_end = as + (num_a - 1) * size;
    const char* b_end = bs + (num_b - 1) * size;
    for (int rest = num / 2; rest > 0; --rest) {
        if (compare(as, bs) <= 0) {
            memcpy(buf, as, size);
            as += size;
        } else {
            memcpy(buf, bs, size);
            bs += size;
        }
        memcpy(work, buf, size);
        work += size;
        work_end -= size;
        if (compare(a_end, b_end) > 0) {
            memcpy(buf, a_end, size);
            a_end -= size;
        } else {
            memcpy(buf, b_end, size);
            b_end -= size;
        }
        memcpy(work_end, buf, size);
    }
    if (num & 1) {
        if (as == a_end) {
            memcpy(work, as, size);
        } else {
            memcpy(work, bs, size);
        }
    }
#ifdef __STDC_NO_VLA__
    free(buf);
#endif
}

static void merge_sort_to_work(
        void* restrict start, size_t num, size_t size,
        int (*compare)(const void*, const void*),
        void * restrict work, bool to_work) {
    if (num <= 32) {
        if (!to_work) {
            insertion_sort(start, num, size, compare);
        } else {
            insertion_sort_to_dst(work, start, num, size, compare);
        }
        return;
    }
    size_t num_l = num / 2;
    merge_sort_to_work(start, num_l, size, compare, work, !to_work);
    merge_sort_to_work(((char*)start) + num_l * size, num - num_l, size,
                       compare, ((char*)work) + num_l * size, !to_work);
    char* src;
    char* dst;
    if (to_work) {
        src = start;
        dst = work;
    } else {
        src = work;
        dst = start;
    }
    merge_to_work(src, num_l, src + num_l * size, num - num_l, size,
                  compare, dst);
}

/*
 * 入力配列と同じサイズの作業領域workを指定してmarge sortを行う.
 */
void merge_sort_work(void* restrict start, size_t num, size_t size,
                     int (*compare)(const void*, const void*),
                     void* restrict work) {
    merge_sort_to_work(start, num, size, compare, work, false);
}

void merge_sort(void* start, size_t num, size_t size,
                int (*compare)(const void*, const void*)) {
    void* work = malloc(num * size);
    merge_sort_work(start, num, size, compare, work);
    free(work);
}
