#include "spheres_collision.h"
#include "vptree.h"
#include "array_iterator.h"
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct SpheresCollision_ {
    VpTree* tree;
    double max_radius;
} SpheresCollision;

static double distance_sphere_center2(const void* a, const void* b) {
    Vec3d* ca = &((Sphere*)a)->center;
    Vec3d* cb = &((Sphere*)b)->center;
    return pow(ca->x - cb->x, 2)
           + pow(ca->y - cb->y, 2)
           + pow(ca->z - cb->z, 2);
}

static double distance_sphere_center(const void* a, const void* b) {
    return sqrt(distance_sphere_center2(a, b));
}

/**
 * 衝突判定オブジェクトを作成する.
 * @param [in] spheres 球の配列
 * @param [in] num 球の数
 * @return 衝突判定オブジェクト
 */
SpheresCollision* create_spheres_collision(Sphere* spheres, size_t num) {
    ArrayIterator iter = create_array_iterator(
            spheres, num, sizeof(Sphere));
    return create_spheres_collision_from_iterator(
        &iter, array_iterator_next, num);
}

/** イテレータ消費時に球の最大半径を保存する */
typedef struct {
    void* iter;
    const void* (*src_next)(void*);
    double max_radius;
} TrapIterator;

const void* trap_next(void* trap_itr) {
    TrapIterator* it = (TrapIterator*)trap_itr;
    const Sphere* sp = (const Sphere*)it->src_next(it->iter);
    if ((sp != NULL) && (it->max_radius < sp->radius)) {
        it->max_radius = sp->radius;
    }
    return sp;
}

SpheresCollision* create_spheres_collision_from_iterator(
        void* iter, const void* (iter_next)(void*),
        size_t num) {
    SpheresCollision* col = (SpheresCollision*)malloc(
            sizeof(SpheresCollision));
    TrapIterator trap_iter = (TrapIterator) {
        .iter = iter, .src_next = iter_next, .max_radius = 0.0
    };
    col->tree = create_vptree_from_iterator(
            distance_sphere_center, &trap_iter, trap_next, num, sizeof(Sphere));
    col->max_radius = trap_iter.max_radius;
    return col;
}

/**
 * SpheresCollisionと子オブジェクトのリソースを開放する.
 * @param [in,out] self 開放対象
 */
void free_spheres_collision(SpheresCollision* self) {
    free_vptree(self->tree);
    free(self);
}

typedef struct {
    const Sphere* sphere;
    double margin;
} ContextSameAndNotCol;

static bool same_and_not_col(const void* sphere, void* context) {
    const Sphere* sl = (const Sphere*)sphere;
    const Sphere* sr = ((ContextSameAndNotCol*)context)->sphere;
    double margin = ((ContextSameAndNotCol*)context)->margin;
    return (sl == sr)
           || (pow(sl->radius + sr->radius + margin * 2.0, 2)
               <= distance_sphere_center2(sl, sr));
}

/**
 * 指定された球と衝突している球集合をSpheresCollisionから探索する
 * @param self
 * @param sphere 衝突判定を行う1つの球
 * @param margin 衝突判定対象の2つの球の両方の半径を指定値分加算する.
 * @return sphereと衝突している球の集合
 */
ConstLinkedList* get_collided_spheres(
        SpheresCollision* self, const Sphere* sphere, double margin) {
    ConstLinkedList* list = near_distance_vp(
            self->tree, sphere,
            self->max_radius + sphere->radius + margin * 2);
    ContextSameAndNotCol c = (ContextSameAndNotCol) {
        .margin = margin,
        .sphere = sphere,
    };
    remove_if_list(list, same_and_not_col, &c);
    return list;
}

/**
 * 指定された球と衝突している球集合をSpheresCollisionから
 * 探索して配列に出力する.
 * @param self
 * @param [out] out_array 結果の出力先配列
 * @param [in] out_arrayの最大容量
 * @param sphere 衝突判定を行う1つの球
 * @param margin 衝突判定対象の2つの球の両方の半径を指定値分加算する.
 * @return sphereと衝突している球の数
 *         len_arrayより多い場合はlen_arrayより大きい値
 */
size_t get_collided_spheres_to_array(
        SpheresCollision* self,
        const Sphere** out_array, size_t len_array,
        const Sphere* sphere, double margin) {
    ContextSameAndNotCol c = (ContextSameAndNotCol) {
        .margin = margin,
        .sphere = sphere,
    };
    return near_distance_vp_to_buffer(
            self->tree, (const void**)out_array, len_array,
            sphere, self->max_radius + sphere->radius + margin * 2,
            same_and_not_col, &c);
}

const Sphere* get_nearest_center_sphere(
        SpheresCollision* self, const Sphere* sphere) {
    NearestResult r = nearest_neighbor_vp(self->tree, sphere);
    return (const Sphere*)r.val;
}

typedef struct {
    bool exists;
    double threshold;
    double margin;
    const Sphere* ignore;
} ColWithPoint;

static double col_with_point_updater(void* result, double d, const void* v) {
    const Sphere* sp = (const Sphere*)v;
    ColWithPoint* c = (ColWithPoint*)result;
    if ((c->ignore != sp) && (d < (sp->radius + c->margin))) {
        c->exists = true;
        return 0.0;
    }
    return c->threshold;
}

static double sphere_point_distance(const void* vs, const void* vp) {
    const Sphere* sp = (const Sphere*)vs;
    const Vec3d* v = (const Vec3d*)vp;
    return vec3d_distance(&(sp->center), v);
}

/**
 * 指定点を内包する球が存在する場合はtrueを返す.
 * @param ignore 無視する球
 */
bool exists_collided_sphere_with_point(
        SpheresCollision* self, const Vec3d* point, double margin,
        const Sphere* ignore) {
    ColWithPoint result = (ColWithPoint) {
        .exists = false,
        .threshold = (self->max_radius + margin),
        .margin = margin,
        .ignore = ignore,
    };
    custom_near_vp(&result, self->tree, point,
                   col_with_point_updater, result.threshold,
                   sphere_point_distance);
    return result.exists;
}
