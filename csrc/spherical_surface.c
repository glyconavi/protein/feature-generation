#include "spherical_surface.h"
#include "sphere_integral.h"
#include "spheres_collision.h"
#include "spherical_surface_plot.h"
#include "surface_distance.h"
#include "array_iterator.h"
#include <stdlib.h>
#include <stdio.h>

//*
double calc_surface_area_of_spheres(
        Sphere* spheres, int num_spheres, double margin) {
    printf("plot\n");
    SpheresCollision* col = create_spheres_collision(spheres, num_spheres);
    ArrayIterator iter = create_array_iterator(
            spheres, num_spheres, sizeof(Sphere));
    double areas[3];
    const size_t num_margin = 3;
    double margins[3] = { 1.4, 7.0, 14.0 };
    calc_spheres_area_multi_margin(
            areas, &iter, array_iterator_next,
            col, margins, num_margin, 64*64);
    for (size_t i = 0; i < num_margin; ++i) {
        printf("margin %f area %f\n", margins[i], areas[i]);
    }
    //areas[0] = calc_spheres_area_from_collision(
    //        spheres, num_spheres, col, 1.4, 64*64);
    //areas[1] = calc_spheres_area_from_collision(
    //        spheres, num_spheres, col, 7.0, 64*64);
    //areas[2] = calc_spheres_area_from_collision(
    //        spheres, num_spheres, col, 14.0, 64*64);
    //bool* surface_flag = malloc(num_spheres * sizeof(bool));
    //search_surface_spheres_from_collision(
    //        surface_flag, spheres, num_spheres, col, 1.4, 32*32);
    //double d = surface_distance(
    //        spheres, num_spheres, surface_flag,
    //        0, num_spheres - 1, 7);
    //printf("surface distance: %f\n", d);
    //free(surface_flag);
    free_spheres_collision(col);
    return areas[2];
}
/*/

double calc_surface_area_of_spheres(
        Sphere* spheres, int num_spheres, double margin) {
    printf("integral\n");
    SpheresCollision* col = create_spheres_collision(spheres, num_spheres);
    double areas[3];
    const size_t num_margin = 3;
    double margins[3] = { 1.4, 7.0, 14.0 };
    spherical_surfaces_integral_multi_margin(
            areas, spheres, num_spheres, col, margins, 3, 64);
    for (size_t i = 0; i < num_margin; ++i) {
        printf("margin %f area %f\n", margins[i], areas[i]);
    }
    free_spheres_collision(col);
    return areas[2];
}
//*/
