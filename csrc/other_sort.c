#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void merge_sortA();
void merge_sortW();

typedef double d_type;// ソートするキーの型
#define SWAP(a,b) {d_type work=a;a=b;b=work;}
#define COMPSWAP(a,b) {if((b)<(a)) SWAP(a,b)}

#define MIN_TO_WORK  \
{\
    d_type temp;\
    if (*array_i<=*array_j){\
        temp=*(array_i++);\
    } else {\
        temp=*(array_j++);\
    }\
    *(work_k++)=temp;\
}

#define MAX_TO_WORK  \
{\
    d_type temp;\
    if (*array_x>*array_y){\
        temp=*(array_x--);\
    } else {\
        temp=*(array_y--);\
    }\
    *(work_z--)=temp;\
}

void merge(d_type array[],d_type work[],int left,int mid,int right) {
    d_type *array_i=array+left;
    d_type *array_j=array+mid;
    d_type *work_k=work+left;
    d_type *array_x=array+(mid-1);
    d_type *array_y=array+(right-1);
    d_type *work_z=work+(right-1);
    int kosuu=right-left,c;
    for (c=kosuu/2;c>0;c--){
        MIN_TO_WORK
        MAX_TO_WORK
    }
    if (kosuu%2){//データ数が奇数のとき
        if(array_i==array_x) *work_k=*array_i;else *work_k=*array_j;
    }
}

void insertion_sortA(d_type array[],int right) {
    COMPSWAP(array[0],array[1])//高速化のため
    int i;
    for (i=2;i<right;i++) {
        int temp=array[i];
        if (array[i-1]>temp){
            int j=i;
            do {
                array[j] = array[j-1];
                j--;
            } while (j>0 && array[j-1]>temp);
            array[j]=temp;
        }
    }
}

void insertion_sortW(d_type *array,d_type *work,int right) {
    if (array[0]<=array[1]){//高速化のため
        work[0]=array[0];
        work[1]=array[1];          
    }else{
        work[0]=array[1];
        work[1]=array[0];
    }
    int i;
    for (i=2;i<right;i++) {
        if (work[i-1]>array[i]){
            int j=i;
            do {
                work[j] = work[j-1];
                j--;
            } while (j>0 && work[j-1]>array[i]);
            work[j]=array[i];
        } else {
            work[i]=array[i];
        }
    }
}

#define UTIKIRI 7
void merge_sortA(d_type array[],d_type work[],int left,int right) {
    int kosuu=right-left;
    if (kosuu<=UTIKIRI) {
        insertion_sortA(array+left,kosuu);
        return;
    }
    int mid=left+kosuu/2;
    merge_sortW(array,work,left,mid);
    merge_sortW(array,work,mid,right);
    merge(work,array,left,mid,right);
}

void merge_sortW(d_type array[],d_type work[],int left,int right) {
    int kosuu=right-left;
    if (kosuu<=UTIKIRI) {
        insertion_sortW(array+left,work+left,kosuu);
        return;
    }
    int mid=left+kosuu/2;
    merge_sortA(array,work,left,mid);
    merge_sortA(array,work,mid,right);
    merge(array,work,left,mid,right);
}

#define MIN_TO_ARRAY  \
{\
    d_type temp;\
    if (work[j]<=array[i]){\
        temp=work[j++];\
    } else {\
        temp=array[i++];\
    }\
    array[k++]=temp;\
}

void merge0(d_type array[],d_type work[],int mid,int right) {
    int i=mid,j=0,k=0;
    if (work[mid-1]<=array[right-1]){
        while (j<mid) MIN_TO_ARRAY
    }else{
        while (i<right) MIN_TO_ARRAY
        while (j<mid) array[k++]=work[j++];
    }
}

//追加の作業記憶領域を，データの記憶領域のほぼ1/2に抑えた
void sort(d_type array[],int right){
    if (right<=3) {
        if (right>=2) insertion_sortA(array,right);
        return;
    }
    int mid=(right+1)/2;
    d_type *work=malloc(mid*sizeof(d_type));
    merge_sortA(array+mid,work,0,right-mid);
    merge_sortW(array,work,0,mid);
    merge0(array,work,mid,right);
    free(work);
}

int main() {
    int n_value = 10000307;
    //int n_value = 1000;
    double* data = (double*)malloc(n_value * sizeof(double));
    /*
    for (double* p = data; p < data + n_value; ++p) {
        *p = (double)rand() / RAND_MAX * 10000;
    }
    */
    for (double* p = data; p < data + n_value; ++p) {
        *p = p - data;
    }
    sort(data, n_value);
    /*
    for (double* p = data; p < data + n_value; ++p) {
        printf("%f\n", *p);
    }
    */
    printf("middle = %f\n", *(data + (n_value / 2)));
    return 0;
}
