# feature-generation

This software generates features related to glycan-binding sites and outputs
the result in user specified format (tsv, csv or turtle).

## install:
Execute the following command (Java version of 17 or higher is required).

`git git@gitlab.com:glyconavi/protein/feature-generation.git`


`cd feature-generation`


`mvn clean compile assembly:single`

### To recompile the C programs, execute the following commands.

`cd csrc`

`make install`

## usage:

`java -jar g4d.jar -d output_dir_path -i PDB/AlphaFold_ID -o tsv/turtle/csv [-f]`


or


`java -jar g4d.jar -d output_dir_path -p PDB/AlphaFold_file_path -o tsv/turtle/csv [-f]`


or


`java -jar g4d.jar -d output_dir_path -l id_file_path_list -o tsv/turtle/csv [-f]`

```
  Options:
   -d,--dir <DIRECTORY>                 Set path to the directory where
                                        output file is placed
   -f,--force                           Overwrite existing file
   -h,--help                            Show usage help
   -i,--id <String>                     Specify PDB code or AlphaFold ID to
                                        parse
   -l,--list <String>                   Specify the path of a list which
                                        contains PDB or AlphaFold IDs, or
                                        local mmCIF file paths
   -o,--out <FORMAT=[csv|tsv|turtle]>   Specify output format, required
   -p,--path <String>                   Specify the path of PDB or AlphaFold

                                        mmCIF file
```

## sample:

`java -jar g4d.jar -d ./test -i 8tjc -o tsv`               

`java -jar g4d.jar -p ./test -d ./test/out -i 8tjc -o turtle`

Version: 0.0.2

# Change log

* 2024-02-28 typo in README.md (tutle -> turtle) and add sample
