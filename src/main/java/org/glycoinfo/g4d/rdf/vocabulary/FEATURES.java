package org.glycoinfo.g4d.rdf.vocabulary;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.glycoinfo.g4d.config.Constants;

public class FEATURES {

    public static String getURI() {
        return Constants.G4D_PREFIX;
    }

    public static final Property property(String local) {
        return ResourceFactory.createProperty(getURI(), local);
    }

    public static final Resource resource(String local) {
        return ResourceFactory.createResource(getURI() + local);
    }

    // root model class.
    public static class RootModel {
        public static final Property identifier = ResourceFactory.createProperty(Constants.DC_URL, "identifier");
        public static final String getRootURI(String uri, String pdbId) {
            return String.format("%s/pdb/%s", uri, pdbId);
        }
    }

    // GlycosylationSite class
    public static class GlycosylationSite {
        //
        public static final Property hasNGlycosylationSite = property("has_n_glycosylation_site");
        public static final Property hasOGlycosylationSite = property("has_o_glycosylation_site");
        public static final Property hasDisulfidBondSite = property("has_disulfide_bond_site");
        public static final Property hasNConsensusPattern = property("has_n_consensus_pattern");
        public static final Property hasAsa = property("has_asa");
        public static final Property hasTorsionAngle = property("has_torsion_angle");
        public static final Property hasDistanceBetweenGlygosylationSites = property("has_distance_between_glycosylation_sites");
        public static final Property hasAsn = property("has_asparagine");
        public static final Property hasSer = property("has_serine");
        public static final Property hasThr = property("has_threonine");
        // added. 16.Mar.2024
        public static final Property hasGlycosylationSite = property("has_glycosylation_site");
        public static final Property isGlycosylationSite = property("is_glycosylation_site");
        // 
        public static final Property hasAminoAcid = property("has_amino_acid");
        public static final Property hasAaPosition = property("has_amino_acid_position");
        public static final Property has3DDistance = property("has_3D_distance");
        public static final Property hasAaDistance = property("has_aa_distance");
    }

    // NGlycosylationSite class
    public static class NGlycosylationSite extends GlycosylationSite {
        private static final String className = NGlycosylationSite.class.getSimpleName();
        // https://G4DURL/NGlycosylationSite
        public static final Resource resource() {
            return FEATURES.resource(className);
        }
        // https://G4DURL/"PDBcode"/"AsymID"/GlycosylationSite/NGlycosylationSite/"residue number"
        // String URL: https://G4DURL/"PDBcode"/"asymID"/GlycosylationSite
        public static final String getURI(String url, int resNum) {
            return String.format("%s/%s/%s", url, className, resNum);
        }
    }

    // OGlycosylationSite class
    public static class OGlycosylationSite extends GlycosylationSite {
        private static final String className = OGlycosylationSite.class.getSimpleName();
        // https://G4DURL/OGlycosylationSite
        public static final Resource resource() {
            return FEATURES.resource(className);
        }
        // https://G4DURL/"PDBcode"/"AsymID"/GlycosylationSite/OGlycosylationSite/"residue number"
        // String URL: https://G4DURL/"PDBcode"/"asymID"/GlycosylationSite
        public static final String getURI(String url, int resNum) {
            return String.format("%s/%s/%s", url, className, resNum);
        } 
    }

    // TorsionAngle class
    public static class TorsionAngle extends GlycosylationSite {
        private static final String className = TorsionAngle.class.getSimpleName();
        public static final Property hasAngle = property("has_angle");
        // https://URL/TorsionAngle
        public static final Resource resource() {
            return FEATURES.resource(className);
        }
        // https://G4DURL/"PDBcode"/"AsymID"/GlycosylationSite/OGlycosylationSite/"residue number"
        // String URL: https://G4DURL/"PDBcode"/"asymID"/GlycosylationSite
        public static final String getURI(String url, int resNum) {
            return String.format("%s/%s/%s", url, className, resNum);
        }       
    }

    // NConseunsusPattern class
    public static class NConsensusPattern extends GlycosylationSite {
        private static final String className = NConsensusPattern.class.getSimpleName();
        public static final Property hasConsensusPattern = property("has_consensus_pattern");
        // https://URL/NConsensusPattern
        public static final Resource resource() {
            return FEATURES.resource(className);
        }
        // https://G4DURL/"PDBcode"/"AsymID"/GlycosylationSite/OGlycosylationSite/"residue number"
        // String URL: https://G4DURL/"PDBcode"/"asymID"/GlycosylationSite
        public static final String getURI(String url, int resNum) {
            return String.format("%s/%s/%s", url, className, resNum);
        }
    }

    // ASA class
    public static class Asa extends GlycosylationSite {
        private static final String className = Asa.class.getSimpleName();
        public static final Property has1_4Angstrom = property("has_1_4_angstrom");
        public static final Property has7_0Angstrom = property("has_7_0_angstrom");
        public static final Property has14_0Angstrom = property("has_14_0_angstrom");
        // https://URL/Asa
        public static final Resource resource() {
            return FEATURES.resource(className);
        }
        // https://G4DURL/"PDBcode"/"AsymID"/GlycosylationSite/Asa/"residue number"
        // String URL: https://G4DURL/"PDBcode"/"asymID"/GlycosylationSite
        public static final String getURI(String url, int resNum) {
            return String.format("%s/%s/%s", url, className, resNum);
        }
    }

    // Distance class
    public static class Distance extends GlycosylationSite {
        private static final String className = Distance.class.getSimpleName();
        public static final Property hasSurfaceDistance = property("has_surface_distance");
        // https://URL/Distance
        public static final Resource resource() {
            return FEATURES.resource(className);
        }
        // https://G4DURL/"PDBcode"/"AsymID"/GlycosylationSite/Distance/"residue number","residue number"
        // String URL: https://G4DURL/"PDBcode"/"asymID"/GlycosylationSite
        public static final String getURI(String url, int ptnr1ResNum, int ptnr2ResNum) {
            return String.format("%s/%s/%s,%s", url, className, ptnr1ResNum, ptnr2ResNum);
        }
    }

    public static class Asn extends GlycosylationSite {
        private static final String className = Asn.class.getSimpleName();
        // https://URL/Asn
        public static final Resource resource() {
            return FEATURES.resource(className);
        }
        // https://G4DURL/"PDBcode"/"AsymID"/GlycosylationSite/NGlycosylationSite/Asn/"residue number"
        // String URL: https://G4DURL/"PDBcode"/"asymID"/GlycosylationSite
        public static final String getURI(String url, int resNum) {
            return String.format("%s/%s/%s/%s", url, className, NGlycosylationSite.className, resNum);
        }      
    }

    public static class Ser extends GlycosylationSite {
        private static final String className = Ser.class.getSimpleName();
        // https://URL/Ser
        public static final Resource resource() {
            return FEATURES.resource(className);
        }
        // https://G4DURL/"PDBcode"/"AsymID"/GlycosylationSite/OGlycosylationSite/Ser/"residue number"
        // String URL: https://G4DURL/"PDBcode"/"asymID"/GlycosylationSite
        public static final String getURI(String url, int resNum) {
            return String.format("%s/%s/%s/%s", url, className, OGlycosylationSite.className, resNum);
        }      
    }
    
    public static class Thr extends GlycosylationSite {
        private static final String className = Thr.class.getSimpleName();
        // https://URL/Thr
        public static final Resource resource() {
            return FEATURES.resource(className);
        }
        // https://G4DURL/"PDBcode"/"AsymID"/GlycosylationSite/OGlycosylationSite/Thr/"residue number"
        // String URL: https://G4DURL/"PDBcode"/"asymID"/GlycosylationSite
        public static final String getURI(String url, int resNum) {
            return String.format("%s/%s/%s/%s", url, className, OGlycosylationSite.className, resNum);
        }      
    }

    // DisulfideBond class
    public static class DisulfideBondSite {
        private static final String className = DisulfideBondSite.class.getSimpleName();
        public static final Property has3DDistance = property("has_3D_distance");
        public static final Property hasAaDistance = property("has_aa_distance");
        // https://URL/"PDBcode"/"AsymID"/DisulfideBondSite
        public static final Resource resource() {
            return FEATURES.resource(className);
        }
        // https://G4DURL/"PDBcode"/"AsymID"/DisulfideBondSite/"residue number","residue number"
        // String URL: https://G4DURL/"PDBcode"/"asymID"
        public static final String getURI(String url, int ptnr1ResNum, int ptnr2ResNum, String ptnr1AsymId, String ptnr2AsymId) {
            return String.format("%s/%s/%s,%s,%s,%s", url, className, ptnr1AsymId, ptnr1ResNum, ptnr2AsymId, ptnr2ResNum);
        }      
    }

}
