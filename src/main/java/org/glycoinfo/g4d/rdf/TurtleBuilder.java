package org.glycoinfo.g4d.rdf;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.shared.PrefixMapping;
import org.glycoinfo.g4d.config.Constants;
import org.glycoinfo.g4d.feature.MLFeatures;
import org.glycoinfo.g4d.feature.MLFeaturesUtil;
import org.glycoinfo.g4d.rdf.vocabulary.FEATURES;

public class TurtleBuilder {

    private MLFeatures mlFeatures;

    private Resource nGlyRoot;
    private Resource oGlyRoot;
    private Resource disulfRoot;
    private Resource nConRoot;
    private Resource asaRoot;
    private Resource torsionRoot;
    private Resource distRoot;
    private Resource asnRoot;
    private Resource serRoot;
    private Resource thrRoot;

    private LinkedHashMap<Integer, String> seqIdCompIdMap;

    public TurtleBuilder entry(MLFeatures mlFeatures) {
        this.mlFeatures = mlFeatures;
        return this;
    }

    // Set up RDF namespace prefixes
    public PrefixMapping namespaces() {
        PrefixMapping namespaces = PrefixMapping.Factory.create()
                .setNsPrefixes(PrefixMapping.Standard)
                .setNsPrefix("g4d", Constants.G4D_PREFIX);
        return namespaces;
    }

    // Create a new RDF model
    public Model getModel() {
        Model model = ModelFactory.createDefaultModel();
        return model;
    }

    // Set predefined namespaces in the RDF model
    public void setNamespace(Model model) {
        model.setNsPrefixes(namespaces());
    }

    public void initializeRootResource(Model model) {
        String url = Constants.GLYCONAVI_URL;
        String entryId = this.mlFeatures.getEntryId();
        this.nGlyRoot = model.createResource(FEATURES.RootModel.getRootURI(url, entryId));
        this.oGlyRoot = model.createResource(FEATURES.RootModel.getRootURI(url, entryId));
        this.disulfRoot = model.createResource(FEATURES.RootModel.getRootURI(url, entryId));
        this.nConRoot = model.createResource(FEATURES.RootModel.getRootURI(url, entryId));
        this.asaRoot = model.createResource(FEATURES.RootModel.getRootURI(url, entryId));
        this.torsionRoot = model.createResource(FEATURES.RootModel.getRootURI(url, entryId));
        this.distRoot = model.createResource(FEATURES.RootModel.getRootURI(url, entryId));
        this.asnRoot = model.createResource(FEATURES.RootModel.getRootURI(url, entryId));
        this.serRoot = model.createResource(FEATURES.RootModel.getRootURI(url, entryId));
        this.thrRoot = model.createResource(FEATURES.RootModel.getRootURI(url, entryId));
    }

    // root model
    public void addId(Model model) {
        String pdbId = this.mlFeatures.getEntryId();
        Resource rootModel = model.createResource(FEATURES.RootModel.getRootURI(Constants.GLYCONAVI_URL, this.mlFeatures.getEntryId()));
        rootModel.addProperty(FEATURES.RootModel.identifier, pdbId);
    }

    // Add N-glycosylation site information to the RDF model
    public void addNGlycosylationSite(Model model, String uri) {
        ArrayList<Integer> nGlycoSeqIdList = this.mlFeatures.getnGlycoSeqIdList();
        int startIndex = this.mlFeatures.getnGlycoAsymIdList().indexOf(this.mlFeatures.getCurrentAsymId());
        int endIndex = this.mlFeatures.getnGlycoAsymIdList().lastIndexOf(this.mlFeatures.getCurrentAsymId());
        if (nGlycoSeqIdList.size() > 0 && startIndex != -1 && endIndex != -1) {
        	List<Integer> seqIdList = nGlycoSeqIdList.subList(startIndex, endIndex + 1);
            for (int i = 0; i < seqIdList.size(); i++) {
                int resNum = seqIdList.get(i);
                this.nGlyRoot.addProperty(FEATURES.GlycosylationSite.hasNGlycosylationSite, ResourceFactory.createResource(FEATURES.NGlycosylationSite.getURI(uri, resNum)));
                Resource nGlyco = model.createResource(FEATURES.NGlycosylationSite.getURI(uri, resNum), FEATURES.NGlycosylationSite.resource());
                nGlyco.addProperty(FEATURES.NGlycosylationSite.hasAaPosition, model.createTypedLiteral(resNum));
                nGlyco.addProperty(FEATURES.NGlycosylationSite.hasAminoAcid, this.seqIdCompIdMap.get(resNum)); // key: a residue number, value: the amino acid            
            }
        }
    }

    // Add O-glycosylation site information to the RDF model
    public void addOGlycosylationSite(Model model, String uri) {
        ArrayList<Integer> oGlycoSeqIdList = this.mlFeatures.getoGlycoSeqIdList();
        int startIndex = this.mlFeatures.getoGlycoAsymIdList().indexOf(this.mlFeatures.getCurrentAsymId());
        int endIndex = this.mlFeatures.getoGlycoAsymIdList().lastIndexOf(this.mlFeatures.getCurrentAsymId());
        if (oGlycoSeqIdList.size() > 0 && startIndex != -1 && endIndex != -1) {
        	List<Integer> seqIdList = oGlycoSeqIdList.subList(startIndex, endIndex + 1);
            for (int i = 0; i < seqIdList.size(); i++) {
                int resNum = seqIdList.get(i);
                this.oGlyRoot.addProperty(FEATURES.GlycosylationSite.hasOGlycosylationSite, ResourceFactory.createResource(FEATURES.OGlycosylationSite.getURI(uri, resNum)));
                Resource oGlyco = model.createResource(FEATURES.OGlycosylationSite.getURI(uri, resNum), FEATURES.OGlycosylationSite.resource());
                oGlyco.addProperty(FEATURES.OGlycosylationSite.hasAaPosition, model.createTypedLiteral(resNum));
                oGlyco.addProperty(FEATURES.OGlycosylationSite.hasAminoAcid, this.seqIdCompIdMap.get(resNum)); // key: a residue number, value: the amino acid
            }
        }
    }

    // Add torsion angle information to the RDF model
    public void addTorsionAngle(Model model, String uri) {
        MLFeaturesUtil.calcTorsionAngle(this.mlFeatures);
        List<String> torsionAngleList = this.mlFeatures.getTorsionAngleList();
        // List of glycan binding sites (N-Glycosylation, O-Glycosylation) are joined and merged.
        List<Integer> mergeGlySeqIdList = MLFeaturesUtil.mergeGlycoSeqIdList(mlFeatures);
        List<Integer> nConSeqIdList = mlFeatures.getnGlyConsensusSeqStartNumList();
        // modified. 14.Mar.2024
        for (int i = 0; i < nConSeqIdList.size(); i++) {
            int resNum = nConSeqIdList.get(i);
            this.torsionRoot.addProperty(FEATURES.GlycosylationSite.hasTorsionAngle, ResourceFactory.createResource(FEATURES.TorsionAngle.getURI(uri, resNum)));
            Resource torAng = model.createResource(FEATURES.TorsionAngle.getURI(uri, resNum), FEATURES.TorsionAngle.resource());
            Literal doubleTorAng = ResourceFactory.createTypedLiteral(torsionAngleList.get(i), XSDDatatype.XSDdouble);
            torAng.addProperty(FEATURES.TorsionAngle.hasAngle, doubleTorAng);
            // added. 16.Mar.2024
            if (mergeGlySeqIdList.contains(resNum)) {
                Literal boolGly = ResourceFactory.createTypedLiteral("true", XSDDatatype.XSDboolean);
                torAng.addProperty(FEATURES.TorsionAngle.isGlycosylationSite, boolGly);
            }
        }
    }

    // Add N-glycosylation consensus pattern information to the RDF model
    public void addNGlycosylationConsensusPattern(Model model, String uri) {
        // Extract consensus patterns for N-joins. Extract the consensus pattern for the current chain inside the following method.
    	MLFeaturesUtil.checkNGlyConsensusSequence(mlFeatures); // added. 27.Dec.2023.
        ArrayList<String> nGlyConSeqList = this.mlFeatures.getnGlyConsensusSeqPatternList();
        ArrayList<Integer> nGlyConSeqStartList = this.mlFeatures.getnGlyConsensusSeqStartNumList();
        if (nGlyConSeqList.size() > 0) {
            for (int i = 0; i < nGlyConSeqList.size(); i++) {
                String pattern = nGlyConSeqList.get(i);
                int resNum = nGlyConSeqStartList.get(i);
                this.nConRoot.addProperty(FEATURES.GlycosylationSite.hasNConsensusPattern, ResourceFactory.createResource(FEATURES.NConsensusPattern.getURI(uri, resNum)));
                Resource nPattern = model.createResource(FEATURES.NConsensusPattern.getURI(uri, resNum), FEATURES.NConsensusPattern.resource());
                nPattern.addProperty(FEATURES.NConsensusPattern.hasAaPosition, model.createTypedLiteral(resNum));
                nPattern.addProperty(FEATURES.NConsensusPattern.hasConsensusPattern, pattern);
            }
        }
    }

    // Add ASA information to the RDF model
    public void addAsa(Model model, String uri) {
        // Execute methods to calculate the exposed surface area and the distance between glycosyl linkages. This addAsa must be called before addDistance.
        MLFeaturesUtil.calcSphere(this.mlFeatures);
        ArrayList<String> glySite1_4AreaList = this.mlFeatures.getGlySite1_4AreaList();
        ArrayList<String> glySite7_0AreaList = this.mlFeatures.getGlySite7_0AreaList();
        ArrayList<String> glySite14AreaList = this.mlFeatures.getGlySite14AreaList();
        // List of glycan binding sites (N-Glycosylation, O-Glycosylation) are joined and merged.
        List<Integer> mergeGlySeqIdList = MLFeaturesUtil.mergeGlycoSeqIdList(mlFeatures);
        List<Integer> nConSeqIdList = mlFeatures.getnGlyConsensusSeqStartNumList();
        // modified. 14.Mar.2024
        for (int i = 0; i < nConSeqIdList.size(); i++) {
            this.asaRoot.addProperty(FEATURES.GlycosylationSite.hasAsa, ResourceFactory.createResource(FEATURES.Asa.getURI(uri, nConSeqIdList.get(i))));
            Resource asa = model.createResource(FEATURES.Asa.getURI(uri, nConSeqIdList.get(i)), FEATURES.Asa.resource());
            // added. 16.Mar.2024
            if (mergeGlySeqIdList.contains(nConSeqIdList.get(i))) {
                Literal boolGly = ResourceFactory.createTypedLiteral("true", XSDDatatype.XSDboolean);
                asa.addProperty(FEATURES.Asa.isGlycosylationSite, boolGly);
            }
            if (!glySite1_4AreaList.get(i).equals("-1.0")) {
                Literal double1_4Area = ResourceFactory.createTypedLiteral(glySite1_4AreaList.get(i), XSDDatatype.XSDdouble);
                asa.addProperty(FEATURES.Asa.has1_4Angstrom, double1_4Area);
            }
            if (!glySite7_0AreaList.get(i).equals("-1.0")) {
                Literal double7_0Area = ResourceFactory.createTypedLiteral(glySite7_0AreaList.get(i), XSDDatatype.XSDdouble);
                asa.addProperty(FEATURES.Asa.has7_0Angstrom, double7_0Area);
            }
            if (!glySite14AreaList.get(i).equals("-1.0")) {
                Literal double14Area = ResourceFactory.createTypedLiteral(glySite14AreaList.get(i), XSDDatatype.XSDdouble);
                asa.addProperty(FEATURES.Asa.has14_0Angstrom, double14Area);
            }
        }
    }

    // Add distance information to the RDF model
    public void addDistance(Model model, String uri) {
        // Because of the MLFeaturesUtil.calcSphere in addAsa, the distance can be obtained with this.mlFeatures.getGlySiteTerDistanceList();.
    	// This method is executed after addAsa.
    	ArrayList<String> glySiteTerDistanceList = this.mlFeatures.getGlySiteTerDistanceList();
    	// Calculate the distance of amino acids between glycan binding sites.
    	MLFeaturesUtil.calcGlycoAminoAcidsDistance(this.mlFeatures);
        ArrayList<Integer> glyAaDistanceList = this.mlFeatures.getGlySiteAaDistanceList();
        // List of glycan binding sites (N-Glycosylation, O-Glycosylation) are joined and merged.
        List<Integer> mergeGlySeqIdList = MLFeaturesUtil.mergeGlycoSeqIdList(mlFeatures);
        // modified. 14.Mar.2024
        List<Integer> nConSeqIdList = mlFeatures.getnGlyConsensusSeqStartNumList();
        for (int i = 0; i < nConSeqIdList.size() - 1; i++) {
            int ptnr1ResNum = nConSeqIdList.get(i);
            int ptnr2ResNum = nConSeqIdList.get(i + 1);
            this.distRoot.addProperty(FEATURES.GlycosylationSite.hasDistanceBetweenGlygosylationSites,
                  ResourceFactory.createResource(FEATURES.Distance.getURI(uri, ptnr1ResNum, ptnr2ResNum)));
            Resource distance = model.createResource(FEATURES.Distance.getURI(uri, ptnr1ResNum, ptnr2ResNum), FEATURES.Distance.resource());
            distance.addProperty(FEATURES.Distance.hasAaDistance, model.createTypedLiteral(glyAaDistanceList.get(i)));
            if (mergeGlySeqIdList.contains(ptnr1ResNum) || mergeGlySeqIdList.contains(ptnr1ResNum)) {
                Literal boolGly = ResourceFactory.createTypedLiteral("true", XSDDatatype.XSDboolean);
                distance.addProperty(FEATURES.Distance.hasGlycosylationSite, boolGly);
            }
            if (!glySiteTerDistanceList.get(i).equals("-1.0")) {
                Literal doubleDistance = ResourceFactory.createTypedLiteral(glySiteTerDistanceList.get(i), XSDDatatype.XSDdouble);
                distance.addProperty(FEATURES.Distance.hasSurfaceDistance, doubleDistance);
                if (mergeGlySeqIdList.contains(ptnr1ResNum) || mergeGlySeqIdList.contains(ptnr1ResNum)) {
                    Literal boolGly = ResourceFactory.createTypedLiteral("true", XSDDatatype.XSDboolean);
                    distance.addProperty(FEATURES.Distance.isGlycosylationSite, boolGly);
                }
            }
        }
    }

    // Add Asparagine (Asn) residue information to the RDF model
    public void addAsn(Model model, String uri) {
        // Obtain the residue positions of ASN, SER and THR. Inside the following method, the extraction is performed for the current chain.
        MLFeaturesUtil.setAsnSerThrResidueNum(this.mlFeatures);
        ArrayList<Integer> asnPosList = this.mlFeatures.getAsnSeqPositionList();
        for (int i = 0; i < asnPosList.size(); i++) {
            int resNum = asnPosList.get(i);
            this.asnRoot.addProperty(FEATURES.GlycosylationSite.hasAsn, ResourceFactory.createResource(FEATURES.Asn.getURI(uri, resNum)));
            Resource asn = model.createResource(FEATURES.Asn.getURI(uri, resNum), FEATURES.Asn.resource());
            asn.addProperty(FEATURES.Asn.hasAaPosition, model.createTypedLiteral(resNum));
        }
    }

    // Add Serine (Ser) residue information to the RDF model
    public void addSer(Model model, String uri) {
        // Since MLFeaturesUtil.setAsnSerThrResidueNum(this.mlFeatures); is executed inside the addAsn method, it is not necessary to do anything else after this.
    	ArrayList<Integer> serPosList = this.mlFeatures.getSerSeqPositionList();
        for (int i = 0; i < serPosList.size(); i++) {
            int resNum = serPosList.get(i);
            this.serRoot.addProperty(FEATURES.GlycosylationSite.hasSer, ResourceFactory.createResource(FEATURES.Ser.getURI(uri, resNum)));
            Resource ser = model.createResource(FEATURES.Ser.getURI(uri, resNum), FEATURES.Ser.resource());
            ser.addProperty(FEATURES.Ser.hasAaPosition, model.createTypedLiteral(resNum));
        }
    }

    // Add Threonine (Thr) residue information to the RDF model
    public void addThr(Model model, String uri) {
        // Since MLFeaturesUtil.setAsnSerThrResidueNum(this.mlFeatures); is executed inside the addAsn method, it is not necessary to do anything else after this.
    	ArrayList<Integer> thrPosList = this.mlFeatures.getThrSeqPositionList();
        for (int i = 0; i < thrPosList.size(); i++) {
            int resNum = thrPosList.get(i);
            this.thrRoot.addProperty(FEATURES.GlycosylationSite.hasThr, ResourceFactory.createResource(FEATURES.Thr.getURI(uri, resNum)));
            Resource thr = model.createResource(FEATURES.Thr.getURI(uri, resNum), FEATURES.Thr.resource());
            thr.addProperty(FEATURES.Thr.hasAaPosition, model.createTypedLiteral(resNum));
        }
    }

    // Add disulfide bond site information to the RDF model
    public void addDisulfideBondSite(Model model, String uri) {
        // The amino acid distances of the SS bonds are calculated by the following method. The calculation is performed inside the method with the current chain.
    	MLFeaturesUtil.calcSSBondAaDistance(this.mlFeatures);
    	ArrayList<Integer> ssBondDistanceList = this.mlFeatures.getSsBondsAADistanceList();
    	// The steric distance of the SS bond is calculated by the following method. The calculation is performed inside the method with the current chain.
    	MLFeaturesUtil.calcSSBond3DDistance(this.mlFeatures);
        ArrayList<String> ssBond3DDistanceList = this.mlFeatures.getSsBonds3DDistanceList();
        // Get a list of SS joins for the current chain. However, for ptnr2, since the chain is not necessarily the same as ptnr1, corresponding to
        // the current chain position of ptnr1 Obtain the location information of the SS join of ptnr2.
        int startIndex = this.mlFeatures.getPtnr1ssBondsAsymIdList().indexOf(this.mlFeatures.getCurrentAsymId());
        int endIndex = this.mlFeatures.getPtnr1ssBondsAsymIdList().lastIndexOf(this.mlFeatures.getCurrentAsymId());
        if (startIndex == -1 || endIndex == -1) {
            return;
        }
        // In the case of ptnr2, the chain may be different from ptnr1 to form SS joins, so the chain is obtained (used for URLs in RDF).
        List<String> ptnr2AsymIdList = this.mlFeatures.getPtnr2ssBondsAsymIdList().subList(startIndex, endIndex + 1);
        // Obtain residue position information for ptnr1 and ptnr2. The order of the list corresponds to.
        List<Integer> ptnr1SsBondResList = this.mlFeatures.getPtnr1ssBondsSeqIdList().subList(startIndex, endIndex + 1);
        List<Integer> ptnr2SsBondResList = this.mlFeatures.getPtnr2ssBondsSeqIdList().subList(startIndex, endIndex + 1);
        if (ssBondDistanceList.size() != 0) {
            for (int i = 0; i < ssBondDistanceList.size(); i++) {
            	if (ssBondDistanceList.get(i) == -1 && ssBond3DDistanceList.get(i).equals("-1.0")) {
            	    continue; // Residues that cannot be calculated by HETATM or other means are skipped.
            	}
                int ssAaDistance = ssBondDistanceList.get(i);
                String ss3DDistance = ssBond3DDistanceList.get(i);
                int ptnr1ResNum = ptnr1SsBondResList.get(i);
                String ptnr1AsymId = this.mlFeatures.getCurrentAsymId();
                int ptnr2ResNum = ptnr2SsBondResList.get(i);
                String ptnr2AsymId = ptnr2AsymIdList.get(i);
                this.disulfRoot.addProperty(FEATURES.GlycosylationSite.hasDisulfidBondSite, 
                        ResourceFactory.createResource(FEATURES.DisulfideBondSite.getURI(uri, ptnr1ResNum, ptnr2ResNum, ptnr1AsymId, ptnr2AsymId)));
                Resource ssBond = model.createResource(FEATURES.DisulfideBondSite.getURI(uri, ptnr1ResNum, ptnr2ResNum, ptnr1AsymId, ptnr2AsymId),
                    FEATURES.DisulfideBondSite.resource());
                Literal double3DDistance = ResourceFactory.createTypedLiteral(ss3DDistance, XSDDatatype.XSDdouble);
                if (!ssBondDistanceList.get(i).equals(-1)) {
                    ssBond.addProperty(FEATURES.DisulfideBondSite.hasAaDistance, model.createTypedLiteral(ssAaDistance));
                }
                if (!ssBond3DDistanceList.get(i).equals("-1.0")) {
                    ssBond.addProperty(FEATURES.DisulfideBondSite.has3DDistance, double3DDistance);
                }
            }
        }
    }

    public void setSeqIdCompIdMap(LinkedHashMap<Integer, String> seqIdCompIdMap) {
        this.seqIdCompIdMap = seqIdCompIdMap;
    }
}
