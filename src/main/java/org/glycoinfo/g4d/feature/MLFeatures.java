package org.glycoinfo.g4d.feature;

import java.util.ArrayList;

import org.rcsb.cif.schema.mm.AtomSite;
import org.rcsb.cif.schema.mm.Entry;
import org.rcsb.cif.schema.mm.StructConn;

public class MLFeatures {

	// _entry
	private Entry entry;
	// _atom_site
	private AtomSite atomSite;
	// _struct_conn
	private StructConn structConn;

    // entry ID
    private String entryId;

    // N-Glycosylation site (for PDB mmCIF)
    private ArrayList<String> nGlycoIdList;
    private ArrayList<String> nGlycoAsymIdList;
    private ArrayList<String> nGlycoCompIdList;
    private ArrayList<Integer> nGlycoSeqIdList;

    // O-Glycosylation site (for PDB mmCIF)
    private ArrayList<String> oGlycoIdList;
    private ArrayList<String> oGlycoAsymIdList;
    private ArrayList<String> oGlycoCompIdList;
    private ArrayList<Integer> oGlycoSeqIdList;

    // amino acids sequences
    private String aaSequence;
    private ArrayList<String> aaSequenceList;
    ///////// only Cα
    // group information
    private ArrayList<String> groupList;
    // amino acids information
    private ArrayList<String> aaCompIdList;
    private ArrayList<String> aaAsymIdList;
    private ArrayList<Integer> aaSeqIdList;
    private ArrayList<String> aaAtomIdList;
    // XYZ coordinates.
    private ArrayList<Double> cartnXList;
    private ArrayList<Double> cartnYList;
    private ArrayList<Double> cartnZList;
    //// full atom
    // group information
    private ArrayList<String> fullGroupList;
    // amino acids information
    private ArrayList<String> fullAaCompIdList;
    private ArrayList<String> fullAaAsymIdList;
    private ArrayList<Integer> fullAaSeqIdList;
    private ArrayList<String> fullAaAtomIdList;
    private ArrayList<Integer> fullAaIdList;
    // XYZ coordinates.
    private ArrayList<Double> fullCartnXList;
    private ArrayList<Double> fullCartnYList;
    private ArrayList<Double> fullCartnZList;

    // Presence or absence of consensus sequences for N-Glycosylation (for PDB and AlphaFold mmCIFs)
    private boolean nGlyConsensusSequenceHit;
    // start sequence number list of N-Glycosylation consensus sequences.
    private ArrayList<Integer> nGlyConsensusSeqStartNumList;
    // N-Glycosylation site consensus sequence patterns.
    private ArrayList<String> nGlyConsensusSeqPatternList;

    // sequence positions of ASN in N-Glycosylation consensus sequences.
    private ArrayList<Integer> asnSeqPositionList;
    // sequence positions of SER in full sequence.
    private ArrayList<Integer> serSeqPositionList;
    // sequence positions of THR in full sequence.
    private ArrayList<Integer> thrSeqPositionList;

    // S-S bonds IDs.
    private ArrayList<String> ssIDList;
    // ptnr1 and ptnr2 S-S bonds AsymIds.
    private ArrayList<String> ptnr1ssBondsAsymIdList;
    private ArrayList<String> ptnr2ssBondsAsymIdList;
    // ptnr1 and ptnr2 S-S bonds SeqIds.
    private ArrayList<Integer> ptnr1ssBondsSeqIdList;
    private ArrayList<Integer> ptnr2ssBondsSeqIdList;
    // ptnr1 and ptnr2 S-S bonds CompIds.
    private ArrayList<String> ptnr1ssBondsCompIdList;
    private ArrayList<String> ptnr2ssBondsCompIdList;

    // distances between S-S bonds amino acids.
    private ArrayList<Integer> ssBondsAADistanceList;
    // 3D distances between S-S bonds.
    private ArrayList<String> ssBonds3DDistanceList; // to avoid exponential notation

    // torsion angles.
    private ArrayList<String> torsionAngleList; // to avoid exponential notation

    // current AsymID (required for processing each asymID data)
    private String currentAsymId;

    // amino acids distances between glycosylation sites.
    private ArrayList<Integer> glySiteAaDistanceList;
    // tertiary distances between glycosylation sites.
    private ArrayList<String> glySiteTerDistanceList; // to avoid exponential notation

    // surface area.
    private ArrayList<String> glySite1_4AreaList; // to avoid exponential notation
    private ArrayList<String> glySite7_0AreaList; // to avoid exponential notation
    private ArrayList<String> glySite14AreaList; // to avoid exponential notation

    // Glycosylation site tag list (for RDF)
    private ArrayList<String> glycosylationSiteTagList;
    // Glycosylation distance site tag list (for RDF)
    private ArrayList<String> glycoDistanceSiteTagList;

    // status
    private int status;

    public MLFeatures() {
    }

    public Entry getEntry() {
        return entry;
    }
    public void setEntry(Entry entry) {
        this.entry = entry;
    }

    public AtomSite getAtomSite() {
        return atomSite;
    }
    public void setAtomSite(AtomSite atomSite) {
        this.atomSite = atomSite;
    }

    public StructConn getStructConn() {
        return structConn;
    }
    public void setStructConn(StructConn structConn) {
        this.structConn = structConn;
    }

	public String getEntryId() {
        return entryId;
    }
    public void setEntryId(String entryId) {
        this.entryId = entryId;
    }

	public String getAaSequence() {
        return aaSequence;
    }
    public void setAaSequence(String aaSequence) {
        this.aaSequence = aaSequence;
    }

    public ArrayList<String> getAaSequenceList() {
        return aaSequenceList;
    }
    public void setAaSequenceList(ArrayList<String> aaSequenceList) {
        this.aaSequenceList = aaSequenceList;
    }

    public ArrayList<String> getGroupList() {
        return groupList;
    }
    public void setGroupList(ArrayList<String> groupList) {
        this.groupList = groupList;
    }

    public ArrayList<String> getAaCompIdList() {
        return aaCompIdList;
    }
	public void setAaCompIdList(ArrayList<String> aaCompIdList) {
        this.aaCompIdList = aaCompIdList;
    }

	public ArrayList<String> getAaAsymIdList() {
		return aaAsymIdList;
	}
	public void setAaAsymIdList(ArrayList<String> aaAsymIdList) {
		this.aaAsymIdList = aaAsymIdList;
	}

	public ArrayList<Integer> getAaSeqIdList() {
		return aaSeqIdList;
	}
	public void setAaSeqIdList(ArrayList<Integer> aaSeqIdList) {
		this.aaSeqIdList = aaSeqIdList;
	}

	public ArrayList<String> getAaAtomIdList() {
		return aaAtomIdList;
	}
	public void setAaAtomIdList(ArrayList<String> aaAtomIdList) {
		this.aaAtomIdList = aaAtomIdList;
	}

	public ArrayList<String> getFullGroupList() {
	    return fullGroupList;
	}
    public void setFullGroupList(ArrayList<String> fullGroupList) {
        this.fullGroupList = fullGroupList;
    }

    public ArrayList<Integer> getFullAaIdList() {
        return fullAaIdList;
    }
    public void setFullAaIdList(ArrayList<Integer> fullAaIdList) {
        this.fullAaIdList = fullAaIdList;
    }

	public ArrayList<Double> getCartnXList() {
        return cartnXList;
    }
    public void setCartnXList(ArrayList<Double> cartnXList) {
        this.cartnXList = cartnXList;
    }

    public ArrayList<Double> getCartnYList() {
        return cartnYList;
    }
    public void setCartnYList(ArrayList<Double> cartnYList) {
        this.cartnYList = cartnYList;
    }

    public ArrayList<Double> getCartnZList() {
        return cartnZList;
    }
    public void setCartnZList(ArrayList<Double> cartnZList) {
        this.cartnZList = cartnZList;
    }

	public ArrayList<String> getnGlycoIdList() {
        return nGlycoIdList;
    }
    public void setnGlycoIdList(ArrayList<String> nGlycoIdList) {
        this.nGlycoIdList = nGlycoIdList;
    }

    public ArrayList<String> getnGlycoAsymIdList() {
        return nGlycoAsymIdList;
    }
    public void setnGlycoAsymIdList(ArrayList<String> nGlycoAsymIdList) {
        this.nGlycoAsymIdList = nGlycoAsymIdList;
    }

    public ArrayList<String> getnGlycoCompIdList() {
        return nGlycoCompIdList;
    }
    public void setnGlycoCompIdList(ArrayList<String> nGlycoCompIdList) {
        this.nGlycoCompIdList = nGlycoCompIdList;
    }

    public ArrayList<Integer> getnGlycoSeqIdList() {
        return nGlycoSeqIdList;
    }
    public void setnGlycoSeqIdList(ArrayList<Integer> nGlycoSeqIdList) {
        this.nGlycoSeqIdList = nGlycoSeqIdList;
    }

    public ArrayList<String> getoGlycoIdList() {
        return oGlycoIdList;
    }
    public void setoGlycoIdList(ArrayList<String> oGlycoIdList) {
        this.oGlycoIdList = oGlycoIdList;
    }

    public ArrayList<String> getoGlycoAsymIdList() {
        return oGlycoAsymIdList;
    }
    public void setoGlycoAsymIdList(ArrayList<String> oGlycoAsymIdList) {
        this.oGlycoAsymIdList = oGlycoAsymIdList;
    }

    public ArrayList<String> getoGlycoCompIdList() {
        return oGlycoCompIdList;
    }
    public void setoGlycoCompIdList(ArrayList<String> oGlycoCompIdList) {
        this.oGlycoCompIdList = oGlycoCompIdList;
    }

    public ArrayList<Integer> getoGlycoSeqIdList() {
        return oGlycoSeqIdList;
    }
    public void setoGlycoSeqIdList(ArrayList<Integer> oGlycoSeqIdList) {
        this.oGlycoSeqIdList = oGlycoSeqIdList;
    }

    public boolean isnGlyConsensusSequenceHit() {
	    return nGlyConsensusSequenceHit;
    }
    public void setnGlyConsensusSequenceHit(boolean nGlyConsensusSequenceHit) {
        this.nGlyConsensusSequenceHit = nGlyConsensusSequenceHit;
    }

	public ArrayList<Integer> getnGlyConsensusSeqStartNumList() {
        return nGlyConsensusSeqStartNumList;
    }
    public void setnGlyConsensusSeqStartNumList(ArrayList<Integer> nGlyConsensusSeqStartNumList) {
        this.nGlyConsensusSeqStartNumList = nGlyConsensusSeqStartNumList;
    }

    public ArrayList<String> getnGlyConsensusSeqPatternList() {
        return nGlyConsensusSeqPatternList;
    }
    public void setnGlyConsensusSeqPatternList(ArrayList<String> nGlyConsensusSeqPatternList) {
        this.nGlyConsensusSeqPatternList = nGlyConsensusSeqPatternList;
    }

	public ArrayList<Integer> getAsnSeqPositionList() {
        return asnSeqPositionList;
    }
    public void setAsnSeqPositionList(ArrayList<Integer> asnSeqPositionList) {
        this.asnSeqPositionList = asnSeqPositionList;
    }

    public ArrayList<Integer> getSerSeqPositionList() {
        return serSeqPositionList;
    }
    public void setSerSeqPositionList(ArrayList<Integer> serSeqPositionList) {
        this.serSeqPositionList = serSeqPositionList;
    }

    public ArrayList<Integer> getThrSeqPositionList() {
        return thrSeqPositionList;
    }
    public void setThrSeqPositionList(ArrayList<Integer> thrSeqPositionList) {
        this.thrSeqPositionList = thrSeqPositionList;
    }

    public ArrayList<Integer> getSsBondsAADistanceList() {
        return ssBondsAADistanceList;
    }
    public void setSsBondsAADistanceList(ArrayList<Integer> ssBondsAADistanceList) {
        this.ssBondsAADistanceList = ssBondsAADistanceList;
    }

    public ArrayList<String> getSsBonds3DDistanceList() {
        return ssBonds3DDistanceList;
    }
    public void setSsBonds3DDistanceList(ArrayList<String> ssBonds3DDistanceList) {
        this.ssBonds3DDistanceList = ssBonds3DDistanceList;
    }

    public ArrayList<String> getSsIDList() {
        return ssIDList;
    }
    public void setSsIDList(ArrayList<String> ssIDList) {
        this.ssIDList = ssIDList;
    }

    public ArrayList<String> getPtnr1ssBondsAsymIdList() {
        return ptnr1ssBondsAsymIdList;
    }
    public void setPtnr1ssBondsAsymIdList(ArrayList<String> ptnr1ssBondsAsymIdList) {
        this.ptnr1ssBondsAsymIdList = ptnr1ssBondsAsymIdList;
    }

    public ArrayList<String> getPtnr2ssBondsAsymIdList() {
        return ptnr2ssBondsAsymIdList;
    }
	public void setPtnr2ssBondsAsymIdList(ArrayList<String> ptnr2ssBondsAsymIdList) {
        this.ptnr2ssBondsAsymIdList = ptnr2ssBondsAsymIdList;
    }

    public ArrayList<Integer> getPtnr1ssBondsSeqIdList() {
        return ptnr1ssBondsSeqIdList;
    }
    public void setPtnr1ssBondsSeqIdList(ArrayList<Integer> ptnr1ssBondsSeqIdList) {
        this.ptnr1ssBondsSeqIdList = ptnr1ssBondsSeqIdList;
    }

    public ArrayList<Integer> getPtnr2ssBondsSeqIdList() {
        return ptnr2ssBondsSeqIdList;
    }
    public void setPtnr2ssBondsSeqIdList(ArrayList<Integer> ptnr2ssBondsSeqIdList) {
        this.ptnr2ssBondsSeqIdList = ptnr2ssBondsSeqIdList;
    }

    public ArrayList<String> getPtnr1ssBondsCompIdList() {
        return ptnr1ssBondsCompIdList;
    }
    public void setPtnr1ssBondsCompIdList(ArrayList<String> ptnr1ssBondsCompIdList) {
        this.ptnr1ssBondsCompIdList = ptnr1ssBondsCompIdList;
    }

    public ArrayList<String> getPtnr2ssBondsCompIdList() {
        return ptnr2ssBondsCompIdList;
    }
    public void setPtnr2ssBondsCompIdList(ArrayList<String> ptnr2ssBondsCompIdList) {
        this.ptnr2ssBondsCompIdList = ptnr2ssBondsCompIdList;
    }

	public String getCurrentAsymId() {
        return currentAsymId;
    }
    public void setCurrentAsymId(String currentAsymId) {
        this.currentAsymId = currentAsymId;
    }

    public ArrayList<String> getFullAaCompIdList() {
        return fullAaCompIdList;
    }
    public void setFullAaCompIdList(ArrayList<String> fullAaCompIdList) {
        this.fullAaCompIdList = fullAaCompIdList;
    }

    public ArrayList<String> getFullAaAsymIdList() {
        return fullAaAsymIdList;
    }
    public void setFullAaAsymIdList(ArrayList<String> fullAaAsymIdList) {
        this.fullAaAsymIdList = fullAaAsymIdList;
    }

    public ArrayList<Integer> getFullAaSeqIdList() {
        return fullAaSeqIdList;
    }
    public void setFullAaSeqIdList(ArrayList<Integer> fullAaSeqIdList) {
        this.fullAaSeqIdList = fullAaSeqIdList;
    }

    public ArrayList<String> getFullAaAtomIdList() {
        return fullAaAtomIdList;
    }
    public void setFullAaAtomIdList(ArrayList<String> fullAaAtomIdList) {
        this.fullAaAtomIdList = fullAaAtomIdList;
    }

    public ArrayList<Double> getFullCartnXList() {
        return fullCartnXList;
    }
    public void setFullCartnXList(ArrayList<Double> fullCartnXList) {
        this.fullCartnXList = fullCartnXList;
    }

    public ArrayList<Double> getFullCartnYList() {
        return fullCartnYList;
    }
    public void setFullCartnYList(ArrayList<Double> fullCartnYList) {
        this.fullCartnYList = fullCartnYList;
    }

    public ArrayList<Double> getFullCartnZList() {
        return fullCartnZList;
    }
    public void setFullCartnZList(ArrayList<Double> fullCartnZList) {
        this.fullCartnZList = fullCartnZList;
    }

    public ArrayList<String> getTorsionAngleList() {
        return torsionAngleList;
    }
    public void setTorsionAngleList(ArrayList<String> torsionAngleList) {
        this.torsionAngleList = torsionAngleList;
    }

    public ArrayList<Integer> getGlySiteAaDistanceList() {
        return glySiteAaDistanceList;
    }
    public void setGlySiteAaDistanceList(ArrayList<Integer> glySiteAaDistanceList) {
        this.glySiteAaDistanceList = glySiteAaDistanceList;
    }

    public ArrayList<String> getGlySiteTerDistanceList() {
        return glySiteTerDistanceList;
    }
    public void setGlySiteTerDistanceList(ArrayList<String> glySiteTerDistanceList) {
        this.glySiteTerDistanceList = glySiteTerDistanceList;
    }

    public ArrayList<String> getGlySite1_4AreaList() {
        return glySite1_4AreaList;
    }
    public void setGlySite1_4AreaList(ArrayList<String> glySite1_4AreaList) {
        this.glySite1_4AreaList = glySite1_4AreaList;
    }

    public ArrayList<String> getGlySite7_0AreaList() {
        return glySite7_0AreaList;
    }
    public void setGlySite7_0AreaList(ArrayList<String> glySite7_0AreaList) {
        this.glySite7_0AreaList = glySite7_0AreaList;
    }

    public ArrayList<String> getGlySite14AreaList() {
        return glySite14AreaList;
    }
    public void setGlySite14AreaList(ArrayList<String> glySite14AreaList) {
        this.glySite14AreaList = glySite14AreaList;
    }

    public ArrayList<String> getGlycosylationSiteTagList() {
        return glycosylationSiteTagList;
    }
    public void setGlycosylationSiteTagList(ArrayList<String> glycosylationSiteTagList) {
        this.glycosylationSiteTagList = glycosylationSiteTagList;
    }

    public ArrayList<String> getGlycoDistanceSiteTagList() {
        return glycoDistanceSiteTagList;
    }
    public void setGlycoDistanceSiteTagList(ArrayList<String> glycoDistanceSiteTagList) {
        this.glycoDistanceSiteTagList = glycoDistanceSiteTagList;
    }

    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }

	public ArrayList<Integer> getIndexByAsymId(String asymId) {
        ArrayList<Integer> asymIdxList = new ArrayList<Integer>();
        for (int i = 0; i < this.getAaAsymIdList().size(); i++) {
            if (this.getAaAsymIdList().get(i).equals(asymId)) {
                asymIdxList.add(i);
            }
        }
        return asymIdxList;
    }
}
