package org.glycoinfo.g4d.feature;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.glycoinfo.g4d.calc.calcsurface.MolSurfaceFeatures;
import org.glycoinfo.g4d.calc.calcsurface.SphereCalc;
import org.glycoinfo.g4d.config.Constants;

public class MLFeaturesUtil {

    // added. 6.Feb.2024
    public static void deleteHetAtmFromCovaleStructConn(MLFeatures mlFeatures) {
        // get atom information corresponding to the current asym ID.
        int startIndex = mlFeatures.getFullAaAsymIdList().indexOf(mlFeatures.getCurrentAsymId());
        int endIndex = mlFeatures.getFullAaAsymIdList().lastIndexOf(mlFeatures.getCurrentAsymId());
        List<String> groupList = mlFeatures.getFullGroupList().subList(startIndex, endIndex + 1);
        List<Integer> seqIdList = mlFeatures.getFullAaSeqIdList().subList(startIndex, endIndex + 1);
        // delete N-Glycosylation sites which are in HETATM lines. rare case.
        ArrayList<Integer> nGlySeqIdList = mlFeatures.getnGlycoSeqIdList();
        ArrayList<String> nGlyCompIdList = mlFeatures.getnGlycoCompIdList();
        ArrayList<String> nGlyAsymIdList = mlFeatures.getnGlycoAsymIdList();
        ArrayList<String> nGlyIdList = mlFeatures.getnGlycoIdList();
        boolean hetatmHit = false;
        for (int i = 0; i < nGlySeqIdList.size(); i++) {
            int seqId = nGlySeqIdList.get(i);
            int idx = seqIdList.indexOf(seqId);
            if (idx == -1) {
                continue;
            }
            String groupAtom = groupList.get(idx);
            if (groupAtom.equals("HETATM")) {
                hetatmHit = true;
                nGlyIdList.remove(i);
                nGlySeqIdList.remove(i);
                nGlyAsymIdList.remove(i);
                nGlyCompIdList.remove(i);
            }
        }
        if (hetatmHit == true) {
            mlFeatures.setnGlycoAsymIdList(nGlyAsymIdList);
            mlFeatures.setnGlycoCompIdList(nGlyCompIdList);
            mlFeatures.setnGlycoIdList(nGlyIdList);
            mlFeatures.setnGlycoSeqIdList(nGlySeqIdList);
        }
        // delete O-Glycosylation sites which are in HETATM lines. rare case.
        ArrayList<Integer> oGlySeqIdList = mlFeatures.getoGlycoSeqIdList();
        ArrayList<String> oGlyCompIdList = mlFeatures.getoGlycoCompIdList();
        ArrayList<String> oGlyAsymIdList = mlFeatures.getoGlycoAsymIdList();
        ArrayList<String> oGlyIdList = mlFeatures.getoGlycoIdList();
        hetatmHit = false;
        for (int i = 0; i < oGlySeqIdList.size(); i++) {
            int seqId = oGlySeqIdList.get(i);
            int idx = seqIdList.indexOf(seqId);
            if (idx == -1) { // e.g. [0, 0]6x5u
                continue;
            }
            String groupAtom = groupList.get(idx);
            if (groupAtom.equals("HETATM")) {
                hetatmHit = true;
                oGlyIdList.remove(i);
                oGlySeqIdList.remove(i);
                oGlyAsymIdList.remove(i);
                oGlyCompIdList.remove(i);
            }
        }
        if (hetatmHit == true) {
            mlFeatures.setoGlycoAsymIdList(oGlyAsymIdList);
            mlFeatures.setoGlycoCompIdList(oGlyCompIdList);
            mlFeatures.setoGlycoIdList(oGlyIdList);
            mlFeatures.setoGlycoSeqIdList(oGlySeqIdList);
        }   
    }

    public static void checkNGlyConsensusSequence(MLFeatures mlFeatures) {
    	// compared sequence.
    	int asymIdStartIndex = mlFeatures.getAaAsymIdList().indexOf(mlFeatures.getCurrentAsymId());
    	int asymIdEndIndex = mlFeatures.getAaAsymIdList().lastIndexOf(mlFeatures.getCurrentAsymId());
        // Since the position of the list of chain IDs corresponds to the position of the list containing the amino acids,
    	// the amino acid sequence corresponding to the current chain ID can be obtained by cutting out the list
    	// containing the amino acids in the same part as the current chain ID.
    	String aaSeq = String.join("", mlFeatures.getAaSequenceList().subList(asymIdStartIndex, asymIdEndIndex + 1));
        // initialization.
        ArrayList<Integer> nGlyConSeqStartNumList = new ArrayList<Integer>();
        ArrayList<String> nGlyConSeqPatternList = new ArrayList<String>();
        // pattern matches.
        Pattern p = Pattern.compile(Constants.NGLYCO_RGX_PATTERN);
        Matcher m = p.matcher(aaSeq);
        if (m != null) {
        	// set true.
            mlFeatures.setnGlyConsensusSequenceHit(true);
            // get the amino acid residue numbers corresponding to the current asymID.
            // Since the position of the list of chain IDs corresponds to the position of the list containing the residue numbers of amino acids,
            // the residue number corresponding to the current chain ID is obtained by cutting out the list
            // containing the residue numbers of amino acids in the same part as the current chain ID.
            List<Integer> aaSeqIdList = mlFeatures.getAaSeqIdList().subList(asymIdStartIndex, asymIdEndIndex + 1);
            while (m.find()) {
                int startIndexNum = m.start();
                String matchSeq = m.group();
                String trimSeq = matchSeq.substring(0, 3);
                nGlyConSeqStartNumList.add(aaSeqIdList.get(startIndexNum));
                nGlyConSeqPatternList.add(trimSeq);
            }
            mlFeatures.setnGlyConsensusSeqStartNumList(nGlyConSeqStartNumList);
            mlFeatures.setnGlyConsensusSeqPatternList(nGlyConSeqPatternList);
        } else {
            mlFeatures.setnGlyConsensusSequenceHit(false);
            // set empty data.
            mlFeatures.setnGlyConsensusSeqStartNumList(nGlyConSeqStartNumList);
            // set empty data.
            mlFeatures.setnGlyConsensusSeqPatternList(nGlyConSeqPatternList);
        }
    }

    public static void setAsnSerThrResidueNum(MLFeatures mlFeatures) {
    	// get asymID list corresponding to the current asymID.
    	int asymIdStartIndex = mlFeatures.getAaAsymIdList().indexOf(mlFeatures.getCurrentAsymId());
    	int asymIdEndIndex = mlFeatures.getAaAsymIdList().lastIndexOf(mlFeatures.getCurrentAsymId());
    	// get all amino acids (one letter code) corresponding to the current asymID.
    	List<String> aaSequenceList = mlFeatures.getAaSequenceList().subList(asymIdStartIndex, asymIdEndIndex + 1);
        // initialize.
        ArrayList<Integer> asnResNumList = new ArrayList<Integer>();
        // set ASN residue numbers in N-Glycosylation consensus patterns.
        boolean nGlyConsensusSequenceHit = mlFeatures.isnGlyConsensusSequenceHit();
        if (nGlyConsensusSequenceHit == true) {
            ArrayList<String> nGlyConSeqList = mlFeatures.getnGlyConsensusSeqPatternList();
            ArrayList<Integer> nGlyConSeqNumList = mlFeatures.getnGlyConsensusSeqStartNumList();
            for (int i = 0; i < nGlyConSeqList.size(); i++) {
            	int nGlyPosNum = nGlyConSeqNumList.get(i);
            	// N-glycosylation site begins with "ASN"
                asnResNumList.add(nGlyPosNum);
                // N-Glycosylation motif is "N-X{P}-T/S-{P}". i.e. check only second amino acid.
                if (nGlyConSeqList.get(i).indexOf("N", 1) != -1) {
                	asnResNumList.add(nGlyPosNum + 1);
                }
            }
        }
        // set ASN positions.
        mlFeatures.setAsnSeqPositionList(asnResNumList);

    	// get all SeqIDs.
        // Since the list of chain IDs and the list of residue information of amino acids correspond in their positions,
        // the list of residue information of the amino acid can be obtained by cutting out the part corresponding to the current chain ID.
        List<Integer> aaSequenceResNumList = mlFeatures.getAaSeqIdList().subList(asymIdStartIndex, asymIdEndIndex + 1);
        // set SER and THR residue numbers to ArrayList.
        ArrayList<Integer> serResNumList = new ArrayList<Integer>();
        ArrayList<Integer> thrResNumList = new ArrayList<Integer>();
        for (int i = 0; i < aaSequenceList.size(); i++) {
            if (aaSequenceList.get(i).equals("S")) {
                serResNumList.add(aaSequenceResNumList.get(i));
            }
            if (aaSequenceList.get(i).equals("T")) {
                thrResNumList.add(aaSequenceResNumList.get(i));
            }
        }
        // set SER and THR positions.
        mlFeatures.setSerSeqPositionList(serResNumList);
        mlFeatures.setThrSeqPositionList(thrResNumList);
    }

    public static void calcSSBondAaDistance(MLFeatures mlFeatures) {
        // initialize.
        ArrayList<Integer> ssBondAaDistanceList = new ArrayList<Integer>();
        // get start and end indexes corresponding to the current asymID.
    	// First, check whether the two SS-bond residues are on the same chain or not. reject if not because the length of the amino acids
        // cannot be calculated. 7a02: The asymID of ptnr2 is different from that of ptnr1, so it is impossible to calculate the ss bond
        // aa distances.  
        ArrayList<Boolean> sameChainList = new ArrayList<Boolean>();
        int startIndex = mlFeatures.getPtnr1ssBondsAsymIdList().indexOf(mlFeatures.getCurrentAsymId());
        int endIndex = mlFeatures.getPtnr1ssBondsAsymIdList().lastIndexOf(mlFeatures.getCurrentAsymId());
        if (startIndex != -1 && endIndex != -1) {
            List<String> ptnr1AsymIdList = mlFeatures.getPtnr1ssBondsAsymIdList().subList(startIndex, endIndex + 1);
            for (int i = 0; i < ptnr1AsymIdList.size(); i++) {
                if (mlFeatures.getPtnr1ssBondsAsymIdList().get(startIndex).equals(mlFeatures.getPtnr2ssBondsAsymIdList().get(startIndex))) {
                    sameChainList.add(true);
                } else {
                    sameChainList.add(false);
                }
            }
        }
        // get S-S bond residue list corresponding to the current asymID.
        if (startIndex != -1 && endIndex != -1) {
            List<Integer> ptnr1SeqIdList = mlFeatures.getPtnr1ssBondsSeqIdList().subList(startIndex, endIndex + 1);
            List<Integer> ptnr2SeqIdList = mlFeatures.getPtnr2ssBondsSeqIdList().subList(startIndex, endIndex + 1);
            for (int i = 0; i < ptnr1SeqIdList.size(); i++) {
                if (sameChainList.get(i) == false) {
                	ssBondAaDistanceList.add(-1); // added. 18.Dec.2023
                    continue;
                }
                // Not including C between S-S bonds
                ssBondAaDistanceList.add((ptnr2SeqIdList.get(i) - 1) - (ptnr1SeqIdList.get(i) + 1) + 1);
            }
        }
        mlFeatures.setSsBondsAADistanceList(ssBondAaDistanceList);
    }

    public static void calcSSBond3DDistance(MLFeatures mlFeatures) {
        // get the start and end indexes of the amino acid information corresponding to the current asymID.
        // for ptnr1
        int startIndex = mlFeatures.getAaAsymIdList().indexOf(mlFeatures.getCurrentAsymId());
        int endIndex = mlFeatures.getAaAsymIdList().lastIndexOf(mlFeatures.getCurrentAsymId());
        // initialize.
        ArrayList<String> ssBond3DDistList = new ArrayList<String>();
        // get the residue numbers of the amino acid sequence corresponding to the current asymID.
        List<Integer> aaSeqIdList = mlFeatures.getAaSeqIdList().subList(startIndex, endIndex + 1);
        // get 3D coordinate information corresponding to the current asymID (Ca only).
        List<Double> cartnXList = mlFeatures.getCartnXList().subList(startIndex, endIndex + 1);
        List<Double> cartnYList = mlFeatures.getCartnYList().subList(startIndex, endIndex + 1);
        List<Double> cartnZList = mlFeatures.getCartnZList().subList(startIndex, endIndex + 1);
        // get start and end indexes of structure information corresponding to the current asymID.
        int ssStartIndex = mlFeatures.getPtnr1ssBondsAsymIdList().indexOf(mlFeatures.getCurrentAsymId());
        int ssEndIndex = mlFeatures.getPtnr1ssBondsAsymIdList().lastIndexOf(mlFeatures.getCurrentAsymId());
        if (ssStartIndex != -1 && ssEndIndex != -1) { // If there is a disulf row in _struct_conn corresponding to the chain identifier
            // S-S bond residue numbers.
            List<Integer> ptnr1ssBondSeqIdList = mlFeatures.getPtnr1ssBondsSeqIdList().subList(ssStartIndex, ssEndIndex + 1);
            List<Integer> ptnr2ssBondSeqIdList = mlFeatures.getPtnr2ssBondsSeqIdList().subList(ssStartIndex, ssEndIndex + 1);
            List<String> ptnr2ssBondAsymIdList = mlFeatures.getPtnr2ssBondsAsymIdList().subList(ssStartIndex, ssEndIndex + 1);
            for (int i = 0; i < ptnr1ssBondSeqIdList.size(); i++) {
                // for ptnr2.
                String ptnr2AsymId = ptnr2ssBondAsymIdList.get(i);
                int startIndex2 = mlFeatures.getAaAsymIdList().indexOf(ptnr2AsymId);
                int endIndex2 = mlFeatures.getAaAsymIdList().lastIndexOf(ptnr2AsymId);
                if (startIndex2 == -1 || endIndex2 == -1) { // e.g. ptnr1 "157", ptnr2 "."
                	ssBond3DDistList.add("-1.0");
                    continue;
                }
                List<Integer> ptnr2AaSeqIdList = mlFeatures.getAaSeqIdList().subList(startIndex2, endIndex2 + 1);
                List<Double> cartnXList2 = mlFeatures.getCartnXList().subList(startIndex2, endIndex2 + 1);
                List<Double> cartnYList2 = mlFeatures.getCartnYList().subList(startIndex2, endIndex2 + 1);
                List<Double> cartnZList2 = mlFeatures.getCartnZList().subList(startIndex2, endIndex2 + 1);
                int ptnr1ssSeqIdIndex = aaSeqIdList.indexOf(ptnr1ssBondSeqIdList.get(i));
                int ptnr2ssSeqIdIndex = ptnr2AaSeqIdList.indexOf(ptnr2ssBondSeqIdList.get(i));
                if (ptnr2ssSeqIdIndex == -1 || ptnr1ssSeqIdIndex == -1) { // e.g. ptnr1: HETATM (3ax3, 3ax5) currently, HETATM added.
                	ssBond3DDistList.add("-1.0");
                    continue;
                }
                Double x1 = cartnXList.get(ptnr1ssSeqIdIndex);
                Double y1 = cartnYList.get(ptnr1ssSeqIdIndex);
                Double z1 = cartnZList.get(ptnr1ssSeqIdIndex);
                Double x2 = cartnXList2.get(ptnr2ssSeqIdIndex);
                Double y2 = cartnYList2.get(ptnr2ssSeqIdIndex);
                Double z2 = cartnZList2.get(ptnr2ssSeqIdIndex);
                Double ss3DDist = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2) + Math.pow(z1 - z2, 2));
                String trimSs3DDist = BigDecimal.valueOf(((double)Math.round(ss3DDist * 1000)) / 1000).toPlainString();
                ssBond3DDistList.add(trimSs3DDist);
            }
        }
        mlFeatures.setSsBonds3DDistanceList(ssBond3DDistList);
    }

    public static void calcTorsionAngle(MLFeatures mlFeatures) {
        List<Integer> nConSeqIdList = mlFeatures.getnGlyConsensusSeqStartNumList();
        if (nConSeqIdList.size() == 0) {
            mlFeatures.setTorsionAngleList(new ArrayList<String>()); // set empty ArrayList.
        } else {
            runTorsionAngleByEachRes(mlFeatures, nConSeqIdList);
        }
    }

    public static void runTorsionAngleByEachRes(MLFeatures mlFeatures, List<Integer> nConSeqIdList) {
    	// Only the data of the current chain identifier is stored in the list from the list with all ATOM information.
    	int aaStart = mlFeatures.getFullAaAsymIdList().indexOf(mlFeatures.getCurrentAsymId());
    	int aaEnd = mlFeatures.getFullAaAsymIdList().lastIndexOf(mlFeatures.getCurrentAsymId());
        List<String> fullAaAtomIdList = mlFeatures.getFullAaAtomIdList().subList(aaStart, aaEnd + 1);
        List<Integer> fullAaSeqIdList = mlFeatures.getFullAaSeqIdList().subList(aaStart, aaEnd + 1);
        // list to store torsion angles
        ArrayList<String> torsionAngleList = new ArrayList<String>();
        // Processed per glycosylation site. The C of the preceding residue is also required.
        for (int resNum : nConSeqIdList) {
        	// C of the one residue before the glycosylation residue, and a region of two residues to obtain the N, Cα and C of the glycosylation residue
            int preResNum = resNum - 1;
            int preStart = fullAaSeqIdList.indexOf(preResNum);
            int preEnd = fullAaSeqIdList.lastIndexOf(preResNum);
            if (preStart == -1 || preEnd == -1) {
            	torsionAngleList.add("");
            	System.out.println("Missing residue: " + preResNum);
                continue;
            }
            List<String> aaAtomIdList = fullAaAtomIdList.subList(preStart, preEnd + 1);
            List<Double> cartnXList = mlFeatures.getFullCartnXList().subList(preStart, preEnd + 1);
            List<Double> cartnYList = mlFeatures.getFullCartnYList().subList(preStart, preEnd + 1);
            List<Double> cartnZList = mlFeatures.getFullCartnZList().subList(preStart, preEnd + 1);
            Vector3D c1 = null;
            for (int i = 0; i < aaAtomIdList.size(); i++) {
                if (aaAtomIdList.get(i).equals(Constants.C_TAG)) {
                    c1 = setCoor(cartnXList.get(i), cartnYList.get(i), cartnZList.get(i));
                    break;
                }
            }
            int start = fullAaSeqIdList.indexOf(resNum);
            int end = fullAaSeqIdList.lastIndexOf(resNum);
            aaAtomIdList = fullAaAtomIdList.subList(start, end + 1);
            cartnXList = mlFeatures.getFullCartnXList().subList(start, end + 1);
            cartnYList = mlFeatures.getFullCartnYList().subList(start, end + 1);
            cartnZList = mlFeatures.getFullCartnZList().subList(start, end + 1);
            Vector3D n = null;
            Vector3D ca = null;
            Vector3D c2 = null;
            for (int i = 0; i < aaAtomIdList.size(); i++) {
                if (aaAtomIdList.get(i).equals(Constants.C_TAG)) {
                    c2 = setCoor(cartnXList.get(i), cartnYList.get(i), cartnZList.get(i));
                } else if (aaAtomIdList.get(i).equals(Constants.CALPHA_TAG)) {
                	ca = setCoor(cartnXList.get(i), cartnYList.get(i), cartnZList.get(i));
                } else if (aaAtomIdList.get(i).equals(Constants.N_TAG)) {
                	n = setCoor(cartnXList.get(i), cartnYList.get(i), cartnZList.get(i));
                } else {}
            }
            String torsionAngleDegree = "";
            if (c1 != null && n != null && ca != null && c2 != null) {
                torsionAngleDegree = String.valueOf(torsionAngle(c1, n, ca, c2));
            } else {
                System.out.println(mlFeatures.getEntryId() + " " + mlFeatures.getCurrentAsymId() + " Residue number: " + resNum + ", Missing atoms: C, N, CA, and/or C.");
            }
            torsionAngleList.add(torsionAngleDegree);
        }
        mlFeatures.setTorsionAngleList(torsionAngleList);
    }

    public static Vector3D setCoor(double x, double y, double z) {
    	return new Vector3D(x, y, z);
    }

    public static double torsionAngle(Vector3D c1, Vector3D n, Vector3D ca, Vector3D c2) {
        Vector3D vC1N = c1.subtract(n);
        Vector3D vNCA = n.subtract(ca);
        Vector3D vCAC2 = ca.subtract(c2);
        Vector3D normVectorC1NCA = vC1N.crossProduct(vNCA);
        Vector3D normVectorNCAC2 = vNCA.crossProduct(vCAC2);
        double angle = Math.atan2(normVectorC1NCA.crossProduct(normVectorNCAC2).getNorm(),
                                  normVectorC1NCA.dotProduct(normVectorNCAC2));
        String torsionAngleDegree = BigDecimal.valueOf(((double)Math.round(Math.toDegrees(angle) * 1000)) / 1000).toPlainString();
        return Double.parseDouble(torsionAngleDegree);
    }

    public static void calcGlycoAminoAcidsDistance(MLFeatures mlFeatures) {
        // initialization.
    	ArrayList<Integer> glyAaDistanceList = new ArrayList<Integer>();
    	// modified. 14.Mar.2024 
        // no glycosylation site exists.
    	List<Integer> nConSeqIdList = mlFeatures.getnGlyConsensusSeqStartNumList();
    	if (nConSeqIdList.size() == 0) {
    	    mlFeatures.setGlySiteAaDistanceList(glyAaDistanceList);
        } else {
            if (nConSeqIdList.size() > 1) {
                for (int i = 0; i < nConSeqIdList.size() - 1; i++) {
                    int aaDistance = (nConSeqIdList.get(i + 1) - 1) - (nConSeqIdList.get(i) + 1) + 1;
                    glyAaDistanceList.add(aaDistance);
                }
        	}
        	mlFeatures.setGlySiteAaDistanceList(glyAaDistanceList);
        }
    }


    public static void calcSphere(MLFeatures mlFeatures) {
        int aaStart = mlFeatures.getFullAaAsymIdList().indexOf(mlFeatures.getCurrentAsymId());
        int aaEnd = mlFeatures.getFullAaAsymIdList().lastIndexOf(mlFeatures.getCurrentAsymId());
        List<Integer> fullAaIdList = mlFeatures.getFullAaIdList().subList(aaStart, aaEnd + 1);
        List<Integer> fullAaSeqIdList = mlFeatures.getFullAaSeqIdList().subList(aaStart, aaEnd + 1);
        List<String> fullAaAtomIdList = mlFeatures.getFullAaAtomIdList().subList(aaStart, aaEnd + 1);
        // for surface area
        ArrayList<String> sphere1_4List = new ArrayList<String>();
        ArrayList<String> sphere7_0List = new ArrayList<String>();
        ArrayList<String> sphere14List = new ArrayList<String>();
        // for tertiary distance 
        ArrayList<String> terDistList = new ArrayList<String>();

        // for RDF.
        ArrayList<String> glycoSiteTagList = new ArrayList<String>();
        ArrayList<String> glycoDistanceTagList = new ArrayList<String>();

        // modified. 14.Mar.2024
        // get merged (N and O) glycosylation residues.
        List<Integer> nConSeqResList = mlFeatures.getnGlyConsensusSeqStartNumList();
        // for calculating asa and distance.
        List<SphereCalc.IndexPair> areaIdxs = new ArrayList<SphereCalc.IndexPair>();
        List<SphereCalc.IndexPair> distanceIdxs = new ArrayList<SphereCalc.IndexPair>();
        // for setting asa
        ArrayList<Boolean> validAsaResList = new ArrayList<Boolean>();
        // for setting distances
        ArrayList<Boolean> validDistanceResList = new ArrayList<Boolean>();

        // for each glyco residues.
        for (int i = 0; i < nConSeqResList.size(); i++) {
            int glycoResStart = fullAaSeqIdList.indexOf(nConSeqResList.get(i));
            int glycoResEnd = fullAaSeqIdList.lastIndexOf(nConSeqResList.get(i));
            if (glycoResStart == -1 || glycoResEnd == -1) {
                glycoSiteTagList.add(""); // set empty
                validAsaResList.add(false);
                continue;
            } else if (glycoResEnd == glycoResStart) { // i.e. Ca only.
                glycoSiteTagList.add(""); // set empty
                validAsaResList.add(false);
                System.out.println(mlFeatures.getEntryId() + " " + mlFeatures.getCurrentAsymId() + " Residue number: " + glycoResEnd + ", CA atom only"); 
                continue;                
            } else {
                glycoSiteTagList.add(String.valueOf(nConSeqResList.get(i)));
                validAsaResList.add(true);
            }
            int atomStart = fullAaIdList.get(glycoResStart);
            int atomEnd = fullAaIdList.get(glycoResEnd);
            areaIdxs.add(new SphereCalc.IndexPair(atomStart, atomEnd));
            if (i < nConSeqResList.size() - 1) {
                int nextGlycoResEnd = fullAaSeqIdList.lastIndexOf(nConSeqResList.get(i + 1));
                int calphaStart = fullAaAtomIdList.subList(glycoResStart, nextGlycoResEnd + 1).indexOf(Constants.CALPHA_TAG);
                int calphaNextEnd = fullAaAtomIdList.subList(glycoResStart, nextGlycoResEnd + 1).lastIndexOf(Constants.CALPHA_TAG);
                if (calphaStart == -1 || calphaNextEnd == -1) {
                    glycoDistanceTagList.add(""); // set empty.
                    validDistanceResList.add(false);
                } else {
                    glycoDistanceTagList.add(String.valueOf(nConSeqResList.get(i)) + "," + String.valueOf(nConSeqResList.get(i + 1)));
                    validDistanceResList.add(true);
                }
                int caIdStart = fullAaIdList.subList(glycoResStart, nextGlycoResEnd + 1).get(calphaStart);
                int caIdEnd = fullAaIdList.subList(glycoResStart, nextGlycoResEnd + 1).get(calphaNextEnd);
                distanceIdxs.add(new SphereCalc.IndexPair(caIdStart, caIdEnd));
            }
        }
        // calculate asa and surface distances.
        try {
            if (areaIdxs.size() > 0) {
                SphereCalc.FeatureResult r = MolSurfaceFeatures.calcFeatures(
                            mlFeatures.getAtomSite(),
                            areaIdxs.iterator(), areaIdxs.size(),
                            distanceIdxs.iterator(), distanceIdxs.size());
                // get results.
                int lidx = 0;
                for (SphereCalc.SurfaceAreas sa : r.areas) {
                    if (validAsaResList.get(lidx) == false) { // i.e. missing residues.
                        sphere1_4List.add("-1.0");
                        sphere7_0List.add("-1.0");
                        sphere14List.add("-1.0");
                        lidx++;
                        continue;
                    }
                    if (sa.area_1_4 >= 0.0) {
                        sphere1_4List.add(BigDecimal.valueOf((double)Math.round(sa.area_1_4 * 1000) / 1000).toPlainString());
                    } else {
                        sphere1_4List.add("-1.0");
                    }
                    if (sa.area_7_0 >= 0.0) {
                        sphere7_0List.add(BigDecimal.valueOf((double)Math.round(sa.area_7_0 * 1000) / 1000).toPlainString());
                    } else {
                        sphere7_0List.add("-1.0");
                    }
                    if (sa.area_14_0 >= 0.0) {
                        sphere14List.add(BigDecimal.valueOf((double)Math.round(sa.area_14_0 * 1000) / 1000).toPlainString());
                    } else {
                        sphere14List.add("-1.0");
                    }
                }
                lidx = 0; // initialization.
                for (Double d : r.distances) {
                    if (validDistanceResList.get(lidx) == false) { // i.e. missing residues.
                        terDistList.add("-1.0");
                        lidx++;
                        continue;
                    }
                    if (d >= 0.0) {
                        terDistList.add(BigDecimal.valueOf((double)Math.round(d * 1000) / 1000).toPlainString());
                    } else {
                        terDistList.add("-1.0");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mlFeatures.setGlySite1_4AreaList(sphere1_4List);
        mlFeatures.setGlySite7_0AreaList(sphere7_0List);
        mlFeatures.setGlySite14AreaList(sphere14List);
        mlFeatures.setGlySiteTerDistanceList(terDistList);
        // for RDF.
        mlFeatures.setGlycosylationSiteTagList(glycoSiteTagList);
        mlFeatures.setGlycoDistanceSiteTagList(glycoDistanceTagList);
    }

    public static LinkedHashMap<Integer, String> getCurrentAsymIdAaSeqIdMap(MLFeatures mlFeatures) {
    	LinkedHashMap<Integer, String> seqIdCompIdMap = new LinkedHashMap<Integer, String>();
        ArrayList<String> aaAsymIdList = mlFeatures.getAaAsymIdList();
        ArrayList<String> aaCompIdList = mlFeatures.getAaCompIdList();
        ArrayList<Integer> aaSeqIdList = mlFeatures.getAaSeqIdList();
        int startIndex = aaAsymIdList.indexOf(mlFeatures.getCurrentAsymId());
        int endIndex = aaAsymIdList.lastIndexOf(mlFeatures.getCurrentAsymId());
        List<String> curCompIdList = aaCompIdList.subList(startIndex, endIndex + 1);
        List<Integer> curSeqIdList = aaSeqIdList.subList(startIndex, endIndex + 1);
        for (int i = 0; i < curSeqIdList.size(); i++) {
            seqIdCompIdMap.put(curSeqIdList.get(i), curCompIdList.get(i));
        }
        return seqIdCompIdMap;
    }

    public static List<Integer> mergeGlycoSeqIdList(MLFeatures mlFeatures) {
        int startIndex = mlFeatures.getnGlycoAsymIdList().indexOf(mlFeatures.getCurrentAsymId());
        int endIndex = mlFeatures.getnGlycoAsymIdList().lastIndexOf(mlFeatures.getCurrentAsymId());
        List<Integer> nGlySeqIdList = new ArrayList<Integer>();
        if (startIndex != -1 && endIndex != -1) {
            nGlySeqIdList = mlFeatures.getnGlycoSeqIdList().subList(startIndex, endIndex + 1);
        }
        int oStartIndex = mlFeatures.getoGlycoAsymIdList().indexOf(mlFeatures.getCurrentAsymId());
        int oEndIndex = mlFeatures.getoGlycoAsymIdList().lastIndexOf(mlFeatures.getCurrentAsymId());
        List<Integer> oGlySeqIdList = new ArrayList<Integer>();
        if (oStartIndex != -1 && oEndIndex != -1) {
            oGlySeqIdList = mlFeatures.getoGlycoSeqIdList().subList(oStartIndex, oEndIndex + 1);
        }
        List<Integer> mergeGlySeqIdList = new ArrayList<Integer>();
        if (nGlySeqIdList.size() > 0) {
            mergeGlySeqIdList.addAll(nGlySeqIdList);
        }
        if (oGlySeqIdList.size() > 0) {
            mergeGlySeqIdList.addAll(oGlySeqIdList);
        }
        List<Integer> sortGlySeqIdList = mergeGlySeqIdList.stream().sorted().collect(Collectors.toList());
        return sortGlySeqIdList;
    }
}
