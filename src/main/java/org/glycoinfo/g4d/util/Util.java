package org.glycoinfo.g4d.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class Util {
    public Util() {
    }
    // Formats the given time in milliseconds to a string representation in seconds.
    public static String formatTime(long milliSeconds) {
        // msec = milliSeconds % 1000;
        // long sec = milliSeconds / 1000;
        // long min = (sec % 3600) / 60;
        // long hour = sec / 3600;
        return String.format("%2.3f s", (float)milliSeconds / 1000 );
    }

    // Checks if the provided URL exists by making a GET request and checking the response code.
    public static int existsUrl(URL url) {
        try {
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.setInstanceFollowRedirects(false);
            connection.connect();
            int status = connection.getResponseCode();
            connection.disconnect();
            if (status == 404) {
                return 404; // Not Found
            }
            if (status == 503) {
                return 503; // Service Unavailable
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return 200; // OK
    }
}
