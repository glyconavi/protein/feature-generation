package org.glycoinfo.g4d.calc.calcsurface;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.rcsb.cif.schema.mm.AtomSite;

import org.glycoinfo.g4d.calc.calcsurface.SphereCalc.Sphere;

public class AtomSiteToSphereIterator implements Iterator<SphereCalc.Sphere> {
    private String nextSymbol;
    private Iterator<Double> xIter;
    private Iterator<Double> yIter;
    private Iterator<Double> zIter;
    private Iterator<String> symbolIter;
    private Iterator<String> groupIter;

    public AtomSiteToSphereIterator(AtomSite atomSite) {
        this.xIter = atomSite.getCartnX().values().iterator();
        this.yIter = atomSite.getCartnY().values().iterator();
        this.zIter = atomSite.getCartnZ().values().iterator();
        this.symbolIter = atomSite.getTypeSymbol().values().iterator();
        this.groupIter = atomSite.getGroupPDB().values().iterator();
        this.setNext();
    }

	@Override
	public boolean hasNext() {
	    return (this.nextSymbol != null);
	}

	@Override
	public SphereCalc.Sphere next() throws SphereCalcException {
	    if (this.nextSymbol != null) {
	        Sphere val = new Sphere();
	        try {
                val.r = SymbolToRadius.instance().getRadius(this.nextSymbol);
            } catch (NullPointerException e) {
                throw new SphereCalcException(
                        "Unknown atom symbol \"" + this.nextSymbol + "\".");
            }
	        val.x = this.xIter.next();
	        val.y = this.yIter.next();
	        val.z = this.zIter.next();
	        this.setNext();
	        return val;
        } else {
            throw new NoSuchElementException();
        }
	}

	private void setNext() {
	    while (this.symbolIter.hasNext()) {
	        if (this.groupIter.next().compareTo("ATOM") != 0) {
	            this.symbolIter.next();
                this.xIter.next();
                this.yIter.next();
                this.zIter.next();
                continue;
            }
	        this.nextSymbol = this.symbolIter.next().toUpperCase();
            if (this.nextSymbol.compareTo("H") != 0) {
                return;
            }
            this.xIter.next();
            this.yIter.next();
            this.zIter.next();
        }
        this.nextSymbol = null;
	}
}
