package org.glycoinfo.g4d.calc.calcsurface;

public class SphereCalcException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    SphereCalcException(String msg) {
        super(msg);
    }
}
