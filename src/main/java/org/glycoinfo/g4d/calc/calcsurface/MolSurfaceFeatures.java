package org.glycoinfo.g4d.calc.calcsurface;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.rcsb.cif.schema.mm.AtomSite;

import org.glycoinfo.g4d.calc.calcsurface.SphereCalc.IndexPair;

public class MolSurfaceFeatures {
    /**
     * Calculates features of the molecular surface.
     * @param atomSite Molecular data
     * @param areaRanges Specifies the range for calculating the surface area by atom ID.
     *                   Includes the atom on the trailing side.
     *                   If the ID of a hydrogen atom is specified,
     *                   it behaves the same as when specifying an atom other than the nearest hydrogen atom
     *                   in the same residue.
     * @param num_areas Number of elements in areaRanges
     * @param distances Specifies the 2 atoms for calculating surface distance by atom ID.
     * @param num_distances Number of elements in distances
     * @return Features stored in the same order as areaRanges and distances
     */
    public static SphereCalc.FeatureResult calcFeatures(
            AtomSite atomSite,
            Iterator<IndexPair> areaRanges, int num_areas,
            Iterator<IndexPair> distances, int num_distances)
            throws SphereCalcException {
        IdToIndexResult r = createIdToIndex(atomSite);
        AtomSiteToSphereIterator sphereItr =
            new AtomSiteToSphereIterator(atomSite);
        return SphereCalc.calcSurfaceFeatures(
                sphereItr, r.numAtoms,
                new IdToIndexIterator(areaRanges, r.idToIndex, true),
                num_areas,
                new IdToIndexIterator(distances, r.idToIndex, false),
                num_distances);
    }

    private static IdToIndexResult createIdToIndex(AtomSite atomSite) {
        IdToIndexResult ret = new IdToIndexResult();
        Iterator<String> symbolItr = atomSite.getTypeSymbol().values().iterator();
        Iterator<Integer> seqIdItr = atomSite.getLabelSeqId().values().iterator();
        Iterator<String> groupItr = atomSite.getGroupPDB().values().iterator();
        int lastId = atomSite.getId().get(atomSite.getId().getRowCount() - 1);
        ret.idToIndex = new ArrayList<>(lastId + 1);
        Iterator<Integer> idItr = atomSite.getId().values().iterator();
        int idx = -1;
        int prevSeq = -1;
        while (symbolItr.hasNext()) {
            int id = idItr.next();
            int seq = seqIdItr.next();
            String symbol = symbolItr.next();
            if (groupItr.next().compareTo("ATOM") != 0) {
                continue;
            }
            while (id > ret.idToIndex.size()) {
                ret.idToIndex.add(null);
            }
            if (symbol.compareTo("H") != 0) {
                prevSeq = seq;
                ++idx;
                ret.idToIndex.add(idx);
            } else {
                if (prevSeq == seq) {
                    ret.idToIndex.add(idx);
                } else {
                    ret.idToIndex.add(idx + 1);
                }
            }
        }
        ret.numAtoms = idx + 1;
        return ret;
    }

    private static class IdToIndexResult {
        public int numAtoms;
        public List<Integer> idToIndex;
    }

    private static class IdToIndexIterator implements Iterator<IndexPair> {
        private Iterator<IndexPair> idItr;
        private List<Integer> idToIndex;
        private int addEnd;

        public IdToIndexIterator(
                Iterator<IndexPair> idItr, List<Integer> idToIndex,
                boolean addEnd) {
            this.idItr = idItr;
            this.idToIndex = idToIndex;
            if (addEnd) {
                this.addEnd = 1;
            } else {
                this.addEnd = 0;
            }
        }

		@Override
		public boolean hasNext() {
		    return this.idItr.hasNext();
		}

		@Override
		public IndexPair next() {
		    IndexPair id = this.idItr.next();
		    return new IndexPair(
		            this.idToIndex.get(id.idx0),
		            this.idToIndex.get(id.idx1) + this.addEnd);
		}
    }
}
