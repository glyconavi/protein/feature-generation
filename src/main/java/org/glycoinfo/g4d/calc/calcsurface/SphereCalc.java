package org.glycoinfo.g4d.calc.calcsurface;

import java.util.ArrayList;
import java.util.Iterator;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

public class SphereCalc {
    public SphereCalc() {}

    public static class Sphere {
        double x;
        double y;
        double z;
        double r;
    };

    public static class IndexPair {
        public IndexPair(int idx0, int idx1) {
            this.idx0 = idx0;
            this.idx1 = idx1;
        }
        int idx0;
        int idx1;
    }

    public static class SurfaceAreas {
        public SurfaceAreas(
                double area_1_4, double area_7_0, double area_14_0) {
            this.area_1_4 = area_1_4;
            this.area_7_0 = area_7_0;
            this.area_14_0 = area_14_0;
        }
        public double area_1_4;
        public double area_7_0;
        public double area_14_0;
    }

    public static class FeatureResult {
        public ArrayList<SurfaceAreas> areas;
        public ArrayList<Double> distances;
    }

    /**
     * Calculates features of a set of spherical surfaces.
     * @param spheres Set of spheres
     * @param numSpheres Number of elements in spheres
     * @param surfaceAreaRanges Set of element index ranges for surface area calculation
     *                            The index at the end is not included
     * @param numAreas Number of elements in surfaceAreaRanges
     * @param surfaceDistances Set of index pairs for surface distance calculation between 2 spheres
     * @param numDistances Number of elements in surfaceDistances
     * @return Features stored in the same order as surfaceAreaRanges and surfaceDistances
     */
    public static FeatureResult calcSurfaceFeatures(
            Iterator<Sphere> spheres,
            int numSpheres,
            Iterator<IndexPair> surfaceAreaRanges,
            int numAreas,
            Iterator<IndexPair> surfaceDistances,
            int numDistances
            ) {
        PointerByReference area_result = new PointerByReference();
        PointerByReference distance_result = new PointerByReference();
        LibSphereCalcMapper.INSTANCE.calc_surface_features(
                area_result, distance_result,
                sphereArrayFromIterator(spheres, numSpheres), numSpheres,
                indexPairArrayFromIterator(surfaceAreaRanges, numAreas),
                numAreas,
                indexPairArrayFromIterator(surfaceDistances, numDistances),
                numDistances);

        FeatureResult result = new FeatureResult();
        result.areas = areaArrayFromPointer(area_result.getValue(), numAreas);
        result.distances = doubleArrayFromPointer(
                distance_result.getValue(), numDistances);
        LibSphereCalcMapper.INSTANCE.free_surface_result(
            area_result, distance_result);
        return result;
    }

    private static LibSphereCalcMapper.Sphere.ByReference
            sphereArrayFromIterator(Iterator<Sphere> spheres, int numSpheres) {
        final LibSphereCalcMapper.Sphere.ByReference arrayRef
            = new LibSphereCalcMapper.Sphere.ByReference();
        final LibSphereCalcMapper.Sphere[] array
            = (LibSphereCalcMapper.Sphere[])arrayRef.toArray(numSpheres);
        for (int i = 0; i < numSpheres; ++i) {
            Sphere s = spheres.next();
            array[i].radius = s.r;
            array[i].center.x = s.x;
            array[i].center.y = s.y;
            array[i].center.z = s.z;
        }
        return arrayRef;
    }

    private static LibSphereCalcMapper.IntIndexPair.ByReference
            indexPairArrayFromIterator(Iterator<IndexPair> src, int num) {
        final LibSphereCalcMapper.IntIndexPair.ByReference arrayRef
            = new LibSphereCalcMapper.IntIndexPair.ByReference();
        if (num > 0) {
            final LibSphereCalcMapper.IntIndexPair[] array
                = (LibSphereCalcMapper.IntIndexPair[])arrayRef.toArray(num);
            for (int i = 0; i < num; ++i) {
                IndexPair idx = src.next();
                array[i].start = idx.idx0;
                array[i].end = idx.idx1;
            }
        }
        return arrayRef;
    }

    private static ArrayList<SurfaceAreas> areaArrayFromPointer(
            Pointer areaArray, int num) {
        long structSize = (new LibSphereCalcMapper.SurfaceAreaResult()).size();
        ArrayList<SurfaceAreas> ret = new ArrayList<>(num);
        for (int i = 0; i < num; ++i) {
            Pointer p = areaArray.share(i * structSize);
            LibSphereCalcMapper.SurfaceAreaResult r =
                new LibSphereCalcMapper.SurfaceAreaResult(p);
            ret.add(new SurfaceAreas(r.area_1_4, r.area_7_0, r.area_14_0));
        }
        return ret;
    }

    private static ArrayList<Double> doubleArrayFromPointer(
            Pointer areaArray, int num) {
        ArrayList<Double> ret = new ArrayList<>(num);
        Pointer p = areaArray.share(0);
        for (int i = 0; i < num; ++i) {
            ret.add(p.getDouble(i * Double.BYTES));
        }
        return ret;
    }
}
