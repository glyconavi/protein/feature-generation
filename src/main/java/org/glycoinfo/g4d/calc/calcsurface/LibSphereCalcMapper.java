package org.glycoinfo.g4d.calc.calcsurface;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Structure;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

public interface LibSphereCalcMapper extends Library {
    public static class Vec3d extends Structure {
        public static class ByReference extends Vec3d implements Structure.ByReference {}

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList("x", "y", "z");
        }

        public double x;
        public double y;
        public double z;
    }

    public static class Sphere extends Structure {
        public static class ByReference extends Sphere implements Structure.ByReference {}

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList("radius", "center");
        }

        public double radius;
        public Vec3d center;
    }

    public static class IntIndexPair extends Structure {
        public static class ByReference extends IntIndexPair implements Structure.ByReference {}

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList("start", "end");
        }

        public int start;
        public int end;
    }

    public static class SurfaceAreaResult extends Structure {
        public SurfaceAreaResult() {}

        public SurfaceAreaResult(Pointer pointer) {
            super(pointer);
            read();
        }

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList("area_1_4", "area_7_0", "area_14_0");
        }

        public double area_1_4;
        public double area_7_0;
        public double area_14_0;
    }

    public int calc_surface_features(
            PointerByReference area_result,
            PointerByReference distance_result,
            Sphere.ByReference spheres, int num_spheres,
            IntIndexPair.ByReference area_ranges, int num_ranges,
            IntIndexPair.ByReference distance_idxs, int num_distances);

    public void free_surface_result(
            PointerByReference area_result,
            PointerByReference distance_result);

    public final LibSphereCalcMapper INSTANCE =
        (LibSphereCalcMapper)Native.load(
            "/libspherecalc.so", LibSphereCalcMapper.class);
}
