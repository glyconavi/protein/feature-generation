package org.glycoinfo.g4d.io;

import static java.lang.Thread.sleep;
import static org.glycoinfo.g4d.cli.App.NETWORK_ERROR_RETRY;

import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

import org.biojava.nbio.structure.StructureTools;
import org.glycoinfo.g4d.config.Constants;
import org.glycoinfo.g4d.feature.MLFeatures;
import org.glycoinfo.g4d.util.Util;
import org.rcsb.cif.CifIO;
import org.rcsb.cif.model.CifFile;
import org.rcsb.cif.model.FloatColumn;
import org.rcsb.cif.model.IntColumn;
import org.rcsb.cif.model.StrColumn;
import org.rcsb.cif.schema.StandardSchemata;
import org.rcsb.cif.schema.mm.AtomSite;
import org.rcsb.cif.schema.mm.Entry;
import org.rcsb.cif.schema.mm.MmCifBlock;
import org.rcsb.cif.schema.mm.MmCifFile;
import org.rcsb.cif.schema.mm.StructConn;

public class MmCIFParser {

    private String structureId = "";
    private boolean useStructureId = false;
    private boolean isPdbId = false;
    private String cifFile = "";

    // Constructor
    public MmCIFParser(String structureId, boolean isPdbId) {
        this.structureId = structureId;
        this.useStructureId = true;
        this.isPdbId = isPdbId;
    }
    public MmCIFParser(String cifFile) {
        this.cifFile = cifFile;
    }

    public MLFeatures parse() throws MalformedURLException, IOException {
        // use cif-tools library
        CifFile cifFile = null;
        // http code.
        int status = 200;
        // if the PDB or AlphaFold ID is specified,
        if (this.useStructureId == true) {
            for (int i = 1; ; i++) {
                try {
                    if (this.isPdbId) { // i.e. PDB ID
                    	   URL pdbCodeUrl = new URL(Constants.PDB_BCIF_URL + this.structureId + ".bcif");
                    	   status = Util.existsUrl(pdbCodeUrl);
                        if (status != 200) {
                            if (status == 404) {
                    	           System.out.println(this.structureId + " PDB file open failed.");
                            } else if (status == 503) {
                                System.out.println(Constants.PDB_BCIF_URL + " site is not available now. Please try again later.");
                            } else {}
                    	       MLFeatures mlFeatures = new MLFeatures();
                    	       mlFeatures.setStatus(status);
                    	       mlFeatures.setEntryId(this.structureId);
                    	       mlFeatures.setAaAsymIdList(new ArrayList<String>()); // empty data to deal with PDB code that doesn't exist.
                    	       return mlFeatures;
                        } else {
                            cifFile = CifIO.readFromURL(new URL(Constants.PDB_BCIF_URL + this.structureId + ".bcif"));
                        }
                    } else { // i.e. AlphaFold ID
                        URL alphaFoldCodeUrl = new URL(Constants.AF_CIF_URL + this.structureId + ".cif");
                        status = Util.existsUrl(alphaFoldCodeUrl);
                        if (status != 200) {
                            if (status == 404) {
                                System.out.println(this.structureId + " AlphaFold file open failed.");
                            } else if (status == 503) {
                                System.out.println(Constants.AF_CIF_URL + " site is not available now. Please try again later.");
                            } else {}
                 	           MLFeatures mlFeatures = new MLFeatures();
                 	           mlFeatures.setStatus(status);
                 	           mlFeatures.setEntryId(this.structureId);
                 	           mlFeatures.setAaAsymIdList(new ArrayList<String>()); // empty data to deal with AlphaFold ID that doesn't exist.
                 	           return mlFeatures;
                        } else {
                            cifFile = CifIO.readFromURL(new URL(Constants.AF_CIF_URL + this.structureId + ".cif"));
                        }
                    }
                } catch (ConnectException ce) {
                    if (i >= NETWORK_ERROR_RETRY) {
                        throw ce;
                    }
                    int wait = (int) Math.pow(2, i);
                    try {
                        sleep(wait * 1000);
                    } catch (InterruptedException e2) {
                        break;
                    }
                }
                break;
            }
        } else { // i.e. when using a local mmCIF file,
            String localCifFile = this.cifFile;
            // Replace wrong path separator for win
            if (localCifFile.contains("\\")) {
                localCifFile = localCifFile.replace("\\", "/");
            }
            cifFile = CifIO.readFromPath(Paths.get(localCifFile));
        }
        // get mmCIF form.
        MmCifFile mmCIFFile = cifFile.as(StandardSchemata.MMCIF);

        // get first block of mmCIF
        // no multiple block found in PDB mmCIF data.
        MmCifBlock block = mmCIFFile.getFirstBlock();

        // get category with name 'entry'
        Entry entry = block.getEntry();

        // get category with name '_atom_site' from first block - access is type safe, all categories
        // are derived from the CIF schema
        AtomSite atomSite = block.getAtomSite();

        // get category with name '_struct_conn' from first block
        StructConn structConn = block.getStructConn();

        // call the MLFeatures class to store machine learning features.
        MLFeatures mlFeatures = new MLFeatures();

        // status. i.e. HTTP code 200
        mlFeatures.setStatus(status);

        // set entry ID.
        parseEntryAndSetEntryId(entry, mlFeatures);

        // _struct_conn
        // get machine learning features from _struct_conn and set them to the variables.
        parseStructConnAndSetGlycoSsInfo(structConn, mlFeatures);
 
        // _atom_site
        // get machine learning features from _atom_site and set them to the variables.
        parseAtomSiteAndSetAminoAcidInfo(atomSite, mlFeatures);

        return mlFeatures;
    }

    private void parseEntryAndSetEntryId(Entry entry, MLFeatures mlFeatures) {
        // get values from _entry
        mlFeatures.setEntry(entry);
        String id = entry.getId().get(0);
        // set entry id.
        mlFeatures.setEntryId(id);
    }

    private void parseAtomSiteAndSetAminoAcidInfo(AtomSite atomSite, MLFeatures mlFeatures) {
        // get values from _atom_site.
        mlFeatures.setAtomSite(atomSite);
        StrColumn groups = atomSite.getGroupPDB(); // added. 6.Feb.2024
        StrColumn aaCompIds = atomSite.getLabelCompId();
        StrColumn aaAsymIds = atomSite.getLabelAsymId();
        IntColumn aaSeqIds = atomSite.getLabelSeqId();
        StrColumn aaAtomIds = atomSite.getLabelAtomId();
        IntColumn aaIds = atomSite.getId();
        FloatColumn cartnXs = atomSite.getCartnX();
        FloatColumn cartnYs = atomSite.getCartnY();
        FloatColumn cartnZs = atomSite.getCartnZ();
        // initialize amino acid information variables (only Cα).
        ArrayList<String> groupList = new ArrayList<String>(); // to deal with irregular HETATM codes. added. 6.Feb.2024
        ArrayList<String> aaCompIdList = new ArrayList<String>(); // three letters.
        ArrayList<String> aaList = new ArrayList<String>(); // one letter.
        ArrayList<String> aaAsymIdList = new ArrayList<String>();
        ArrayList<Integer> aaSeqIdList = new ArrayList<Integer>();
        ArrayList<String> aaAtomIdList = new ArrayList<String>();
        ArrayList<Double> cartnXList = new ArrayList<Double>();
        ArrayList<Double> cartnYList = new ArrayList<Double>();
        ArrayList<Double> cartnZList = new ArrayList<Double>();
        // initialize amino acid information variables (full Atoms)
        ArrayList<String> fullGroupList = new ArrayList<String>(); // to deal with irregular HETATM codes. added. 6.Feb.2024
        ArrayList<String> fullAaCompIdList = new ArrayList<String>(); // three letters.
        ArrayList<String> fullAaAsymIdList = new ArrayList<String>();
        ArrayList<Integer> fullAaSeqIdList = new ArrayList<Integer>();
        ArrayList<String> fullAaAtomIdList = new ArrayList<String>();
        ArrayList<Integer> fullAaIdList = new ArrayList<Integer>();
        ArrayList<Double> fullCartnXList = new ArrayList<Double>();
        ArrayList<Double> fullCartnYList = new ArrayList<Double>();
        ArrayList<Double> fullCartnZList = new ArrayList<Double>();

        // for 7zzv and so on. (Some data formats have the same chain but different coordinates.)
        HashMap<String, Integer> chainAtomResHashMap = new HashMap<String, Integer>();
        int columnNum = atomSite.getRowCount(); // number of _atom_site lines.
        for (int i = 0; i < columnNum; i++) {
            // to check for duplicate lines. (e.g. 7zzv) start line.
            String dupLineCheckTag = aaAsymIds.get(i) + ":" + aaAtomIds.get(i) + ":" + aaSeqIds.get(i);
            if (chainAtomResHashMap.containsKey(dupLineCheckTag)) {
                continue;
            } else {
                chainAtomResHashMap.put(dupLineCheckTag, 1);
            }
            // end line.
            if (aaAtomIds.get(i).equals(Constants.CALPHA_TAG)) { // get and set Cα lines information.
                // HETATM CA pattern. (only seven PDB codes)
                if (groups.get(i).equals("HETATM")) { // full atom lists are also skipped.
                    continue;
                }
                groupList.add(groups.get(i)); // added. 6.Feb.2024
        	    aaCompIdList.add(aaCompIds.get(i));
                aaList.add(StructureTools.get1LetterCode(aaCompIds.get(i)).toString());
                aaAsymIdList.add(aaAsymIds.get(i));
                aaSeqIdList.add(aaSeqIds.get(i));
                aaAtomIdList.add(aaAtomIds.get(i));
                cartnXList.add(cartnXs.get(i));
                cartnYList.add(cartnYs.get(i));
                cartnZList.add(cartnZs.get(i));
            }
            // full atom ID data
            fullGroupList.add(groups.get(i)); // added. 6.Feb.2024
            fullAaCompIdList.add(aaCompIds.get(i));
            fullAaAsymIdList.add(aaAsymIds.get(i));
            fullAaSeqIdList.add(aaSeqIds.get(i));
            fullAaAtomIdList.add(aaAtomIds.get(i));
            fullAaIdList.add(aaIds.get(i));
            fullCartnXList.add(cartnXs.get(i));
            fullCartnYList.add(cartnYs.get(i));
            fullCartnZList.add(cartnZs.get(i));
        }
        //// Cα data
        // set each group PDB to ArrayList.
        mlFeatures.setGroupList(groupList);
        // set each amino acid (one letter) to ArrayList.
        mlFeatures.setAaSequenceList(aaList);
        // set each amino acid (three letters) to ArrayList.
        mlFeatures.setAaCompIdList(aaCompIdList);
        // set AsymIDs to ArrayList.
        mlFeatures.setAaAsymIdList(aaAsymIdList);
        // set SeqIds to ArrayList.
        mlFeatures.setAaSeqIdList(aaSeqIdList);
        // set AtomIds to ArrayList.
        mlFeatures.setAaAtomIdList(aaAtomIdList);
        // set CartnX to ArrayList.
        mlFeatures.setCartnXList(cartnXList);
        // set CartnY to ArrayList.
        mlFeatures.setCartnYList(cartnYList);
        // set CartnZ to ArrayList.
        mlFeatures.setCartnZList(cartnZList);
        //// full atom ID data.
        // set group PDB to ArraList.
        mlFeatures.setFullGroupList(fullGroupList);
        // set each amino acid (three letters) to ArrayList.
        mlFeatures.setFullAaCompIdList(fullAaCompIdList);
        // set AsymIDs to ArrayList.
        mlFeatures.setFullAaAsymIdList(fullAaAsymIdList);
        // set SeqIds to ArrayList.
        mlFeatures.setFullAaSeqIdList(fullAaSeqIdList);
        // set AtomIds to ArrayList.
        mlFeatures.setFullAaAtomIdList(fullAaAtomIdList);
        // set IDs to ArrayList.
        mlFeatures.setFullAaIdList(fullAaIdList);
        // set CartnX to ArrayList.
        mlFeatures.setFullCartnXList(fullCartnXList);
        // set CartnY to ArrayList.
        mlFeatures.setFullCartnYList(fullCartnYList);
        // set CartnZ to ArrayList.
        mlFeatures.setFullCartnZList(fullCartnZList);
    }

    private void parseStructConnAndSetGlycoSsInfo(StructConn structConn, MLFeatures mlFeatures) {
        // get values from _struct_conn.
        mlFeatures.setStructConn(structConn);
        StrColumn ids = structConn.getId();
        StrColumn connTypeIds = structConn.getConnTypeId();
        StrColumn ptnr1AsymIds = structConn.getPtnr1LabelAsymId();
        StrColumn ptnr1CompIds = structConn.getPtnr1LabelCompId();
        IntColumn ptnr1SeqIds = structConn.getPtnr1LabelSeqId();
        StrColumn ptnr2AsymIds = structConn.getPtnr2LabelAsymId();
        StrColumn ptnr2CompIds = structConn.getPtnr2LabelCompId();
        IntColumn ptnr2SeqIds = structConn.getPtnr2LabelSeqId();
        StrColumn pdbxRoles = structConn.getPdbxRole();
        // initialize ArrayLists for N/O-Glycosylation sites.
        ArrayList<String> nGlycoIdList = new ArrayList<String>();
        ArrayList<String> nGlycoAsymList = new ArrayList<String>();
        ArrayList<String> nGlycoCompIdList = new ArrayList<String>();
        ArrayList<Integer> nGlycoSeqIdList = new ArrayList<Integer>();
        ArrayList<String> oGlycoIdList = new ArrayList<String>();
        ArrayList<String> oGlycoAsymList = new ArrayList<String>();
        ArrayList<String> oGlycoCompIdList = new ArrayList<String>();
        ArrayList<Integer> oGlycoSeqIdList = new ArrayList<Integer>();
        // initialize ArrayLists for S-S bonds.
        ArrayList<String> ssIdList = new ArrayList<String>();
        ArrayList<Integer> ptnr1ssSeqIdList = new ArrayList<Integer>();
        ArrayList<Integer> ptnr2ssSeqIdList = new ArrayList<Integer>();
        ArrayList<String> ptnr1ssAsymList = new ArrayList<String>();
        ArrayList<String> ptnr2ssAsymList = new ArrayList<String>();
        ArrayList<String> ptnr1ssCompIdList = new ArrayList<String>();
        ArrayList<String> ptnr2ssCompIdList = new ArrayList<String>();

    	int columnNum = structConn.getRowCount(); // number of _struct_conn lines.
    	for (int i = 0; i < columnNum; i++) {
            if (connTypeIds.get(i).equals("disulf")) {
                ssIdList.add(ids.get(i));
                ptnr1ssSeqIdList.add(ptnr1SeqIds.get(i));
                ptnr2ssSeqIdList.add(ptnr2SeqIds.get(i));
                ptnr1ssAsymList.add(ptnr1AsymIds.get(i));
                ptnr2ssAsymList.add(ptnr2AsymIds.get(i));
                ptnr1ssCompIdList.add(ptnr1CompIds.get(i));
                ptnr2ssCompIdList.add(ptnr2CompIds.get(i));
                continue;
            }
            if (connTypeIds.get(i).equals("covale")) {
                if (!pdbxRoles.isDefined()) { // added. 30.Nov.2023
                    continue;
                }
                if (pdbxRoles.get(i).equals("N-Glycosylation")) { // 
                    nGlycoIdList.add(ids.get(i));
                    nGlycoAsymList.add(ptnr1AsymIds.get(i));
                    nGlycoCompIdList.add(ptnr1CompIds.get(i));
                    nGlycoSeqIdList.add(ptnr1SeqIds.get(i));
                    continue;
                }
                if (pdbxRoles.get(i).equals("O-Glycosylation")) {
                    oGlycoIdList.add(ids.get(i));
                    oGlycoAsymList.add(ptnr1AsymIds.get(i));
                    oGlycoCompIdList.add(ptnr1CompIds.get(i));
                    oGlycoSeqIdList.add(ptnr1SeqIds.get(i));
                    continue;
                }
            }
        }
        // set N-Glycosylation type IDs, AsymIDs, CompIds and SeqIds to ArrayLists.
        mlFeatures.setnGlycoIdList(nGlycoIdList);
        mlFeatures.setnGlycoAsymIdList(nGlycoAsymList);
        mlFeatures.setnGlycoCompIdList(nGlycoCompIdList);
        mlFeatures.setnGlycoSeqIdList(nGlycoSeqIdList);
        // set O-Glycosylation type IDs, AsymIDs, CompIds and SeqIds to ArrayLists.
        mlFeatures.setoGlycoIdList(oGlycoIdList);
        mlFeatures.setoGlycoAsymIdList(oGlycoAsymList);
        mlFeatures.setoGlycoCompIdList(oGlycoCompIdList);
        mlFeatures.setoGlycoSeqIdList(oGlycoSeqIdList);
        // set amino acid length of S-S bond residues to ArrayLists.
        mlFeatures.setSsIDList(ssIdList);
        mlFeatures.setPtnr1ssBondsSeqIdList(ptnr1ssSeqIdList);
        mlFeatures.setPtnr2ssBondsSeqIdList(ptnr2ssSeqIdList);
        mlFeatures.setPtnr1ssBondsAsymIdList(ptnr1ssAsymList);
        mlFeatures.setPtnr2ssBondsAsymIdList(ptnr2ssAsymList);
        mlFeatures.setPtnr1ssBondsCompIdList(ptnr1ssCompIdList);
        mlFeatures.setPtnr2ssBondsCompIdList(ptnr2ssCompIdList);
    }

}
