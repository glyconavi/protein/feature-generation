package org.glycoinfo.g4d.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class FileInput {

    // Method to read the contents of a file line by line and return them as a list
    public static ArrayList<String> getListContents(String listPath) throws Exception {
    	ArrayList<String> idPathList = new ArrayList<String>();
        try {
            FileInputStream fis = new FileInputStream(new File(listPath));
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            String line = "";
            while ((line = br.readLine()) != null) {
                line = line.replace("\r\n", "");
                idPathList.add(line);
            }
            br.close();
            isr.close();
            fis.close();
        } catch (IOException ioe) {
           throw new Exception("File open failed: " + listPath);
        }
        return idPathList;
    }

}
