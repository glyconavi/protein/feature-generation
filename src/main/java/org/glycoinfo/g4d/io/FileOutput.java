package org.glycoinfo.g4d.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.jena.rdf.model.Model;
import org.glycoinfo.g4d.config.Constants;
import org.glycoinfo.g4d.feature.MLFeatures;
import org.glycoinfo.g4d.feature.MLFeaturesUtil;
import org.glycoinfo.g4d.rdf.TurtleBuilder;
import org.glycoinfo.g4d.rdf.vocabulary.FEATURES;
import org.glycoinfo.g4d.text.OutputTextBuilder;

public class FileOutput {

    // Class for handling file output operations
    private String outputFilePath;
    private FileOutputStream fos;
    private OutputStreamWriter osw;
    private BufferedWriter bw;

    private MLFeatures mlFeatures;
    private String outputFileType;
    private String sepType;

    private boolean dataHit = false;

    // for turtle.
    private TurtleBuilder turtleBuilder;
    private Model model;

    // Constructor that takes the output file path
    public FileOutput(String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }

    // Setter method for MLFeatures data
    public void setMLFeaturesData(MLFeatures mlFeatures) {
        this.mlFeatures = mlFeatures;
    }

    // Setter method for output file type
    public void setOutputType(String outputFileType) {
        this.outputFileType = outputFileType;
        if (this.outputFileType == "tsv") {
            this.sepType = Constants.TSV_SEP;
        } else if (this.outputFileType == "csv") {
            this.sepType = Constants.CSV_SEP;
        } else {}
    }

    // Method to create a BufferedWriter for writing to the output file
    public void createWriteStream() throws Exception {
        try {
            this.fos = new FileOutputStream(new File(this.outputFilePath));
            this.osw = new OutputStreamWriter(fos);
            this.bw = new BufferedWriter(osw);
        } catch (IOException ioe) {
            throw new Exception("Output file open failed: " + this.outputFilePath);
        }
    }

    // Method to close the BufferedWriter and associated streams
    public void closeWriteStream() throws Exception {
        try {
        	this.bw.close();
        	this.osw.close();
        	this.fos.close();
        } catch (IOException ioe) {
        	throw new Exception("Output file close failed: " + this.outputFilePath);
        }
    }

    // Getter method for the BufferedWriter
    public BufferedWriter getBufferedWriter() {
        return this.bw;
    }

    // Method to write text data to the output file
    public void writeText() {
        OutputTextBuilder builder = new OutputTextBuilder(mlFeatures, outputFileType);
        int columnNum = 0;
        try {
        	if (this.mlFeatures.getStatus() == 404) { // if the PDB/AlphaFold ID doesn't exist,
//        	    this.bw.write(this.mlFeatures.getEntryId() + " data not found." + "\n");
                System.out.println(this.mlFeatures.getEntryId() + " data not found." + "\n");
        	    return;
        	}
        	if (this.mlFeatures.getStatus() == 503) { // close the file write handle and stop execution so that the HTTP code is 503.
//        	    this.bw.write(this.mlFeatures.getEntryId() + ": Please try again later.\n");
        	    System.out.println(this.mlFeatures.getEntryId() + ": Please try again later.\n");
        	    try {
        	        closeWriteStream();
        	    } catch (Exception e) {
        	        e.printStackTrace();
        	    }
        	    System.exit(0);
        	}
        	ArrayList<String> aaAsymIdList = this.mlFeatures.getAaAsymIdList();
        	// delete duplicate chain IDs.
        	List<String> aaNrAsymIdList = aaAsymIdList.stream().distinct().collect(Collectors.toList());
        	this.dataHit = false; // initialization.
        	for (int i = 0; i < aaNrAsymIdList.size(); i++) {
        		this.dataHit = true;
                this.mlFeatures.setCurrentAsymId(aaNrAsymIdList.get(i));
                // delete N/O-Glycosylation site information that has amino acid symbols in compId of HETATM lines. added. 6.Feb.2024
                MLFeaturesUtil.deleteHetAtmFromCovaleStructConn(mlFeatures);
                // write entry ID + AsymID
                columnNum = 1;
                builder.writeEntryAsymIdColumn(this.bw);
                this.bw.write(this.sepType);
                //// write machine learning features in form of text format.
                // N-Glycosylation sites.
                columnNum = 2;
                builder.writeNGlycoSeqNumColumn(this.bw);
                this.bw.write(this.sepType);
                // O-Glycosylation sites.
                columnNum = 3;
                builder.writeOGlycoSeqNumColumn(this.bw);
                this.bw.write(this.sepType);
                // whether or not N-glycosylation consensus patterns are present
                columnNum = 5;
                builder.writeNGlycoConsensusSeqColumn(this.bw);
                this.bw.write(this.sepType);
                // ASN positions in N-Glycosylation consensus patterns.
                columnNum = 6;
                builder.writeAsnPositions(this.bw);
                this.bw.write(this.sepType);
                // SER positions in all amino acids.
                columnNum = 7;
                builder.writeSerPositions(this.bw);
                this.bw.write(this.sepType);
                // THR positions in all amino acids.
                columnNum = 8;
                builder.writeThrPositions(this.bw);
                this.bw.write(this.sepType);
                // S-S bond distances in the amino acid sequence.
                columnNum = 9;
                builder.writeSSBondAaDistances(this.bw);
                this.bw.write(this.sepType);
                // S-S bond 3D distances.
                columnNum = 10;
                builder.writeSSBond3DDistances(this.bw);
                this.bw.write(this.sepType);
                // torsion angles.
                columnNum = 11;
                builder.writeTorsionAngles(this.bw);
                this.bw.write(this.sepType);
                // amino acid sequence lengths between glycosylation sites.
                columnNum = 12;
                builder.writeGlyAaDistances(this.bw);
                this.bw.write(this.sepType);
                // tertiary distances and exposed surface areas between glycosylation sites.
                columnNum = 13;
                builder.writeGlyTerDistancesAndAreas(this.bw);
                this.bw.write("\n");
                this.bw.flush();
        	}
            if (this.dataHit == false) {
//                this.bw.write(this.mlFeatures.getEntryId() + " is data for which features cannnot be calculated.\n");
                System.out.println(this.mlFeatures.getEntryId() + " is data for which features cannnot be calculated.\n");
            }
        } catch (Exception e) {
            System.out.println("ERROR ID: " + this.mlFeatures.getEntryId());
            // Fill in the CSV columns with blanks for data that have been terminated due to an error.
            try {
                for (int i = columnNum; i < 16; i++) {
                    this.bw.write(Constants.EMPTY_COLUMN);
                    this.bw.write(this.sepType);
                }
                this.bw.write(Constants.EMPTY_COLUMN + "\n");
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    // new model class of jena (one per exec)
    public void setRdfModel() {
        this.turtleBuilder = new TurtleBuilder();
        this.model = turtleBuilder.getModel();
        turtleBuilder.setNamespace(model);
    }
    public Model getRdfModel() {
        return this.model;
    }
    public void closeRdfModel() {
        this.model.close();
    }

    public void writeTurtle() {
    	turtleBuilder.entry(this.mlFeatures);
        try {
        	if (this.mlFeatures.getStatus() == 404) { // if PDB/AlphaFold ID doesn't exist,
        	    System.out.println(this.mlFeatures.getEntryId() + " data not found." + "\n");
        	    return;
        	}
        	if (this.mlFeatures.getStatus() == 503) { // close the file write handle and quit execution so that HTTP code is 503.
        	    System.out.println(this.mlFeatures.getEntryId() + ": Please try again later.\n");
        	    model.close();
        	    try {
        	        closeWriteStream();
        	    } catch (Exception e) {
        	        e.printStackTrace();
        	    }
        	    System.exit(0);
        	}
            turtleBuilder.addId(model);
            turtleBuilder.initializeRootResource(model);
        	ArrayList<String> aaAsymIdList = this.mlFeatures.getAaAsymIdList();
        	// delete duplicated chain IDs.
        	List<String> aaNrAsymIdList = aaAsymIdList.stream().distinct().collect(Collectors.toList());
        	this.dataHit = false; // initialization
        	for (int i = 0; i < aaNrAsymIdList.size(); i++) {
        		this.dataHit = true;
                this.mlFeatures.setCurrentAsymId(aaNrAsymIdList.get(i)); // Set chain ID here.
                // Obtain the information corresponding to the current chain, with the amino acid residue number
                // as key and the amino acid as value, and set it to the TurtleBuilder instance.
                turtleBuilder.setSeqIdCompIdMap(MLFeaturesUtil.getCurrentAsymIdAaSeqIdMap(this.mlFeatures));
                // URI
                String uri = FEATURES.getURI() + this.mlFeatures.getEntryId() + "/" + aaNrAsymIdList.get(i);
                // Creation of TTL statements on N-Glycosylation sites.
                turtleBuilder.addNGlycosylationSite(model, uri);
                // Creation of TTL statements on O-Glycosylation sites.
                turtleBuilder.addOGlycosylationSite(model, uri);
                // Creating TTL statements on N-coupled consensus patterns.
                turtleBuilder.addNGlycosylationConsensusPattern(model, uri);
                // Creation of TTL statements on ASA
                turtleBuilder.addAsa(model, uri);
                // Creation of TTL statements on amino acid and surface distances between glycan binding sites.
                turtleBuilder.addDistance(model, uri);
                // If there are consensus motifs, create TTL statements on ASN residue information.
                turtleBuilder.addAsn(model, uri);
                // Creating TTL statements on SER residue information
                turtleBuilder.addSer(model, uri);
                // Creating TTL statements on THR residue information
                turtleBuilder.addThr(model, uri);
                // Creation TTL statements on S-S binding sites.
                turtleBuilder.addDisulfideBondSite(model, uri);
                // Creating TTL statements on torsion angles.
                turtleBuilder.addTorsionAngle(model, uri);
        	}
            if (this.dataHit == false) {
                System.out.println(this.mlFeatures.getEntryId() + " is data for which features cannnot be calculated.\n");
            }
        } catch (Exception e) {
            System.out.println("ERROR ID: " + this.mlFeatures.getEntryId());
            e.printStackTrace();
        }
    }
}
