package org.glycoinfo.g4d.cli;

import java.io.File;
import java.util.regex.Pattern;

import org.glycoinfo.g4d.config.Constants;

public enum OutputFormat {
    // Enum constants representing different output formats with their names and extensions
    TSV("tsv", Constants.TSV_EXT),
    CSV("csv", Constants.CSV_EXT),
    RDF_TURTLE("turtle", Constants.TTL_EXT);

    // Enum fields for format name and extension
    private String name;
    private String ext;

    // Enum constructor to initialize enum constants with name and extension
    private OutputFormat(String name, String ext) {
        this.name = name;
        this.ext = ext;
    }

    // Method to lookup and return the OutputFormat based on the specified name
    public static OutputFormat lookup(String name) {
        for (OutputFormat fmt : OutputFormat.values()) {
            if (fmt.name.contains(name)) {
                return fmt;
            }
        }
        throw new IllegalArgumentException("Unknown format.");
    }

    // Method to set the output file name based on the input and output format 
    public static String setOutputFileName(String input, String outFormat) {
        String outFileName = input;
        // If the input is a valid PDB code, convert it to lowercase
        if (Pattern.matches(Constants.REGEX_PDB_CODE, input)) {
            outFileName = String.format("%s", input.toLowerCase());
        }
        // If the input is a valid AlphaFold code, keep it unchanged
        if (Pattern.matches(Constants.REGEX_AF_CODE, input)) {
            outFileName = input;
        }
        // If the input is an existing file, use the file name
        if ((new File(input)).exists()) {
            outFileName = new File(input).getName();
        }
        // Remove file extension from the file name
        outFileName = outFileName.split("\\.")[0];
        // Append the appropriate extension based on the specified output format
        for (OutputFormat fmt: OutputFormat.values()) {
            if (fmt.name.equals(outFormat)) {
                outFileName += fmt.ext;
                return outFileName;
            }
        }
        throw new IllegalAccessError("output file name error.");
    }

    // Getter method for the format name
    public String getName() {
        return this.name;
    }

}
