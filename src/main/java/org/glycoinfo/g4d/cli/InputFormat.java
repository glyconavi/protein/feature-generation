package org.glycoinfo.g4d.cli;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.regex.Pattern;

import org.glycoinfo.g4d.config.Constants;

public enum InputFormat {
    // Enum constants representing different input formats
	PDB("pdb", Constants.REGEX_PDB_CODE, Constants.PDB_BCIF_URL),
	ALPHAFOLD("alphafold", Constants.REGEX_AF_CODE, Constants.AF_CIF_URL),
    MMCIF("mmcif", Constants.MMCIF_EXT, null),
	MMCIFGZIP("mmcif", Constants.MMCIF_EXT + Constants.GZIP_EXT, null);

    // Enum fields for name, regex, and path
    private String name;
    private String regex;
    private String path;

    // Enum constructor to initialize enum constants with name, regex, and path
    private InputFormat(String name, String regex, String path) {
        this.name = name;
        this.regex = regex;
        this.path = path;
    }

    // Getter methods for enum fields
    public String getName() {
        return this.name;
    }
    public String getRegex() {
        return this.regex;
    }
    public String getPath() {
        return this.path;
    }

    // Method to lookup and return the InputFormat based on the input string
    public static InputFormat lookup(String input) throws FileNotFoundException {
        // Check if the input string is a PDB or AlphaFold ID or a file path
        if (!input.endsWith(Constants.MMCIF_EXT) && !input.endsWith(Constants.MMCIF_EXT + Constants.GZIP_EXT)) {
            // Verify if the input is a valid PDB or AlphaFold ID
            if (!Pattern.matches(Constants.REGEX_PDB_CODE, input)) {
                if (!Pattern.matches(Constants.REGEX_AF_CODE, input)) {
        	        throw new IllegalArgumentException("Invalid PDB (or AlphaFold) ID.");
                }
            }
        } else {
            // If the input is a file path, check if the file exists
            if (!new File(input).exists()) {
                throw new FileNotFoundException("File not found: " + input + ".");
            }
        }
        // Iterate through enum constants and match the input with the regex
        for (InputFormat fmt : InputFormat.values()) {
            if (Pattern.matches(fmt.regex, input)) {
                return fmt;
            }
            if (input.endsWith(fmt.regex)) {
                return fmt;
            }
        }
        // If no match is found, throw an exception indicating an unknown format
        throw new IllegalArgumentException("Unknown format");
    }
}
