package org.glycoinfo.g4d.cli;

import java.io.File;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.glycoinfo.g4d.config.Constants;
import org.glycoinfo.g4d.io.FileInput;
import org.glycoinfo.g4d.io.FileOutput;
import org.glycoinfo.g4d.util.Util;

public class App {
    // Application information
    static final String APP_NAME = "g4d";
    static final String APP_VERSION = "0.0.1";

    // Status code for different error scenarios
    private static final int STATUS_PARSE_ERROR = 1;
    private static final int STATUS_ARGUMENT_ERROR = 2;
    private static final int STATUS_RUNTIME_ERROR = 3;

    // Number of retry attembps for network errors
    public static final int NETWORK_ERROR_RETRY = 5;

    // Command line option constants
    private static final String OPTION_INPUT_ID_LONG = "id";
    private static final String OPTION_INPUT_ID_SHORT = "i";

    private static final String OPTION_INPUT_LIST_SHORT = "l";
    private static final String OPTION_INPUT_LIST_LONG = "list";

    private static final String OPTION_INPUT_FILE_PATH_LONG = "path";
    private static final String OPTION_INPUT_FILE_PATH_SHORT = "p";

    private static final String OPTION_OUTPUT_FORMAT_LONG = "out";
    private static final String OPTION_OUTPUT_FORMAT_SHORT = "o";

    private static final String OPTION_FORCE_LONG = "force";
    private static final String OPTION_FORCE_SHORT = "f";

    private static final String OPTION_OUTDIR_PATH_LONG = "dir";
    private static final String OPTION_OUTDIR_PATH_SHORT = "d";

    // Supported output formats
    private static final String[] ARGUMENTS_OUTPUT_FORMAT = {"csv", "tsv", "turtle"};

    // Flags for PDB and local data
    private boolean pdbData = false; // if PDB_DATA == false, i.e. AlphaFold mmCIF data
    private boolean localData = false; // if local PDB/AlphaFold file is specified ->  true

    // Main method
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        new App().run(args);
        long time = System.currentTimeMillis() - start;
        String timeFormated = Util.formatTime(time);
        System.out.println("Total execution time: " + timeFormated);
    }

    // Application logic
    void run(String[] args) {
        // Initialize command line parser and custom parser
        CommandLineParser cmdParser = new DefaultParser();
        Parser parser = getParser();
        try {
            // Parse command line arguments
            CommandLine cmd = cmdParser.parse(options(), args);
            // Print usage if help option is present.
            if (cmd.hasOption("help")) {
                printUsage();
                return;
            }
            // Check if an output format is specified
            String outForm = "";
            if (!cmd.hasOption(OPTION_OUTPUT_FORMAT_LONG)) {
                throw new IllegalArgumentException("Not specified any output form.");
            } else {
                outForm = cmd.getOptionValue(OPTION_OUTPUT_FORMAT_LONG);
            }
            // Check the output directory path
            String outDir = "."; // i.e. current directory
            if (cmd.hasOption(OPTION_OUTDIR_PATH_LONG)) {;
                outDir = cmd.getOptionValue(OPTION_OUTDIR_PATH_LONG);
            }
            // Check if force overwrite is specified
            boolean force = false;
            if (cmd.hasOption(OPTION_FORCE_LONG)) {
                force = true;
            }
            // Check input options (PDB/AlphaFold ID, local mmCIF file, or list)
            ArrayList<String> idPathList = new ArrayList<String>(); // empty except for specifying --l/--list
            if (cmd.hasOption(OPTION_INPUT_LIST_LONG)) {
                // Handle case when a list is specified
                String listFilePath = cmd.getOptionValue(OPTION_INPUT_LIST_LONG);
                idPathList = FileInput.getListContents(listFilePath);
                parser.force(force)
                    .outputFormat(OutputFormat.lookup(outForm))
                    .outputFilePath(
            	        OutputFormat.setOutputFileName(
            	            (new File(listFilePath).getName().split("\\.")[0]), outForm),
            	                outDir);
            } else if (cmd.hasOption(OPTION_INPUT_ID_LONG)) {
                // Handle case when a single PDB/AlphaFold ID is specified
                idPathList.add(cmd.getOptionValue(OPTION_INPUT_ID_LONG));
                parser.force(force)
                    .outputFormat(OutputFormat.lookup(outForm))
                    .outputFilePath(OutputFormat.setOutputFileName(idPathList.get(0), outForm), outDir);
            } else if (cmd.hasOption(OPTION_INPUT_FILE_PATH_LONG)) {
                // Handle case when a local mmCIF file path is specified
                idPathList.add(cmd.getOptionValue(OPTION_INPUT_FILE_PATH_LONG));
                parser.force(force)
                    .outputFormat(OutputFormat.lookup(outForm))
                    .outputFilePath(OutputFormat.setOutputFileName(idPathList.get(0), outForm), outDir);
            } else { // Input error. No ID or local mmCIF path or list specified.
                printUsage();
                return;
            }
            // Prepare output stream.
            FileOutput fileOutput = new FileOutput(parser.getOutputFilePath());
            fileOutput.createWriteStream();

            // Set RDF model if output format is "turtle"
            if (outForm.equals("turtle")) {
                fileOutput.setRdfModel();
                parser.setFileOutput(fileOutput);
            }

            // Process each PDB/AlphaFold ID or local mmCIF path in the list
            for (String idPath : idPathList) {
                if (Pattern.matches(Constants.REGEX_PDB_CODE, idPath)) {
                    parser
                        .inputFormat(InputFormat.lookup(idPath))
                        .structureId(idPath)
                        .setFileOutput(fileOutput)
                        .run();
                } else if (Pattern.matches(Constants.REGEX_AF_CODE, idPath)) {
                    parser
                        .inputFormat(InputFormat.lookup(idPath))
                        .structureId(idPath)
                        .setFileOutput(fileOutput)
                        .run();
                } else if (idPath.endsWith(Constants.MMCIF_EXT)) {
                    parser
                        .inputFormat(InputFormat.lookup(idPath))
                        .cifFilePath(idPath)
                        .setFileOutput(fileOutput)
                        .run();
                } else if (idPath.endsWith(Constants.MMCIF_EXT + Constants.GZIP_EXT)) {
                    parser
                        .inputFormat(InputFormat.lookup(idPath))
                        .cifFilePath(idPath)
                        .setFileOutput(fileOutput)
                        .run();
                } else {
                    // Invalid PDB/AlphaFold ID or mmCIF path error
                    throw new Exception("PDB/AlphaFold ID or mmCIF path error : " + idPath);
                }
            }

            // Write RDF model if output format is "turtle"
            if (outForm.equals("turtle")) {
                fileOutput.getRdfModel().write(fileOutput.getBufferedWriter(), "TURTLE");
                fileOutput.closeRdfModel();
            }

            // Close output stream.
            fileOutput.closeWriteStream();

        } catch (NumberFormatException e) {
            // Handle errors in parsing numbers
            System.exit(STATUS_RUNTIME_ERROR);
        } catch (ParseException e) {
            // Handle parse errors
            System.err.println("Parse Error: " + e.getMessage() + "\n");
            printUsage();
            System.exit(STATUS_PARSE_ERROR);
        } catch (IllegalArgumentException e) {
            // Handle argument errors
            System.err.println("Argument Error: " + e.getMessage() + "\n");
            printUsage();
            System.exit(STATUS_ARGUMENT_ERROR);
        } catch (Exception e) {
            // Handle other exceptions
            System.exit(STATUS_RUNTIME_ERROR);
        }
    }

    // Get a new instance of the custom parser
    Parser getParser() {
        return new Parser();
    }

    // Print application usage information
    private void printUsage() {
        String syntax = String
            .format("%s "
        	    + "[-%s [--%s] <PDB CODE or ALPHAFOLD ID>]"
                + " [-%s [--%s] <OUTPUT DIRECTORY>"
                + " [-%s [--%s] <OUTPUT FORMAT(tsv, csv or turtle)>]"
                + " [-%s [--%s] <FORCE OVERWRITE>]"
                + "\nor\n"
                + "%s "
                + "[-%s [--%s] <mmCIF FILE PATH>]]"
                + " [-%s [--%s] <OUTPUT DIRECTORY>"
                + " [-%s [--%s] <OUTPUT FORMAT(tsv, csv or turtle)>]"
                + " [-%s [--%s] <FORCE OVERWRITE>]"
                + "\nor\n"
                + "%s "
                + "[-%s [--%s] <list (contains PDB or AlphaFold IDs, or local mmCIF file paths)>]]"
                + " [-%s [--%s] <OUTPUT DIRECTORY>"
                + " [-%s [--%s] <OUTPUT FORMAT(tsv, csv or turtle)>]"
                + " [-%s [--%s] <FORCE OVERWRITE>]",
            APP_NAME,
            OPTION_INPUT_ID_SHORT,
            OPTION_INPUT_ID_LONG,
            OPTION_OUTDIR_PATH_SHORT,
            OPTION_OUTDIR_PATH_LONG,
            OPTION_OUTPUT_FORMAT_SHORT,
            OPTION_OUTPUT_FORMAT_LONG,
            OPTION_FORCE_SHORT,
            OPTION_FORCE_LONG,
            APP_NAME,
            OPTION_INPUT_FILE_PATH_SHORT,
            OPTION_INPUT_FILE_PATH_LONG,
            OPTION_OUTDIR_PATH_SHORT,
            OPTION_OUTDIR_PATH_LONG,
            OPTION_OUTPUT_FORMAT_SHORT,
            OPTION_OUTPUT_FORMAT_LONG,
            OPTION_FORCE_SHORT,
            OPTION_FORCE_LONG,
            APP_NAME,
            OPTION_INPUT_LIST_SHORT,
            OPTION_INPUT_LIST_LONG,
            OPTION_OUTDIR_PATH_SHORT,
            OPTION_OUTDIR_PATH_LONG,
            OPTION_OUTPUT_FORMAT_SHORT,
            OPTION_OUTPUT_FORMAT_LONG,
            OPTION_FORCE_SHORT,
            OPTION_FORCE_LONG
        );
	    String header = "\nOptions:";
	    String footer = String.format("\nVersion: %s", APP_VERSION);

	    HelpFormatter hf = new HelpFormatter();

	    hf.printHelp(new PrintWriter(System.err, true), HelpFormatter.DEFAULT_WIDTH, syntax,
	        header, options(), HelpFormatter.DEFAULT_LEFT_PAD, HelpFormatter.DEFAULT_DESC_PAD, footer);
    }

    // Define command line options
    private Options options() {
        Options options = new Options();
        options.addOption(Option.builder(OPTION_INPUT_ID_SHORT)
            .longOpt(OPTION_INPUT_ID_LONG)
            .desc("Specify PDB code or AlphaFold ID to parse")
            .hasArg()
            .argName("String")
            .build()
        );
        options.addOption(Option.builder(OPTION_INPUT_FILE_PATH_SHORT)
            .longOpt(OPTION_INPUT_FILE_PATH_LONG)
            .desc("Specify the path of PDB or AlphaFold mmCIF file")
            .hasArg()
            .argName("String")
            .build()
        );
        options.addOption(Option.builder(OPTION_INPUT_LIST_SHORT)
            .longOpt(OPTION_INPUT_LIST_LONG)
            .desc("Specify the path of a list which contains PDB or AlphaFold IDs, or local mmCIF file paths")
            .hasArg()
            .argName("String")
            .build()
        );
        options.addOption(Option.builder(OPTION_OUTDIR_PATH_SHORT)
            .longOpt(OPTION_OUTDIR_PATH_LONG)
            .desc("Set path to the directory where output file is placed")
            .hasArg()
            .argName("DIRECTORY")
            .build()
        );
        options.addOption(Option.builder(OPTION_FORCE_SHORT)
            .longOpt(OPTION_FORCE_LONG)
            .desc("Overwrite existing file")
            .build()
        );
        options.addOption(Option.builder(OPTION_OUTPUT_FORMAT_SHORT)
            .longOpt(OPTION_OUTPUT_FORMAT_LONG)
            .desc("Specify output format, required")
            .hasArg()
            .argName("FORMAT")
            .argName("FORMAT=[" + String.join("|", ARGUMENTS_OUTPUT_FORMAT) + "]")
            .required()
            .build()
        );
        options.addOption(Option.builder("h")
            .longOpt("help")
            .desc("Show usage help")
            .build()
        );
	    return options;
    }

    // Getters and setters for PDB and local data flags
    public boolean isPdbData() {
        return this.pdbData;
    }
    public void setPdbData(boolean pdbData) {
        this.pdbData = pdbData;
    }

    public boolean isLocalData() {
        return this.localData;
    }
    public void setLocalData(boolean localData) {
        this.localData = localData;
    }

}

