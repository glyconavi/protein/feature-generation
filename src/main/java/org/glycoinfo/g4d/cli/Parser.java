package org.glycoinfo.g4d.cli;

import java.io.File;
import java.io.IOException;

import org.glycoinfo.g4d.exception.MmCIFParseException;
import org.glycoinfo.g4d.feature.MLFeatures;
import org.glycoinfo.g4d.io.FileOutput;
import org.glycoinfo.g4d.io.MmCIFParser;
import org.glycoinfo.g4d.util.Util;

public class Parser {

    private String cifFilePath;
    private boolean force;
    private InputFormat inputFormat;
    private OutputFormat outputFormat;
    private String outputFilePath;
    private String structureId;
    private boolean isPdbId;
    private boolean useId;
    private boolean outputToDir;

    private FileOutput fileOutput;

    // Default constructor initializing some boolean variables
    public Parser() {
        this.force = false;
        this.outputToDir = true;
        this.isPdbId = false;
    }

    // Setter methods for various parameters
    public Parser cifFilePath(String cifFilePath) {
        this.cifFilePath = cifFilePath;
        this.useId = false;
        return this;
    }
    public Parser force(boolean force) {
        this.force = force;
        return this;
    }
    public Parser outputFormat(OutputFormat format) {
        this.outputFormat = format;
        return this;
    }
    public Parser inputFormat(InputFormat format) {
        this.inputFormat = format;
        // if PDB ID is specified, sets true to this.isPdbId.
        if (this.inputFormat.getName() == "pdb") {
            this.isPdbId = true;
        } else {
            this.isPdbId = false;
        }
        return this;
    }
    public Parser setFileOutput(FileOutput fileOutput) {
    	this.fileOutput = fileOutput;
    	return this;
    }
    // Output file path with output directory.
    public Parser outputFilePath(String outputFilePath, String outDir) {
        this.outputFilePath = outDir + "/" + outputFilePath;
        if (force == false && (new File(this.outputFilePath).exists())) {
            System.out.println(this.outputFilePath + " file already exists.");
            System.out.println("Remove the -f option from the command line to overwrite the output results.");
            System.exit(0);
        }
        if (outDir != ".") {
        	createParentDir(new File(outDir));
            this.outputToDir = true;
        } else { // i.e. store output files in current directory
            this.outputToDir = false;
        }
        return this;
    }

    // PDB ID or AlphaFold ID
    public Parser structureId(String structureId) {
        this.structureId = structureId;
        this.useId = true;
        return this;
    }

    // Getter method for output file path
    public String getOutputFilePath() {
        return this.outputFilePath;
    }

    // Main method to execute the parsing and processing
    public void run() throws IOException, MmCIFParseException {
        // start time measurements.
        long start = System.currentTimeMillis();
        // run process method (gets machine learning features).
        process();
        // measure process times.
        long time = System.currentTimeMillis() - start;
        String timeFormated = Util.formatTime(time);
        System.out.println("Process time: " + timeFormated);
    }

    // Private method to handle the parsing and feature extraction
    private void process() throws IOException, MmCIFParseException {
        MmCIFParser mmCifParser;
        // Call an instance
        if (this.useId == true) {
            mmCifParser = new MmCIFParser(this.structureId, this.isPdbId);
        } else {
            mmCifParser = new MmCIFParser(getLocalCifFilePath());
        }
        // Parse mmCIF contents
        MLFeatures mlFeatures = mmCifParser.parse();
        // Output machine learning features by specifying format (TSV, CSV or Turtle)
        fileOutput.setMLFeaturesData(mlFeatures);
        fileOutput.setOutputType(this.outputFormat.getName());
        if (!this.outputFormat.getName().equals("turtle")) {
            fileOutput.writeText();
        } else { 
            fileOutput.writeTurtle();
        }
    }

    // Check a file path. If the file path is correct, return it.
    private String getLocalCifFilePath() {
        if (!new File(this.cifFilePath).exists()) {
        	throw new RuntimeException("File not found: " + this.cifFilePath);
        }
        return this.cifFilePath;
    }

    // Utility method to create parent directories for a given file path
    private void createParentDir(File path) {
        if (path == null || path.exists())
            return;
        if (path.getParentFile() != null) {
            if (!path.getParentFile().exists() || !path.getParentFile().isDirectory()) {
                createParentDir(path.getParentFile());
            }
        }
        path.mkdir();
    }

    // Getter method for output directory status
    public boolean getOutputToDir() {
        return this.outputToDir;
    }
}
