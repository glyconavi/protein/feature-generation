package org.glycoinfo.g4d.config;

public class Constants {
    // PDB URL
    public static final String PDB_BCIF_URL = "https://models.rcsb.org/";
    public static final String PDB_CIF_URL = "https://files.rcsb.org/download/";

    // AlphaFold URL
    public static final String AF_CIF_URL = "https://alphafold.ebi.ac.uk/files/";

    // N-Glycosylation site pattern.
    // It has been shown that the presence of proline between Asn and Ser/Thr will
    // inhibit N-glycosylation; this has been confirmed by a statistical analysis of
    // glycosylation sites, which also shows that about 50% of the sites that
    // have a proline C-terminal to Ser/Thr are not glycosylated.
    // note that Thr is more common than Ser.
    // i.e. N-{P}-[T/S]-{P}
    public static final String NGLYCO_RGX_PATTERN = "N[^P][S|T][^P]";
    //  It must also be noted that there are a few reported cases of glycosylation sites with
    // the pattern Asn-Xaa-Cys; an experimentally demonstrated occurrence of such a non-standard site
    // is found in the plasma protein C.
    // public static final String NGLYCO_RGX_PATTERN2 = "N-{P}-[C]";

    // PDB ID and AlphaFold ID regex.
    public static final String REGEX_PDB_CODE = "[0-9a-zA-Z]{4}";
    public static final String REGEX_AF_CODE = "(AF)\\-[0-9a-zA-Z]{6}\\-[0-9a-zA-Z]{2}\\-(model)\\_[v]{1}[0-9]+";

    // mmCIF extension.
    public static final String MMCIF_EXT = ".cif";
    public static final String GZIP_EXT = ".gz";
    // TSV extension.0
    public static final String TSV_EXT = ".tsv";
    // CSV extension.
    public static final String CSV_EXT = ".csv";
    // Turtle (RDF) extension.
    public static final String TTL_EXT = ".ttl";

    // separator.
    public static final String TSV_SEP = "	";
    public static final String CSV_SEP = ",";

    // word joiner.
    public static final String WORD_JOINER = ";";

    // empty column.
    public static final String EMPTY_COLUMN = "";
    
    // ATOM line atom ID Cα.
    public static final String CALPHA_TAG = "CA";
    // ATOM line atom ID C.
    public static final String C_TAG = "C";
    // ATOM line atom ID N.
    public static final String N_TAG = "N";

    // PDB RDF site url.
    public static final String PDBR_PREFIX = "https://rdf.wwpdb.org/pdb/";

    // g4d RDF url.
    public static final String G4D_PREFIX = "http://glyconavi.org/pdb/";

    // glyconavi url.
    public static final String GLYCONAVI_URL = "http://glyconavi.org";

    // DC url.
    public static final String DC_URL = "http://purl.org/dc/elements/1.1/";

}
