package org.glycoinfo.g4d.text;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

import org.glycoinfo.g4d.config.Constants;
import org.glycoinfo.g4d.feature.MLFeatures;
import org.glycoinfo.g4d.feature.MLFeaturesUtil;

public class OutputTextBuilder {

    private MLFeatures mlFeatures;
    private String outputFileType;
    private String sepType;

    // Constructor to initialize MLFeatures, output file type, and separator type
    public OutputTextBuilder(MLFeatures mlFeatures, String outputFileType) {
        this.mlFeatures = mlFeatures;
        this.outputFileType = outputFileType;
        // Determine the separator type based on the output file type
        if (this.outputFileType == "tsv") {
            this.sepType = Constants.TSV_SEP;
        } else if (this.outputFileType == "csv") {
            this.sepType = Constants.CSV_SEP;
        } else {
        }
    }

    // Write Entry AsymId column to the provided BufferedWriter
    public void writeEntryAsymIdColumn(BufferedWriter bw) {
        try {
            bw.write(this.mlFeatures.getEntryId() + ":" + this.mlFeatures.getCurrentAsymId());
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    // Write N-Glycosylation Sequence Numbers column to the provided BufferedWriter
    public void writeNGlycoSeqNumColumn(BufferedWriter bw) {
        ArrayList<Integer> nGlycoSeqIdList = this.mlFeatures.getnGlycoSeqIdList();
        int startIndex = this.mlFeatures.getnGlycoAsymIdList().indexOf(this.mlFeatures.getCurrentAsymId());
        int endIndex = this.mlFeatures.getnGlycoAsymIdList().lastIndexOf(this.mlFeatures.getCurrentAsymId());
        try {
            if (nGlycoSeqIdList.size() > 0 && startIndex != -1 && endIndex != -1) {
                // convert ArrayList<Integer> to ArrayList<String> and join them with ";".
                bw.write(
                    String.join(Constants.WORD_JOINER,
                        new ArrayList<String>(
                            nGlycoSeqIdList.subList(startIndex, endIndex + 1).stream().map(Object::toString).collect(Collectors.toList()))));
            } else { // i.e. N-Glycosylation site not found.
                bw.write(Constants.EMPTY_COLUMN);
            }
        } catch (IOException ioe) {
        	ioe.printStackTrace();
        }
    }

    // Write O-Glycosylation Sequence Numbers column to the provided BufferedWriter
    public void writeOGlycoSeqNumColumn(BufferedWriter bw) {
        ArrayList<Integer> oGlycoSeqIdList = this.mlFeatures.getoGlycoSeqIdList();
          int startIndex = this.mlFeatures.getoGlycoAsymIdList().indexOf(this.mlFeatures.getCurrentAsymId());
          int endIndex = this.mlFeatures.getoGlycoAsymIdList().lastIndexOf(this.mlFeatures.getCurrentAsymId());
        try {
            if (oGlycoSeqIdList.size() > 0 && startIndex != -1 && endIndex != -1) {
                // convert ArrayList<Integer> to ArrayList<String> and join them with ";".
                bw.write(
                    String.join(Constants.WORD_JOINER,
                        new ArrayList<String>(
                            oGlycoSeqIdList.subList(startIndex, endIndex + 1).stream().map(Object::toString).collect(Collectors.toList()))));
            } else { // i.e. O-Glycosylation site not found.
                bw.write(Constants.EMPTY_COLUMN);
            }
        } catch (IOException ioe) {
        	ioe.printStackTrace();
        }
    }

    // From here, data is generated for each AsymID. As that part is missing, data is generated for all chains for each chain.

    // Write N-Glycosylation consensus patterns column to the provided BufferedWriter
    public void writeNGlycoConsensusSeqColumn(BufferedWriter bw) {
    	MLFeaturesUtil.checkNGlyConsensusSequence(mlFeatures);
        ArrayList<String> nGlyConSeqList = this.mlFeatures.getnGlyConsensusSeqPatternList();
        // get consensus patterns corresponding to asymID.
        try {
            if (nGlyConSeqList.size() > 0) {
                bw.write("true");
                bw.write(this.sepType);
                bw.write(String.join(Constants.WORD_JOINER, nGlyConSeqList));
            } else {
                bw.write("false");
                bw.write(this.sepType);
                bw.write(Constants.EMPTY_COLUMN);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    // Write ASN positions column to the provided BufferedWriter
    public void writeAsnPositions(BufferedWriter bw) {
        MLFeaturesUtil.setAsnSerThrResidueNum(this.mlFeatures);
        ArrayList<Integer> asnPosList = this.mlFeatures.getAsnSeqPositionList();
        try {
            if (asnPosList.size() != 0) {
                // convert ArrayList<Integer> to ArrayList<String> and join them with ";".
                bw.write(
                    String.join(Constants.WORD_JOINER,
                        asnPosList.stream().map(Object::toString).collect(Collectors.toList())));
            } else {
                bw.write(Constants.EMPTY_COLUMN);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    // Write SER positions column to the provided BufferedWriter
    public void writeSerPositions(BufferedWriter bw) {
        ArrayList<Integer> serPosList = this.mlFeatures.getSerSeqPositionList();
        try {
            if (serPosList.size() != 0) {
                // convert ArrayList<Integer> to ArrayList<String> and join them with ";".
                bw.write(
                    String.join(Constants.WORD_JOINER,
                        serPosList.stream().map(Object::toString).collect(Collectors.toList())));
            } else {
                bw.write(Constants.EMPTY_COLUMN);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    // Write THR positions column to the provided BufferedWriter
    public void writeThrPositions(BufferedWriter bw) {
        ArrayList<Integer> thrPosList = this.mlFeatures.getThrSeqPositionList();
        try {
            if (thrPosList.size() != 0) {
                // convert ArrayList<Integer> to ArrayList<String> and join them with ";".
                bw.write(
                    String.join(Constants.WORD_JOINER,
                        thrPosList.stream().map(Object::toString).collect(Collectors.toList())));
            } else {
                bw.write(Constants.EMPTY_COLUMN);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    // Write S-S bond amino acid distances column to the provided BufferedWriter
    public void writeSSBondAaDistances(BufferedWriter bw) {
    	MLFeaturesUtil.calcSSBondAaDistance(this.mlFeatures);
        ArrayList<Integer> ssBondDistanceList = this.mlFeatures.getSsBondsAADistanceList();
        try {
            if (ssBondDistanceList.size() != 0) {
                // convert ArrayList<Integer> to ArrayList<String> and join them with ";".
                bw.write(
                    String.join(Constants.WORD_JOINER,
                        ssBondDistanceList.stream().map(Object::toString).collect(Collectors.toList())).replace("-1", ""));
            } else {
                bw.write(Constants.EMPTY_COLUMN);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    // Write S-S bond 3D distances column to the provided BufferedWriter
    public void writeSSBond3DDistances(BufferedWriter bw) {
    	MLFeaturesUtil.calcSSBond3DDistance(this.mlFeatures);
        ArrayList<String> ssBond3DDistanceList = this.mlFeatures.getSsBonds3DDistanceList();
        try {
            if (ssBond3DDistanceList.size() != 0) {
                // convert ArrayList<Float> to ArrayList<String> and join them with ";".
                bw.write(
                    String.join(Constants.WORD_JOINER,
                        ssBond3DDistanceList.stream().map(Object::toString).collect(Collectors.toList())).replace("-1.0", ""));
            } else {
                bw.write(Constants.EMPTY_COLUMN);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    // Write Torsion Angles column to the provided BufferedWriter
    public void writeTorsionAngles(BufferedWriter bw) {
    	MLFeaturesUtil.calcTorsionAngle(this.mlFeatures);
        ArrayList<String> torsionAngleList = this.mlFeatures.getTorsionAngleList();
        try {
            if (torsionAngleList.size() != 0) {
                bw.write(String.join(Constants.WORD_JOINER, torsionAngleList));
            } else {
                bw.write(Constants.EMPTY_COLUMN);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    // Write Glyco Amino Acid Distances column to the provided BufferedWriter
    public void writeGlyAaDistances(BufferedWriter bw) {
        MLFeaturesUtil.calcGlycoAminoAcidsDistance(this.mlFeatures);
        ArrayList<Integer> glyAaDistanceList = this.mlFeatures.getGlySiteAaDistanceList();
        try {
            if (glyAaDistanceList.size() != 0) {
                // convert ArrayList<Integer> to ArrayList<String> and join them with ";".
                bw.write(
                    String.join(Constants.WORD_JOINER,
                        glyAaDistanceList.stream().map(Object::toString).collect(Collectors.toList())));
            } else {
                bw.write(Constants.EMPTY_COLUMN);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    // Write Glyco Ter Distances and Areas columns to the provided BufferedWriter
    public void writeGlyTerDistancesAndAreas(BufferedWriter bw) {
        MLFeaturesUtil.calcSphere(this.mlFeatures);
        ArrayList<String> glySiteTerDistanceList = this.mlFeatures.getGlySiteTerDistanceList();
        ArrayList<String> glySite1_4AreaList = this.mlFeatures.getGlySite1_4AreaList();
        ArrayList<String> glySite7_0AreaList = this.mlFeatures.getGlySite7_0AreaList();
        ArrayList<String> glySite14AreaList = this.mlFeatures.getGlySite14AreaList();
        try {
            if (glySiteTerDistanceList.size() != 0) {
                // convert ArrayList<Integer> to ArrayList<String> and join them with ";".
                bw.write(
                    String.join(Constants.WORD_JOINER,
                        glySiteTerDistanceList.stream().map(Object::toString).collect(Collectors.toList())).replace("-1.0", ""));
            } else {
                bw.write(Constants.EMPTY_COLUMN);
            }
            bw.write(this.sepType);
            if (glySite1_4AreaList.size() != 0) {
                // convert ArrayList<Integer> to ArrayList<String> and join them with ";".
                bw.write(
                    String.join(Constants.WORD_JOINER,
                        glySite1_4AreaList.stream().map(Object::toString).collect(Collectors.toList())).replace("-1.0", ""));
            } else {
                bw.write(Constants.EMPTY_COLUMN);
            }
            bw.write(this.sepType);
            if (glySite7_0AreaList.size() != 0) {
                // convert ArrayList<Integer> to ArrayList<String> and join them with ";".
                bw.write(
                    String.join(Constants.WORD_JOINER,
                        glySite7_0AreaList.stream().map(Object::toString).collect(Collectors.toList())).replace("-1.0", ""));
            } else {
                bw.write(Constants.EMPTY_COLUMN);
            }
            bw.write(this.sepType);
            if (glySite14AreaList.size() != 0) {
                // convert ArrayList<Integer> to ArrayList<String> and join them with ";".
                bw.write(
                    String.join(Constants.WORD_JOINER,
                        glySite14AreaList.stream().map(Object::toString).collect(Collectors.toList())).replace("-1.0", ""));
            } else {
                bw.write(Constants.EMPTY_COLUMN);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

}

